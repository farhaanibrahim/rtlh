<!--bootstrap-->
<link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/bootstrap.css">
<script src="<?php echo base_url('asset/frontend'); ?>/js/bootstrap.js"></script>
<!-- load library jquery dan highcharts -->

<?php

$p1 = array(
  array(
    'y'=>$jml_p1,
    'color'=>'red'
  )
);
$p2 = array(
  array(
    'y'=>$jml_p2,
    'color'=>'yellow'
  )
);
$p3 = array(
  array(
    'y'=>$jml_p3,
    'color'=>'blue'
  )
);
foreach ($judul_report->result() as $judul_report) {}
?>

<div id="report"></div>

<a href="<?php echo site_url('front/pencarian'); ?>" class="btn btn-primary">Back</a>

<script src="<?php echo base_url('asset/frontend/js'); ?>/jquery.js"></script>
<script src="<?php echo base_url('asset/highcharts/js'); ?>/highcharts.js"></script>
<script src="<?php echo base_url('asset/highcharts/js/modules'); ?>/exporting.js"></script>
<script src="<?php echo base_url('asset/highcharts/js/modules'); ?>/offline-exporting.js"></script>

<script type="text/javascript">
$(function() {
    $('#report').highcharts({
        chart: {
            type: 'column',
            margin: 75,
            options3d: {
                enabled: false,
                alpha: 10,
                beta: 25,
                depth: 70
            }
        },
        title: {
            text: '<?php echo $judul_report->nm_kecamatan; ?>',
            style: {
                    fontSize: '18px',
                    fontFamily: 'Verdana, sans-serif'
            }
        },
        plotOptions: {
            column: {
                depth: 25
            }
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories:  ['Prioritas 1', 'Prioritas 2', 'Prioritas 3']
        },
        exporting: {
            enabled: false
        },
        yAxis: {
            title: {
                text: 'Jumlah'
            },
        },
        tooltip: {
             formatter: function() {
                 return 'The value for <b>' + this.x + '</b> is <b>' + Highcharts.numberFormat(this.y,0) + '</b>, in '+ this.series.name;
             }
          },
          exporting: {
              enabled: true,
          },
        series: [{
            name: 'Prioritas 1',
            data: <?php echo json_encode($p1); ?>,
            shadow : true,
            dataLabels: {
                enabled: true,
                color: 'red',
                align: 'center',
                formatter: function() {
                     return Highcharts.numberFormat(this.y, 0);
                }, // one decimal
                y: 0, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        {
            name: 'Prioritas 2',
            data: <?php echo json_encode($p2); ?>,
            shadow : true,
            dataLabels: {
                enabled: true,
                color: 'yellow',
                align: 'center',
                formatter: function() {
                     return Highcharts.numberFormat(this.y, 0);
                }, // one decimal
                y: 0, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        {
            name: 'Prioritas 3',
            data: <?php echo json_encode($p3); ?>,
            shadow : true,
            dataLabels: {
                enabled: true,
                color: 'blue',
                align: 'center',
                formatter: function() {
                     return Highcharts.numberFormat(this.y, 0);
                }, // one decimal
                y: 0, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
});
Highcharts.setOptions({
    colors: ['red', 'yellow', 'blue']
});
</script>
