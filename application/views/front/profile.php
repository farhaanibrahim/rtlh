<?php foreach($instansi->result() as $is_row); ?>
<!DOCTYPE HTML>
<html>

<head>
    <title>Traveler - About Us</title>


    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="Template, html, premium, themeforest" />
    <meta name="description" content="Traveler - Premium template for travel companies">
    <meta name="author" content="Tsoy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- GOOGLE FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600' rel='stylesheet' type='text/css'>
    <!-- /GOOGLE FONTS -->
     <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/icomoon.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/styles.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/mystyles.css">
    <script src="<?php echo base_url('asset/frontend'); ?>/js/modernizr.js"></script>
	
	<!-- /FAVICON---->
	<link rel="shortcut icon" href="<?php echo base_url("upload/$is_row->logo"); ?>"/>

</head>

<body>

    <!-- FACEBOOK WIDGET -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- /FACEBOOK WIDGET -->
    <div class="global-wrap">

		<header id="main-header">
            <?php include_once "layout_front/header.php";  ?>
        </header>
		
        <div class="container">
		<br>
		<br>
		<br>
        </div>




        <div class="container">
            <p>
			Dinas Pengawasan Bangunan dan Permukiman Kota Bogor dibentuk  berdasarkan Peraturan Daerah Kota Bogor Nomor 3 Tahun 2010 tanggal 24 Agustus 2010 tentang Organisasi Perangkat Daerah Kota Bogor, dalam rangka memenuhi tuntutan kebutuhan organisasi Pemerintah Daerah dalam urusan di bidang Pengawasan Bangunan dan Permukiman, guna memfasilitasi pemberdayaan daerah menuju terwujudnya peningkatan pembangunan, terciptanya kesejahteraan masyarakat dan terselenggaranya pemerintahan yang baik  (good governance).
			</p>
			<p>
			Berdasarkan Peraturan Daerah No. 3 Tahun 2010 tentang Organisasi Perangkat Daerah Kota Bogor bahwa Dinas Pengawasan Bangunan dan Permukiman mempunyai tugas pokok melaksanakan sebagaian kewenangan daerah dibidang Pengawasan Bangunan dan Permukiman, untuk melaksanakan tugas pokok tersebut, Dinas Pengawasan Bangunan dan Permukiman mempunyai fungsi :
			</p>
			<p>
			Perumusan kebijakan teknis di bidang pengawasan bangunan dan permukiman;<br>
			<ul>
				<li>Perumusan kebijakan teknis di bidang pengawasan bangunan dan permukiman;</li>
				<li>Penyelenggaraan urusan pemerintahan dan pelayanan umum di bidang Pengawasan Bangunan dan Permukiman;</li>
				<li>Pembinaan dan pelaksanaan tugas di bidang Pengawasan Bangunan dan Permukiman;</li>
				<li>Pelaksanaan tugas lain yang diberikan oleh Walikota Bogor sesuai tugas dan fungsinya;</li>
				<li>Pembinaan terhadap unit pelaksana teknis Dinas dalam lingkup tugasnya.</li>
			</ul>
			</p>
			
			<p><br>
			<b>Sejarah Berdirinya Instansi</b><br>
			Kedudukan Dinas Pengawasan Bangunan dan Permukiman Kota Bogor merupakan perangkat daerah Kota Bogor yang melaksanakan tugas penyelenggaraan urusan teknis dibidang pengawasan bangunan & permukiman dan pada tahun anggaran 2009-2010 merupakan masa transisi perubahan tatanan kelembagaan, maka terjadi penyesuaian tugas pokok dan fungsi dari Dinas Tata Kota & Pertamanan, kemudian Dinas Cipta Karta & Tata Ruang menjadi Dinas Pengawasan Bangunan dan Permukiman Kota Bogor berdasarkan Peraturan Daerah Kota Bogor Nomor 3 Tahun 2010 tentang Organisasi Perangkat Daerah Kota Bogor.
			</p>
            <div class="gap"></div>
        </div>



        <footer id="main-footer">
            <?php include_once "layout_front/footer.php"; ?>
        </footer>
	   
        <script src="<?php echo base_url('asset/frontend') ?>/js/jquery.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/bootstrap.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/slimmenu.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/datepicker.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/bootstrap-timepicker.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/nicescroll.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/dropit.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/ionrangeslider.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/icheck.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/fotorama.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/typeahead.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/card-payment.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/magnific.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/owl-carousel.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/fitvids.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/tweet.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/countdown.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/gridrotator.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/custom.js"></script>
    </div>
</body>

</html>


