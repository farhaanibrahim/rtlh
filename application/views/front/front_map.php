<!DOCTYPE html>
<?php foreach($instansi->result() as $is_row); ?>
<html>
<head>
    <meta charset='utf-8' />
    <title><?php echo $title; ?></title>
    <meta name='viewport' content='initial-scale=1,maximum-scale=1,user-scalable=no' />
    <script src='<?php echo base_url('asset/frontend/mapbox'); ?>/mapbox-gl.js'></script>
    <link href='<?php echo base_url('asset/frontend/mapbox'); ?>/mapbox-gl.css' rel='stylesheet' />
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="Template, html, premium, themeforest" />
    <meta name="description" content="Traveler - Premium template for travel companies">
    <meta name="author" content="Tsoy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- GOOGLE FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600' rel='stylesheet' type='text/css'>
    <!-- /GOOGLE FONTS -->
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/icomoon.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/styles.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/mystyles.css">
    <script src="<?php echo base_url('asset/frontend'); ?>/js/modernizr.js"></script>

	<!-- /FAVICON---->
	<link rel="shortcut icon" href="<?php echo base_url("upload/$is_row->logo"); ?>"/>
</head>
<body>

  <!-- FACEBOOK WIDGET -->
  <div id="fb-root"></div>
  <script>
      (function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s);
          js.id = id;
          js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
          fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
  </script>
  <!-- /FACEBOOK WIDGET -->
  <div class="global-wrap">
    <div class="tabbable">
      <div class="tab-content">
        <div class="tab-pane fade in active" id="tab-1">
          <header id="main-header">
              <?php include_once "layout_front/header.php";  ?>
          </header>
          <style>
            #menu {
                background: #3887be;
                color: #ffffff;
                position: absolute;
                margin: 100px 0px 0px 0px;
                z-index: 1;
                top: 10px;
                right: 10px;
                border-radius: 3px;
                width: 150px;
                border: 1px solid rgba(0,0,0,0.4);
                font-family: 'Open Sans', sans-serif;
            }

            #menu a {
                font-size: 13px;
                color: #ffffff;
                display: block;
                margin: 0;
                padding: 0;
                padding: 5px;
                text-decoration: none;
                border-bottom: 1px solid rgba(0,0,0,0.25);
                text-align: center;
            }
            #menu label {
                font-size: 13px;
                background-color: #d1d2d3;
                color: #ff4747;
                display: block;
                margin: 0;
                padding: 0;
                padding: 10px;
                text-decoration: none;
                border-bottom: 1px solid rgba(0,0,0,0.25);
                text-align: center;
            }

            #menu a:last-child {
                border: none;
            }

            #menu a:hover {
                background-color: #3074a4;
                color: #404040;
            }

            #menu a.active {
                background-color: #fff;
                color: #262626;
            }

            #menu a.active:hover {
                background: #f8f8f8;
            }
            #menuKecamatan {
                background: #3887be;
                position: absolute;
                margin: 250px 0px 0px 0px;
                z-index: 1;
                top: 10px;
                right: 10px;
                border-radius: 3px;
                width: 150px;
                border: 1px solid rgba(0,0,0,0.4);
                font-family: 'Open Sans', sans-serif;
            }

            #menuKecamatan a {
                font-size: 13px;
                color: #ffffff;
                display: block;
                margin: 0;
                padding: 0;
                padding: 5px;
                text-decoration: none;
                border-bottom: 1px solid rgba(0,0,0,0.25);
                text-align: center;
            }
            #menuKecamatan label {
                font-size: 13px;
                background-color: #d1d2d3;
                color: #ff4747;
                display: block;
                margin: 0;
                padding: 0;
                padding: 10px;
                text-decoration: none;
                border-bottom: 1px solid rgba(0,0,0,0.25);
                text-align: center;
            }

            #menuKecamatan a:last-child {
                border: none;
            }

            #menuKecamatan a:hover {
                background-color: #3074a4;
                color: #404040;
            }

            #menuKecamatan a.active {
                background-color: #fff;
                color: #262626;
            }

            #menuKecamatan a.active:hover {
                background: #f8f8f8;
            }
            #pencarian {
                background: #3887be;
                position: absolute;
                margin: 500px 0px 0px 0px;
                z-index: 1;
                top: 10px;
                right: 10px;
                border-radius: 3px;
                width: 150px;
                border: 1px solid rgba(0,0,0,0.4);
                font-family: 'Open Sans', sans-serif;
            }
            #pencarian a {
                font-size: 13px;
                color: #ffffff;
                display: block;
                margin: 0;
                padding: 0;
                padding: 5px;
                text-decoration: none;
                border-bottom: 1px solid rgba(0,0,0,0.25);
                text-align: center;
            }

            .modal-dialog {
              width: 95%;
              height: 95%;
              margin: 0;
              padding: 10px;
              background-color: white;
              border-radius: 10px;
            }

            .modal-content {
              height: auto;
              min-height: 100%;
              border-radius: 0;
            }
        </style>

          <nav id="menu"></nav>
          <nav id="menuKecamatan"></nav>
          <nav id="pencarian">
            <a href="#" id="menu-5">Pencarian</a>
          </nav>
          <div class="modal" id="modal-5" tabindex="-1" role="dialog" style="display: none;opacity:1; top:5%; left:2%;">
            <div class="modal-dialog" role="document">
              <button type="button" class="close" data-dismiss="modal" id="closepopup-5" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Report</h4>
              <iframe id="iframe-5" src="" width="100%"  height="95%" frameborder="0" ></iframe>
            </div><!-- /.modal-dialog -->
          </div><!-- /.modal -->

            <div id='map' style="width: 100%; height:600px;"></div>
            <script>
              mapboxgl.accessToken = 'pk.eyJ1IjoiZmFyaGFhbmlicmFoaW0iLCJhIjoiY2owdGpxam1sMDVoNjJxbzVuZ3ZkeGs3diJ9.HoswZ_11luzV16HQ1CYONA';
              var map = new mapboxgl.Map({
                  container: 'map', // container id
                  style: 'mapbox://styles/farhaanibrahim/cj0zpf94l002b2rqu20o6uich', //stylesheet location
                  center: [106.856,-6.593], // starting position
                  zoom: 11.5 // starting zoom
              });
              var nav = new mapboxgl.NavigationControl();
              map.addControl(nav, 'top-left');

              map.on('load',function() {
                map.addSource('firstPriority',{
                  type: 'geojson',
                  data: {
                    "type": "FeatureCollection",
                    "features": [
                      <?php
                      $numItem = $firstPriority->num_rows();
                      $i = 0;
                      foreach ($firstPriority->result_array() as $row => $value) {
                        ?>
                        {
                          "type": "Feature",
                          "geometry": {
                            "type": "Point",
                            "coordinates": [
                              <?php echo (float)$value['longitude']; ?>,
                              <?php echo (float)$value['latitude']; ?>
                            ]
                          },
                          "properties": {
                            "nama": "<h4><?php echo $value['nm_pemilik']; ?></h4>",
                            "ktp": "<p><?php echo $value['no_ktp'];?></p>",
                            "penghasilan": "<p><?php echo $value['rentang_pengahasilan_bulanan']; ?></p>",
                            "alamat": "<?php echo $value['rtlh_almt']; ?>",
                            "icon":"marker"
                          }
                        }<?php
                          if(++$i === $numItem){
                            echo "";
                          } else {
                            echo ",";
                          }
                        ?>
                        <?php
                      }
                      ?>
                    ]
                  }
                });
                map.addLayer({
                  'id': 'Prioritas 1',
                  'source': 'firstPriority',
                  'type': 'circle',
                  'layout' : {
                      'visibility': 'visible'
                  },
                  'paint': {
                    'circle-radius': 5,
                    'circle-color': 'red'
                  }
                });

                // PRIORITAS 2
                map.addSource('secondPriority',{
                  type: 'geojson',
                  data: {
                    "type": "FeatureCollection",
                    "features": [
                      <?php
                      $numItem = $secondPriority->num_rows();
                      $i = 0;
                      foreach ($secondPriority->result_array() as $row => $value) {
                        ?>
                        {
                          "type": "Feature",
                          "geometry": {
                            "type": "Point",
                            "coordinates": [
                              <?php echo (float)$value['longitude']; ?>,
                              <?php echo (float)$value['latitude']; ?>
                            ]
                          },
                          "properties": {
                            "nama": "<h4><?php echo $value['nm_pemilik']; ?></h4>",
                            "ktp": "<p><?php echo $value['no_ktp'];?></p>",
                            "penghasilan": "<p><?php echo $value['rentang_pengahasilan_bulanan']; ?></p>",
                            "alamat": "<?php echo $value['rtlh_almt']; ?>",
                            "icon":"marker"
                          }
                        }<?php
                          if(++$i === $numItem){
                            echo "";
                          } else {
                            echo ",";
                          }
                        ?>
                        <?php
                      }
                      ?>
                    ]
                  }
                });
                map.addLayer({
                  'id': 'Prioritas 2',
                  'source': 'secondPriority',
                  'type': 'circle',
                  'layout': {
                      'visibility': 'visible'
                  },
                  'paint': {
                    'circle-radius': 5,
                    'circle-color': 'yellow'
                  }
                });

                // PRIORITAS 3
                map.addSource('thirdPriority',{
                  type: 'geojson',
                  data: {
                    "type": "FeatureCollection",
                    "features": [
                      <?php
                      $numItem = $thirdPriority->num_rows();
                      $i = 0;
                      foreach ($thirdPriority->result_array() as $row => $value) {
                        ?>
                        {
                          "type": "Feature",
                          "geometry": {
                            "type": "Point",
                            "coordinates": [
                              <?php echo (float)$value['longitude']; ?>,
                              <?php echo (float)$value['latitude']; ?>
                            ]
                          },
                          "properties": {
                            "nama": "<h4><?php echo $value['nm_pemilik']; ?></h4>",
                            "ktp": "<p><?php echo $value['no_ktp'];?></p>",
                            "penghasilan": "<p><?php echo $value['rentang_pengahasilan_bulanan']; ?></p>",
                            "alamat": "<?php echo $value['rtlh_almt']; ?>"
                          }
                        }<?php
                          if(++$i === $numItem){
                            echo "";
                          } else {
                            echo ",";
                          }
                        ?>
                        <?php
                      }
                      ?>
                    ]
                  }
                });
                map.addLayer({
                  'id': 'Prioritas 3',
                  'source': 'thirdPriority',
                  'type': 'circle',
                  'layout': {
                      'visibility': 'visible'
                  },
                  'paint': {
                    'circle-radius': 5,
                    'circle-color': 'blue'
                  }
                });

                //Bogor Barat
                map.addLayer({
                    'id': 'Bogor Barat',
                    'type': 'fill',
                    'source': {
                        'type': 'geojson',
                        'data': 'http://localhost/rtlh/asset/polygon/PBogorBarat.geojson'
                    },
                    'layout': {
                      'visibility' : 'none'
                    },
                    'paint': {
                        'fill-color': '#ffb2e2',
                        'fill-opacity': 0.5
                    }
                });
              //Bogor Selatan
              map.addLayer({
                  'id': 'Bogor Selatan',
                  'type': 'fill',
                  'source': {
                      'type': 'geojson',
                      'data': 'http://localhost/rtlh/asset/polygon/PBogorSelatan.geojson'
                  },
                  'layout': {
                    'visibility' : 'none'
                  },
                  'paint': {
                      'fill-color': '#c0ffb2',
                      'fill-opacity': 0.5
                  }
              });
              //Bogor Tengah
              map.addLayer({
                  'id': 'Bogor Tengah',
                  'type': 'fill',
                  'source': {
                      'type': 'geojson',
                      'data': 'http://localhost/rtlh/asset/polygon/PBogorTengah.geojson'
                  },
                  'layout': {
                    'visibility' : 'none'
                  },
                  'paint': {
                      'fill-color': '#ff8468',
                      'fill-opacity': 0.5
                  }
              });
              //Bogor Timur
              map.addLayer({
                  'id': 'Bogor Timur',
                  'type': 'fill',
                  'source': {
                      'type': 'geojson',
                      'data': 'http://localhost/rtlh/asset/polygon/PBogorTimur.geojson'
                  },
                  'layout': {
                    'visibility' : 'none'
                  },
                  'paint': {
                      'fill-color': '#f4ffb2',
                      'fill-opacity': 0.5
                  }
              });
              //Bogor Utara
              map.addLayer({
                  'id': 'Bogor Utara',
                  'type': 'fill',
                  'source': {
                      'type': 'geojson',
                      'data': 'http://localhost/rtlh/asset/polygon/PBogorUtara.geojson'
                  },
                  'layout': {
                    'visibility' : 'none'
                  },
                  'paint': {
                      'fill-color': '#b2d3ff',
                      'fill-opacity': 0.5
                  }
              });
              //Tanah Sareal
              map.addLayer({
                  'id': 'Tanah Sareal',
                  'type': 'fill',
                  'source': {
                      'type': 'geojson',
                      'data': 'http://localhost/rtlh/asset/polygon/PTanahSareal.geojson'
                  },
                  'layout': {
                    'visibility' : 'none'
                  },
                  'paint': {
                      'fill-color': '#92f98e',
                      'fill-opacity': 0.5
                  }
              });

            });
              //popup
              map.on('click', function (e) {
                var features = map.queryRenderedFeatures(e.point, { layers: ['Prioritas 1','Prioritas 2','Prioritas 3'] });

                if (!features.length) {
                    return;
                }

                var feature = features[0];

                // Populate the popup and set its coordinates
                // based on the feature found.
                var popup = new mapboxgl.Popup()
                    .setLngLat(feature.geometry.coordinates)
                    .setHTML(feature.properties.nama+feature.properties.ktp+feature.properties.penghasilan+feature.properties.alamat)
                    .addTo(map);

            });

            //Layer Prioritas
            var layerPrioritas = [ 'Prioritas 1', 'Prioritas 2', 'Prioritas 3' ];
            var priority = document.createElement('label');
            priority.className = '';
            priority.textContent = 'Prioritas';
            var heading = document.getElementById('menu');
            heading.appendChild(priority);
            for (var i = 0; i < layerPrioritas.length; i++) {
                var id = layerPrioritas[i];

                var link = document.createElement('a');
                link.href = '#';
                link.className = 'active';
                link.textContent = id;

                link.onclick = function (e) {
                    var clickedLayer = this.textContent;
                    e.preventDefault();
                    e.stopPropagation();

                    var visibility = map.getLayoutProperty(clickedLayer, 'visibility');

                    if (visibility === 'visible') {
                        map.setLayoutProperty(clickedLayer, 'visibility', 'none');
                        this.className = '';
                    } else {
                        this.className = 'active';
                        map.setLayoutProperty(clickedLayer, 'visibility', 'visible');
                    }
                };

                var layers = document.getElementById('menu');
                layers.appendChild(link);
            }

            //Layer Kecamatan
            var layerKecamatan = ['Bogor Barat', 'Bogor Utara', 'Bogor Timur', 'Bogor Tengah', 'Bogor Selatan', 'Tanah Sareal' ];
            var kecamatan = document.createElement('label');
            kecamatan.className = '';
            kecamatan.textContent = 'Kecamatan';
            var headingKecamatan = document.getElementById('menuKecamatan');
            headingKecamatan.appendChild(kecamatan);
            for (var i = 0; i < layerKecamatan.length; i++) {
                var id = layerKecamatan[i];

                var linkKecamatan = document.createElement('a');
                linkKecamatan.href = '#';
                linkKecamatan.className = 'active';
                linkKecamatan.textContent = id;

                linkKecamatan.onclick = function (e) {
                    var clickedLayer = this.textContent;
                    e.preventDefault();
                    e.stopPropagation();

                    var visibility = map.getLayoutProperty(clickedLayer, 'visibility');

                    if (visibility === 'none') {
                        map.setLayoutProperty(clickedLayer, 'visibility', 'visible');
                        this.className = '';
                    } else {
                        this.className = 'active';
                        map.setLayoutProperty(clickedLayer, 'visibility', 'none');
                    }
                };

                var layers = document.getElementById('menuKecamatan');
                layers.appendChild(linkKecamatan);
            }
          </script>

          <footer id="main-footer">
              <?php include_once "layout_front/footer.php"; ?>
          </footer>

          <script src="<?php echo base_url('asset/frontend') ?>/js/jquery.js"></script>
          <script src="<?php echo base_url('asset/frontend') ?>/js/bootstrap.js"></script>
          <script src="<?php echo base_url('asset/frontend') ?>/js/slimmenu.js"></script>
          <script src="<?php echo base_url('asset/frontend') ?>/js/datepicker.js"></script>
          <script src="<?php echo base_url('asset/frontend') ?>/js/bootstrap-datepicker.js"></script>
          <script src="<?php echo base_url('asset/frontend') ?>/js/bootstrap-timepicker.js"></script>
          <script src="<?php echo base_url('asset/frontend') ?>/js/nicescroll.js"></script>
          <script src="<?php echo base_url('asset/frontend') ?>/js/dropit.js"></script>
          <script src="<?php echo base_url('asset/frontend') ?>/js/ionrangeslider.js"></script>
          <script src="<?php echo base_url('asset/frontend') ?>/js/icheck.js"></script>
          <script src="<?php echo base_url('asset/frontend') ?>/js/fotorama.js"></script>
          <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
          <script src="<?php echo base_url('asset/frontend') ?>/js/typeahead.js"></script>
          <script src="<?php echo base_url('asset/frontend') ?>/js/card-payment.js"></script>
          <script src="<?php echo base_url('asset/frontend') ?>/js/magnific.js"></script>
          <script src="<?php echo base_url('asset/frontend') ?>/js/owl-carousel.js"></script>
          <script src="<?php echo base_url('asset/frontend') ?>/js/fitvids.js"></script>
          <script src="<?php echo base_url('asset/frontend') ?>/js/tweet.js"></script>
          <script src="<?php echo base_url('asset/frontend') ?>/js/countdown.js"></script>
          <script src="<?php echo base_url('asset/frontend') ?>/js/gridrotator.js"></script>
          <script src="<?php echo base_url('asset/frontend') ?>/js/custom.js"></script>
        </div>
      </div>
    </div>
  </div>
</body>
</html>

<script>
$("#menu-5" ).click(function() {
  $("#modal-5").show();$("#iframe-5").attr("src", "<?php echo site_url('front/pencarian'); ?>");
  });
$("#closepopup-5" ).click(function() {
	$("#modal-5").hide();
	$("#iframe-5").attr("src", "");
  });
</script>
