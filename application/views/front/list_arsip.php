<!DOCTYPE HTML>
<?php foreach($instansi->result() as $is_row); ?>
<html>

<head>
    <title><?php echo $title2; ?></title>


    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="Template, html, premium, themeforest" />
    <meta name="description" content="Traveler - Premium template for travel companies">
    <meta name="author" content="Tsoy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- GOOGLE FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600' rel='stylesheet' type='text/css'>
    <!-- /GOOGLE FONTS -->
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend'); ?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend'); ?>/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend'); ?>/css/icomoon.css">
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend'); ?>/css/styles.css">
    <link rel="stylesheet" href="<?php echo base_url('aset/asset_frontend'); ?>/css/mystyles.css">
    <script src="<?php echo base_url('aset/asset_frontend'); ?>/js/modernizr.js"></script>
	
	<!-- /FAVICON---->
	<link rel="shortcut icon" href="<?php echo base_url("upload/$is_row->logo"); ?>"/>


</head>

<body>

    <!-- FACEBOOK WIDGET -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- /FACEBOOK WIDGET -->
    <div class="global-wrap">
        <header id="main-header">
			<?php include_once "layout_front/header.php";  ?>
		</header>

        <div class="container">
            <h1 class="page-title"><?php echo $title2; ?></h1>
        </div>




        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <aside class="sidebar-left">
                        <form action="<?php echo site_url('front/cari_arsip/baru') ?>" method="post">
                            <div class="form-group form-group-icon-left"><i class="fa fa-map-marker input-icon input-icon-hightlight"></i>
                                <label>No Surat</label>
                                <input class="typeahead form-control" name="no_surat" placeholder="Ex : S/001/DPKS/2016" type="text" />
                            </div>
                            <div class="input-daterange" data-date-format="MM d, D">
                                <div class="form-group form-group-icon-left"><i class="fa fa-calendar input-icon input-icon-hightlight"></i>
                                    <label>Dari Tanggal</label>
                                    <input class="form-control" name="start" id="start_date" type="text" />
                                </div>
                                <div class="form-group form-group-icon-left"><i class="fa fa-calendar input-icon input-icon-hightlight"></i>
                                    <label>Sampai Tanggal</label>
                                    <input class="form-control" name="end" id="end_date" type="text" />
                                </div>
                            </div>
							<div class="form-group form-group-icon-left">
                                <label>Status Surat</label>
								<div class="radio-inline radio-sm" style="margin-top:10px;">
									<label><input class="i-radio" type="radio" name="status_surat" value="t_surat_keluar" />Surat Keluar</label>
									<label><input class="i-radio" type="radio" name="status_surat" value="t_surat_masuk" />Surat Masuk</label>
								</div>
							</div>
							<input class="btn btn-primary mt10" type="submit" value="Cari Arsip" />
                        </form>
                    </aside>
					<br>
                </div>
                <div class="col-md-9">
                    <div class="nav-drop booking-sort">
                        <h5 class="booking-sort-title"><a href="#">Daftar Surat<i class="fa fa-angle-down"></i><i class="fa fa-angle-up"></i></a></h5>
                        <ul class="nav-drop-menu">
                            
                        </ul>
                    </div>
                    <ul class="booking-list">
						<?php 
						if($data_row->num_rows() > 0){
						foreach($data_row->result() as $arsip): ?>
                        <li>
                            <a class="booking-item" href="#">
                                <div class="row">
                                    <div class="col-md-8">
                                        
                                        <h5 class="booking-item-title"><?php echo $arsip->no_surat; ?></h5>
                                        <p class="booking-item-address"><i class="fa fa-map-marker"></i> 
										<?php 
										if($jenis_surat == 't_surat_keluar'){
											echo 'Tujuan : '.$arsip->tujuan; 
											echo ', Status : <b>Surat Keluar</b>'; 
										}else if($jenis_surat == 't_surat_masuk'){
											echo 'Dari : '.$arsip->dari;
											echo ', Status : <b>Surat Masuk</b>'; 
										}?>
										</p>
                                        <p class="booking-item-description"><?php echo $arsip->isi_ringkas; ?></p>
                                    </div>
                                    <div class="col-md-4" align="right">
										<span class="">Tanggal surat : <?php echo $arsip->tgl_surat; ?></span><br>
										<?php 
										if($jenis_surat == 't_surat_keluar'){
										?>
										<span class="">Tanggal Catat : <?php echo $arsip->tgl_catat; ?></span><br><br>
										<?php
										}else if($jenis_surat == 't_surat_masuk'){
										?>
										<span class="">Tanggal diterima : <?php echo $arsip->tgl_diterima; ?></span><br><br>
										<?php
										}?>
										<span class="btn btn-primary">Lihat<span class="fa fa-search"></span></span>
                                    </div>
                                </div>
                            </a>
                        </li>
						<?php 
						endforeach; 
						}else{
						?>
						<li>
							<h4>Surat yang anda cari tidak kami temukan, mohon cari kembali dalam beberapa waktu kedepan.<h2>
						</li>
						<?php
						}
						?>
					</ul>
                    <div class="row">
                        <div class="col-md-6">
                            <p><small><?php echo "Jumlah data seluruhnya adalah ".$cari_arsip->num_rows()." Data Arsip"; ?>. </small>
                            </p>
                            <ul class="pagination">
                                <?php echo $pagination; ?>
                            </ul>
                        </div>
						
                    </div>
					<br>
                </div>
            </div>
        </div>



        <footer id="main-footer">
            <?php include_once "layout_front/footer.php"; ?>
        </footer>

        <script src="<?php echo base_url('aset/asset_frontend') ?>/js/jquery.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend') ?>/js/bootstrap.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend') ?>/js/slimmenu.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend') ?>/js/datepicker.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend') ?>/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend') ?>/js/bootstrap-timepicker.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend') ?>/js/nicescroll.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend') ?>/js/dropit.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend') ?>/js/ionrangeslider.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend') ?>/js/icheck.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend') ?>/js/fotorama.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="<?php echo base_url('aset/asset_frontend') ?>/js/typeahead.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend') ?>/js/card-payment.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend') ?>/js/magnific.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend') ?>/js/owl-carousel.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend') ?>/js/fitvids.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend') ?>/js/tweet.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend') ?>/js/countdown.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend') ?>/js/gridrotator.js"></script>
        <script src="<?php echo base_url('aset/asset_frontend') ?>/js/custom.js"></script>
    </div>
</body>

</html>


