<!--bootstrap-->
<link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/bootstrap.css">
<script src="<?php echo base_url('asset/frontend'); ?>/js/bootstrap.js"></script>
<!-- load library jquery dan highcharts -->
<script src="<?php echo base_url('asset/highchart'); ?>/jquery.js"></script>
<script src="<?php echo base_url('asset/highchart'); ?>/highcharts.js"></script>
<script src="<?php echo base_url('asset/highchart/modules'); ?>/exporting.js"></script>
<script src="<?php echo base_url('asset/highchart/modules'); ?>/offline-exporting.js"></script>

<?php

$value = array(
  array(
    'y'=>$jml_p1,
    'color'=>'red'
  ),
  array(
    'y'=>$jml_p2,
    'color'=>'yellow'
  ),
  array(
    'y'=>$jml_p3,
    'color'=>'blue'
  )
);


echo $kecamatan;
?>

<div id="report"></div>
<script type="text/javascript">
$(function() {
    $('#report').highcharts({
        chart: {
            type: 'column',
            margin: 75,
            options3d: {
                enabled: false,
                alpha: 10,
                beta: 25,
                depth: 70
            }
        },
        title: {
            text: '<?php echo $judul_report; ?>',
            style: {
                    fontSize: '18px',
                    fontFamily: 'Verdana, sans-serif'
            }
        },
        subtitle: {
           text: 'Penjualan',
           style: {
                    fontSize: '15px',
                    fontFamily: 'Verdana, sans-serif'
            }
        },
        plotOptions: {
            column: {
                depth: 25
            }
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories:  ['Prioritas 1', 'Prioritas 2', 'Prioritas 3']
        },
        exporting: {
            enabled: false
        },
        yAxis: {
            title: {
                text: 'Jumlah'
            },
        },
        tooltip: {
             formatter: function() {
                 return 'The value for <b>' + this.x + '</b> is <b>' + Highcharts.numberFormat(this.y,0) + '</b>, in '+ this.series.name;
             }
          },
        series: [{
            name: 'Report Data',
            data: <?php echo json_encode($value); ?>,
            shadow : true,
            dataLabels: {
                enabled: true,
                color: '#045396',
                align: 'center',
                formatter: function() {
                     return Highcharts.numberFormat(this.y, 0);
                }, // one decimal
                y: 0, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
});
</script>
