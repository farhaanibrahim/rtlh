<!DOCTYPE HTML>
<?php foreach($instansi->result() as $is_row){} ?>
<?php foreach($data_login->result() as $is_row2){} ?>
<?php foreach($data_row->result() as $dt_row){} ?>
<html>

<head>
    <title><?php echo $title; ?></title>


    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="Template, html, premium, themeforest" />
    <meta name="description" content="Traveler - Premium template for travel companies">
    <meta name="author" content="Tsoy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

     <!-- GOOGLE FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600' rel='stylesheet' type='text/css'>
    <!-- /GOOGLE FONTS -->
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/icomoon.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/styles.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/mystyles.css">
    <script src="<?php echo base_url('asset/frontend'); ?>/js/modernizr.js"></script>
	<!-- /FAVICON---->
	<link rel="shortcut icon" href="<?php echo base_url("upload/$is_row->logo"); ?>"/>


</head>

<body>

    <!-- FACEBOOK WIDGET -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- /FACEBOOK WIDGET -->
    <div class="global-wrap">
        <header id="main-header">
			<?php include_once "layout_front/header.php";  ?>
		</header>
		
		<div class="container">
            <h3 style="margin:30px 0;">Peta Informasi RTLH <?php echo $dt_row->nm_pemilik; ?></h3>
        </div>




        <div class="container">
            <div class="row">
                <div class="col-md-12">
					<ul class="nav nav-tabs" id="myTab">
						<li class="active"><a href="#google-map-tab" data-toggle="tab"><i class="fa fa-map-marker"></i> Peta</a>
						</li>
						<li><a href="#tab-1" data-toggle="tab"><i class="fa fa-camera"></i> Foto/Gambar Sisi Rumah</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane fade" id="tab-1">
							<div class="fotorama" data-allowfullscreen="true" data-nav="thumbs">
								<?php
								if($dt_row->sisi1 != ""){
								?>
									<img src="<?php echo base_url("upload/foto_awal/$dt_row->sisi1"); ?>" alt="Image Alternative text" title="Foto Sisi 1 milik Bpk/Ibu <?php echo $dt_row->nm_pemilik; ?>" />
								<?php
								}
								if($dt_row->sisi2 != ""){
								?>
									<img src="<?php echo base_url("upload/foto_awal/$dt_row->sisi2"); ?>" alt="Image Alternative text" title="Foto Sisi 1 milik Bpk/Ibu <?php echo $dt_row->nm_pemilik; ?>" />
								<?php
								}
								if($dt_row->sisi3 != ""){
								?>
									<img src="<?php echo base_url("upload/foto_awal/$dt_row->sisi3"); ?>" alt="Image Alternative text" title="Foto Sisi 3 milik Bpk/Ibu <?php echo $dt_row->nm_pemilik; ?>" />
								<?php
								}
								if($dt_row->sisi4 != ""){
								?>
									<img src="<?php echo base_url("upload/foto_awal/$dt_row->sisi4"); ?>" alt="Image Alternative text" title="Foto Sisi 4 milik Bpk/Ibu <?php echo $dt_row->nm_pemilik; ?>" />
								<?php
								}if($dt_row->sisi5 != ""){
								?>
									<img src="<?php echo base_url("upload/foto_awal/$dt_row->sisi5"); ?>" alt="Image Alternative text" title="Foto Sisi 5 milik Bpk/Ibu <?php echo $dt_row->nm_pemilik; ?>" />
								<?php
								}if($dt_row->sisi6 != ""){
								?>
									<img src="<?php echo base_url("upload/foto_awal/$dt_row->sisi6"); ?>" alt="Image Alternative text" title="Foto Sisi 6 milik Bpk/Ibu <?php echo $dt_row->nm_pemilik; ?>" />
								<?php
								}
								?>
							</div>
						</div>
						<div class="tab-pane fade in active" id="google-map-tab">
							<div id="map-canvas" data-map-type="roadmap" data-map-zoom="5" data-map-address="Indonesia" style="width:100%; height:400px;"></div>
						</div>
					</div>
                </div>
				<br>
				<br>
				<div class="col-md-3"><br>
					<h4 style="font-weight:bold; text-decoration:underline;">A.Identifikasi Lokasi</h4>
					<table width="100%" cellpadding="25" cellspacing="5">
						<tr>
							<th>Atribut</th>
							<th width="10%">  </th>
							<th>Keterangan</th>
						</tr><tr>
							
							<td valign="top">No KTP</td>
							<td valign="top"> : </td>
							<td valign="top"><?php echo $dt_row->no_ktp; ?></td>
						</tr><tr>
							
							<td valign="top">No KK</td>
							<td valign="top"> : </td>
							<td valign="top"><?php echo $dt_row->no_kk; ?></td>
						</tr><tr>
							
							<td valign="top">Nama Pemilik</td>
							<td valign="top"> : </td>
							<td valign="top"><?php echo $dt_row->nm_pemilik; ?></td>
						</tr><tr>
							
							<td valign="top">Kecamatan</td>
							<td valign="top"> : </td>
							<td valign="top"><?php echo $dt_row->nm_kecamatan; ?></td>
						</tr><tr>
							
							<td valign="top">Kelurahan</td>
							<td valign="top"> : </td>
							<td valign="top"><?php echo $dt_row->nm_desa; ?></td>
						</tr><tr>
							
							<td valign="top">RW</td>
							<td valign="top"> : </td>
							<td valign="top">RW No <?php echo $dt_row->no_rw." - ".$dt_row->nm_rw; ?></td>
						</tr><tr>
							
							<td valign="top">RT</td>
							<td valign="top"> : </td>
							<td valign="top">RT No <?php echo $dt_row->no_rt." - ".$dt_row->nm_rt; ?></td>
						</tr><tr>
							
							<td valign="top">Jenis Kelamin</td>
							<td valign="top"> : </td>
							<td valign="top"><?php echo $dt_row->jenis_kelamin; ?></td>
						</tr><tr>
							
							<td valign="top">Status Pernikahan</td>
							<td valign="top"> : </td>
							<td valign="top"><?php echo $dt_row->status_pernikahan; ?></td>
						</tr><tr>
							
							<td valign="top">Prioritas</td>
							<td valign="top"> : </td>
							<td valign="top"><?php echo $dt_row->prioritas;?></td>
						</tr><tr>
							
							<td valign="top">Alamat</td>
							<td valign="top"> : </td>
							<td valign="top"><?php echo $dt_row->rtlh_almt;?></td>
						</tr><tr>
							
							<td valign="top">Keterangan</td>
							<td valign="top"> : </td>
							<td valign="top"><?php echo $dt_row->keterangan;?></td>
						</tr>
					</table>
                </div>
				<div class="col-md-3"><br>
					<h4 style="font-weight:bold; text-decoration:underline;">B.Pekerjaan & Penghasilan</h4>
					<table width="100%" cellpadding="25" cellspacing="5">
						<tr>
							<th>Atribut</th>
							<th width="10%">  </th>
							<th>Keterangan</th>
						</tr><tr>
							
							<td valign="top">Bantuan yang Diterima</td>
							<td valign="top"> : </td>
							<td valign="top"><?php echo $dt_row->bantuan_yang_diterima; ?></td>
						</tr><tr>
							
							<td valign="top">Pekerjaan</td>
							<td valign="top"> : </td>
							<td valign="top"><?php echo $dt_row->pekerjaan; ?></td>
						</tr><tr>
							
							<td valign="top">Intensitas Penghasilan</td>
							<td valign="top"> : </td>
							<td valign="top"><?php echo $dt_row->intensitas_penghasilan; ?></td>
						</tr><tr>
							
							<td valign="top">Rentang Penghasilan Bulanan</td>
							<td valign="top"> : </td>
							<td valign="top"><?php echo $dt_row->rentang_pengahasilan_bulanan; ?></td>
						</tr>
					</table><br>
					<h4 style="font-weight:bold; text-decoration:underline;">F.Kepemilikan Aset Non Lahan</h4>
					<table width="100%" cellpadding="25" cellspacing="5">
						<tr>
							
							<td valign="top">Kendaraan Roda Dua</td>
							<td valign="top"> : </td>
							<td valign="top"><?php echo $dt_row->kendaraan_roda_dua;?></td>
						</tr><tr>
							
							<td valign="top">Sarana Usaha</td>
							<td valign="top"> : </td>
							<td valign="top"><?php echo $dt_row->sarana_usaha;?></td>
						</tr><tr>
							
							<td valign="top">Hewan Ternak</td>
							<td valign="top"> : </td>
							<td valign="top"><?php echo $dt_row->hewan_ternak;?></td>
						</tr>
					</table>
                </div>
				<div class="col-md-3"><br>
					<h4 style="font-weight:bold; text-decoration:underline;">C.Kepemilikan</h4>
					<table width="100%" cellpadding="25" cellspacing="5">
						<tr>
							<th>Atribut</th>
							<th width="10%">  </th>
							<th>Keterangan</th>
						</tr><tr>
							
							<td valign="top">Luas Tanah</td>
							<td valign="top"> : </td>
							<td valign="top"><?php echo $dt_row->luas_tanah; ?></td>
						</tr><tr>
							
							<td valign="top">Status Tanah</td>
							<td valign="top"> : </td>
							<td valign="top"><?php echo $dt_row->status_tanah; ?></td>
						</tr><tr>
							
							<td valign="top">Status Penghunian Bangunan</td>
							<td valign="top"> : </td>
							<td valign="top"><?php echo $dt_row->status_penguhian_bangunan; ?></td>
						</tr><tr>
							
							<td valign="top">Kecamatan</td>
							<td valign="top"> : </td>
							<td valign="top"><?php echo $dt_row->nm_kecamatan; ?></td>
						</tr><tr>
							
							<td valign="top">Ruang Gerak Perjiwa</td>
							<td valign="top"> : </td>
							<td valign="top"><?php echo $dt_row->ruang_gerak_perjiwa; ?></td>
						</tr>
					</table>
                </div>
				<div class="col-md-3"><br>
					<h4 style="font-weight:bold; text-decoration:underline;">D.Dimensi, Tipe, dan Fungsi</h4>
					<table width="100%" cellpadding="25" cellspacing="5">
						<tr>
							<th>Atribut</th>
							<th width="10%">  </th>
							<th>Keterangan</th>
						</tr><tr>	
						<tr>
							
							<td valign="top">Luas Bangunan</td>
							<td valign="top"> : </td>
							<td valign="top"><?php echo $dt_row->luas_bangunan; ?></td>
						</tr><tr>
							
							<td valign="top">Tipe Bangunan</td>
							<td valign="top"> : </td>
							<td valign="top"><?php echo $dt_row->tipe_bangunan; ?></td>
						</tr><tr>
							
							<td valign="top">Status Fungsi Bangunan</td>
							<td valign="top"> : </td>
							<td valign="top"><?php echo $dt_row->fungsi_bangunan; ?></td>
						</tr>
					</table>
                </div>
            </div>
				
		</div>



        <div class="gap"></div>
		<footer id="main-footer">
            <?php include_once "layout_front/footer.php"; ?>
        </footer>
	   
        <script src="<?php echo base_url('asset/frontend'); ?>/js/jquery.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/bootstrap.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/slimmenu.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/bootstrap-timepicker.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/nicescroll.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/dropit.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/ionrangeslider.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/icheck.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/fotorama.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/typeahead.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/card-payment.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/magnific.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/owl-carousel.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/fitvids.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/tweet.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/countdown.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/gridrotator.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/custom.js"></script>
    </div>
</body>
<script>
$(function () {
if ($('#map-canvas').length) {
    var map,
        service;

    jQuery(function($) {
        $(document).ready(function() {
            var latlng = new google.maps.LatLng(<?php echo $dt_row->latitude; ?>, <?php echo $dt_row->longitude; ?>);
            var myOptions = {
                zoom: 16,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                scrollwheel: false
            };

            map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);


            var marker = new google.maps.Marker({
                position: latlng,
                map: map
            });
            marker.setMap(map);


            $('a[href="#google-map-tab"]').on('shown.bs.tab', function(e) {
                google.maps.event.trigger(map, 'resize');
                map.setCenter(latlng);
            });
        });
    });
}
});
</script>
</html>