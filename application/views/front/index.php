<!DOCTYPE HTML>
<?php foreach($instansi->result() as $is_row); ?>
<html>

<head>
    <title><?php echo $title; ?></title>


    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="Template, html, premium, themeforest" />
    <meta name="description" content="Traveler - Premium template for travel companies">
    <meta name="author" content="Tsoy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- GOOGLE FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600' rel='stylesheet' type='text/css'>
    <!-- /GOOGLE FONTS -->
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/icomoon.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/styles.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/mystyles.css">
    <script src="<?php echo base_url('asset/frontend'); ?>/js/modernizr.js"></script>

	<!-- /FAVICON---->
	<link rel="shortcut icon" href="<?php echo base_url("upload/$is_row->logo"); ?>"/>

  <script>
  	$('.carousel').carousel({
  		interval: 3000
  	})
  </script>
</head>

<body>

    <!-- FACEBOOK WIDGET -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- /FACEBOOK WIDGET -->
    <div class="global-wrap">
        <header id="main-header">
            <?php include_once "layout_front/header.php";  ?>
        </header>

        <!-- TOP AREA -->
        <div class="top-area show-onload" >
            <div class="bg-holder full">
                <div class="bg-front full-height bg-front-mob-rel">
                    <div class="container full-height">
                        <div class="col-sm-6">
                          <div style="margin:0 auto; padding:0 auto;" >
                            <h2>
                            <br>
                            Selamat datang
                            <br>
                            <br>
                            </h2>
                          </div>
                          <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                              <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                              <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                              <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                              <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                            </ol>
                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                              <div class="item active">
                                <img src="<?php echo base_url('asset/frontend/img/rtlh'); ?>/01.jpg" height="652" width="400" alt="...">
                              </div>
                              <div class="item">
                                <img src="<?php echo base_url('asset/frontend/img/rtlh'); ?>/02.jpg" height="652" width="400" alt="...">
                              </div>
                             <div class="item">
                                <img src="<?php echo base_url('asset/frontend/img/rtlh'); ?>/03.jpg" height="652" width="400" alt="...">
                              </div>
                              <div class="item">
                                 <img src="<?php echo base_url('asset/frontend/img/rtlh'); ?>/04.jpg" height="652" style="max-height:400px"alt="...">
                               </div>
                            </div>
                            <!-- Controls -->
                            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                              <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                              <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                              <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                              <span class="sr-only">Next</span>
                            </a>
                          </div>
                        </div>
                        <div class="col-sm-6">
                          <div class="rel full-height" >
                              <div style="margin:0 auto; padding:0 auto;" >
                								<h2>
                								<br>
                								<br>
                								<br>
                                </h2>
                              </div>
                              <div class="search-tabs search-tabs-bg " style="padding:0;">
                                  <div class="tabbable">
                                      <ul class="nav nav-tabs" id="myTab">
                                          <li class="active"><a href="#tab-1" data-toggle="tab"><i class="fa fa-search"></i> <span >Cari Data RTLH</span></a>
                                          </li>
                                      </ul>
                                      <div class="tab-content">
                                          <div class="tab-pane fade in active" id="tab-1">
                                              <h3>Silahkan isi dan cari data rumah tidak layak huni disini.</h3>
                                              <form action="<?php echo site_url('front/cari_rtlh/baru') ?>" method="post">
                                                  <div class="row">
                                                      <div class="col-md-6">
                                                          <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-globe input-icon"></i>
                                                              <label>Kecamatan</label>
                              															<select name="kecamatan" id="kecamatan" class="form-control" required>
                              																<option value="">Pilih Kecamatan disini</option>
                              																<?php foreach($kecamatan->result() as $kec){ ?>
                              																	<option value="<?php echo $kec->id_kecamatan; ?>"><?php echo $kec->nm_kecamatan; ?></option>
                              																<?php } ?>
                              															</select>
                                                          </div>
                                                      </div>
                          													<div class="col-md-6">
                          														<div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-globe input-icon"></i>
                          															<label>Kelurahan</label>
                          															<select name="kelurahan" id="kelurahan" class="form-control" required disabled>
                          																<option value="">Pilih Kelurahan disini</option>
                          															</select>
                          														</div>

                          													</div>
                                                  </div>
                                                  <button class="btn btn-primary btn-lg" type="submit">Cari</button>
                                              </form>
                                          </div>
  									                  </div>
                                  </div>
                              </div>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="bg-img hidden-lg" style="background-image:url(<?php echo base_url('') ?>/upload/img_slider/pic4.jpg);"></div>
                <div class="bg-mask hidden-lg"></div>
            </div>
        </div>
        <!-- END TOP AREA  -->
        <footer id="main-footer">
            <?php include_once "layout_front/footer.php"; ?>
        </footer>

        <script src="<?php echo base_url('asset/frontend') ?>/js/jquery.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/bootstrap.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/slimmenu.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/datepicker.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/bootstrap-timepicker.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/nicescroll.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/dropit.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/ionrangeslider.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/icheck.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/fotorama.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/typeahead.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/card-payment.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/magnific.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/owl-carousel.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/fitvids.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/tweet.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/countdown.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/gridrotator.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/custom.js"></script>
    </div>
</body>

</html>


<script type="text/javascript">
$(function () {
	$("#start_date").datepicker({
		format: 'yyyy-mm-dd'
	})
	$("#end_date").datepicker({
		format: 'yyyy-mm-dd'
	})
	$("#kecamatan").change(function(){
		var id_kecamatan = $("#kecamatan option:selected").val();
		$.ajax({
			url: "<?php echo site_url('backend/desa2')?>",
			type: "POST",
			data	: "id_kecamatan="+id_kecamatan,
			success : function (msg) {
				document.getElementById("kelurahan").disabled = false;
				$("#kelurahan").html(msg);
				//$("#kelurahan").css("color","black");
			}
		});
	});
	$("#kelurahan").change(function(){
		var id_kelurahan = $("#kelurahan option:selected").val();
		$.ajax({
			url: "<?php echo site_url('backend/rw2')?>",
			type: "POST",
			data	: "id_kelurahan="+id_kelurahan,
			success : function (msg) {
				if(msg == 'kosong'){
					alert("Maaf data RW dari kelurahan yang dipilih belum tersedia, silahkan ganti pilihan kelurahan lain");
				}else{
					document.getElementById("rw").disabled = false;
					$("#rw").html(msg);
				}
			}
		});
	});
	$("#rw").change(function(){
		var rw = $("#rw option:selected").val();
		$.ajax({
			url: "<?php echo site_url('backend/rt2')?>",
			type: "POST",
			data	: "no_rw="+rw,
			success : function (msg) {
				if(msg == 'kosong'){
					alert("Maaf data RT dari RW yang dipilih belum tersedia, silahkan ganti pilihan RW lain");
				}else{
					$("#rt").html(msg);
					document.getElementById("rt").disabled = false;
				}
			}
		});
	});
});
</script>
