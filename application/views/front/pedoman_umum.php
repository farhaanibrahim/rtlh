<?php foreach($instansi->result() as $is_row); ?>
<!DOCTYPE HTML>
<html>

<head>
    <title><?php echo $title; ?></title>


    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="Template, html, premium, themeforest" />
    <meta name="description" content="Traveler - Premium template for travel companies">
    <meta name="author" content="Tsoy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- GOOGLE FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600' rel='stylesheet' type='text/css'>
    <!-- /GOOGLE FONTS -->
     <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/icomoon.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/styles.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/mystyles.css">
    <script src="<?php echo base_url('asset/frontend'); ?>/js/modernizr.js"></script>

	<!-- /FAVICON---->
	<link rel="shortcut icon" href="<?php echo base_url("upload/$is_row->logo"); ?>"/>

</head>

<body>

    <!-- FACEBOOK WIDGET -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- /FACEBOOK WIDGET -->
    <div class="global-wrap">

		<header id="main-header">
            <?php include_once "layout_front/header.php";  ?>
        </header>

        <div class="container">
		<br>
		<br>
		<br>
        </div>




        <div class="container">
          <p class="western" align="center" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <span style="display: inline-block; border: 1.00pt solid #000001; padding: 0.01in"><font face="Cambria, serif"><font size="4" style="font-size: 16pt"><b><font color="#0070c0">BAB
          1</span></b></font></font></font></p>
          <p class="western" align="center" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <span style="display: inline-block; border: 1.00pt solid #000001; padding: 0.01in"><font face="Cambria, serif"><font size="4" style="font-size: 16pt"><b><font color="#0070c0">PENDAHULUAN</span></b></font></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <br/>

          </p>
          <p class="western" align="justify" style="margin-left: 0.79in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>1.1.</b></font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>
          </b></font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>LATAR</b></font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>BELAKANG</b></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in"><a name="_GoBack"></a>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Sebagaimana
          diamanatkan dalam pasal 28 H Amandemen UUD 1945, rumah merupakan
          salah satu hak dasar rakyat, oleh karena itu setiap warga negara
          berhak untuk mendapatkan tempat tinggal dan lingkungan hidup yang
          baik dan sehat.Selain itu, rumah juga merupakan kebutuhan dasar
          manusia dalam meningkatkan harkat, martabat, mutu kehidupan, dan
          penghidupan serta sebagai pencerminan diri pribadi dalam upaya
          peningkatan taraf hidup, pembentukan watak, karakter, dan kepribadian
          bangsa.</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <br/>

          </p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Menurut
          data statistik sebagian besar masyarakat membangun rumahnya secara
          swadaya, artinya perumahan swadaya menjadi tumpuan sebagian besar
          rakyat Indonesia. Berdasarkan data Badan Pusat Statistik (BPS) tahun
          2010 terdapat 7,9 juta unit Rumah Tidak Layak Huni (RTLH) yang mana
          sebanyak 2,9 juta unit berada di perkotaan dan 5 juta unit berada di
          perdesaan. Permasalahan kemiskinan di Indonesia sudah sangat mendesak
          untuk ditangani. Khususnya di wilayah yang sulit dijangkau oleh
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">emerintah,
          salah satu ciri umum dari kondisi fisik masyarakat miskin adalah
          tidak memiliki akses prasarana dan sarana dasar lingkungan yang
          memadai, dengan kualitas perumahan dan permukiman yang jauh dibawah
          standar kelayakan, serta mata pencaharian yang tidak menentu. </font></font>
          </p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <br/>

          </p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Disisi
          lain, berbagai program terkait peningkatan kualitas rumah tidak layak
          huni dari beberapa kementerian/lembaga (K/L) dan non KL pun seperti
          Program </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>Quick
          Wins</i></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">,
          Pandu Gerbang Kampung-Menkokesra, Program PKH (Program Keluarga
          Harapan), Kementerian Kelautan dan Perikanan (KKP), Program
          Pengembangan Desa Tertinggal, Program Aspirasi dan Program LVRI telah
          marak dilaksanakan dengan berbagai sumber pendanaan. Namun dalam
          pelaksanaannya, seringkali mengalami kendala yang muaranya adalah
          keterbatasan database </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">RTLH
          </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">yang
          menjadikan pelaksanaan program-program tersebut tidak tepat sasaran,
          tidak tepat anggaran, tidak tepat waktu.</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <br/>

          </p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Perlu
          dipahami bahwa kondisi data Perumahan yang ada saat ini sebagian
          besar merupakan data makro. Dalam konteks data kelompok sasaran dan
          ob</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">j</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ek
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">RTLH</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">,
          data makro merupakan data agregat tentang jumlah dan persentase RTLH
          dan masyarakat miskin serta variabel lainnya pada tingkat&nbsp;
          nasional dan wilayah (pro</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">v</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">insi
          dan kabupaten/kota). Sumber data makro sebagian besar bersumber dari
          BPS yang dalam hal ini BPS merupakan institusi yang menyediakan data
          dalam lingkup nasional. Contoh data makro di antaranya adalah
          statistic perumahan dan permukiman tahun 2007 yang menyediaan data
          makro terkait perumahan dan permukiman dalam skala provinsi.</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <br/>

          </p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Sementara,
          untuk menjamin pelaksanaan program peningkatan kualitas hunian dan
          permukiman untuk lebih layak huni lebih tepat sasaran, penggunaan,
          dan tepat waktu, maka dibutuhkan data mikro yang lebih operasional.
          Data mikro yang idealnya mampu menyajikan informasi yang lebih
          spesifik terkait kelompok sasaran MBR dan objek RTLH dan direkap
          dalam unit administrasi terendah (misalnya RT/ RW atau desa/
          kelurahan). Data ini yang bersifat mikro ini lebih operasional dalam
          mengidentifikasi kelompok sasaran dan objek RTLH itu sendiri, yakni
          pemilik RTLH (seperti nama KK, alamat dan jumlah penghasilan) serta
          karakter fisik dari RTLH itu sendiri (kualitas bangunan, luas
          bangunan, ketersediaan sanitasi, dll).</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <br/>

          </p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Berdasarkan
          fakta di</font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt">atas,
          bahwa dalam kegiatan penyediaan rumah layak huni, kebutuhan basis
          data RTLH dalam skala mikro menjadi kebutuhan yang nyata dan perlu
          diprioritaskan ketersediaannya. Sejumlah data&nbsp; yang memuat
          informasi mengenai data spasial/peta rumah yang tidak memenuhi
          kriteria layak huni desa terdesa yang dapat digunakan sebagai acuan
          dalam berbagai program peningkatan kualitas permukiman layak huni
          di</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">tingkat
          kabupaten/kota</span></font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt">dengan
          sumbe</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">r
          </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">pendanaan
          dari APBN/APBD (</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">rovinsi/k</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">abupaten</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">/k</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">ota</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">)
          maupun bantuan pihak ketiga melalui Program CSR dan atau PKBL BUMN.</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <br/>

          </p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>1.2.MAKSUD-TUJUAN-SASARAN</b></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><b>1.2.1.</b></font><span style="font-variant: normal"><font face="Cambria, serif"><b>
          </b></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>MAKSUD</b></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Maksud
          kegiatan</font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt">ini
          adalah menyediakan basis data RTLH yang valid dan </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>up
          to date</i></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          dalam skala mikro (desa</font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt">per
          desa) dan skala makro (</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">kabupaten/kota)</span></font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">untuk:</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">1.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="nb-NO">Memudahkan
          dan membantu pemerintah</span></font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt">pusat-daerah</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="nb-NO">,
          masyarakat dan pemangku kepentingan lain</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">nya</font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="nb-NO">(</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="nb-NO"><i>stakeholders</i></span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="nb-NO">),&nbsp;
          </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">untuk</font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="nb-NO">dapat
          berpartisipasi dan berkontribusi </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">dalam
          peningkatan kualitas rumah </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">dan
          </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">permukiman
          termasuk RTLH di setiap </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">kabupaten/kota</span></font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">secara
          optimal.</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">2.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Landasan
          pelaksanaan upaya peningkatan kualitas rumah dan permukiman di
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">t</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ingkat
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">k</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">abupaten/</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">k</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ota
          yang memerlukan peran serta dari berbagai lini </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">dan
          </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">lintas
          sektoral.</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><b>1.2.2.</b></font><span style="font-variant: normal"><font face="Cambria, serif"><b>
          </b></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>TUJUAN</b></font></font>
          </p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Tujuan
          dari pelaksanaan program pendataan RTLH ini adalah&nbsp; penyusunan
          profil database baik deskriptif maupun spasial RTLH berupa data makro
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">dan
          </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">mikro
          RTLH</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">.</span></font></font>&nbsp;
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Selain
          itu, mengingat dalam pelaksanaan pendataan ini membutuhkan realisasi
          komitmen peran serta dari pemerintah daerah dalam menjalankan urusan
          wajibnya di </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">b</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">idang
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">pe</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">rumahan,
          dan peran serta dari masyarakat untuk menjalankan pendataan RTLH di
          masing-masing wilayah </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">k</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">elurahan/desanya,
          maka di akhir kegiatan ini diharapkan tercapainya kondisi </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">sebagai
          berikut</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">:
          </font></font>
          </p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">1.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Komitmen</font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">dan
          </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">kemampuan/kapasitas
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">pemerintah
          daerah</span></font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt">(</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">provinsi-kabupaten/kota</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">)
          dalam:</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">a.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">menyediakan
          basis data RTLH sebagai salah</font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt">satu
          urusan wajib bidang perumahan; dan</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">b.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">menyediakan-mengupdate
          database RTLH secara mandiri.</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">2.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Kemampuan
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">m</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">asyarakat
          desa untuk melakukan pembaharuan data RTLH-PKP secara mandiri melalui
          pemetaan swadaya dan </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>Community
          Action Plan.</i></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <br/>

          </p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><b>1.2.3.</b></font><span style="font-variant: normal"><font face="Cambria, serif"><b>
          </b></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>SASARAN</b></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Pelaksanaan
          kegiatan penyusunan </font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">database</font></font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">RTLH
          ini adalah agar </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">elaksanaan
          peningkatan kualitas/perbaikan RTLH ini dapat tepat sasaran,
          penggunaan, anggaran dan waktu sehingga amanat hak setiap masyarakat
          untuk memperoleh rumah dan permukiman yang layak huni dapat
          terpenuhi.</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <br/>

          </p>
          <p class="western" align="justify" style="margin-right: 0.39in; margin-bottom: 0in">
                 <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>1.3.PENGGUNA
          DAN PRINSIP-STRUKTUR SUBTANSI</b></font></font></p>
          <p class="western" align="justify" style="margin-right: 0.39in; margin-bottom: 0in">
          <br/>

          </p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><b>1.3.1.</b></font><span style="font-variant: normal"><font face="Cambria, serif"><b>
          </b></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>PENGGUNA</b></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Buku
          ini dapat digunakan oleh </font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">setiap</font></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          stakeholder yang terlibat dalam kegiatan pendataan RTLH, mulai dari
          tingkat pusat, provinsi, </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">kabupaten/kota</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">,
          kecamatan sampai kepada </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">t</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ingkat
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">kelurahan/desa</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">.
          </font></font>
          </p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <br/>

          </p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><b>1.3.2.</b></font><span style="font-variant: normal"><font face="Cambria, serif"><b>
          </b></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>PRINSIP
          SUBSTANSI</b></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Buku
          ini </font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">merupakan</font></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">Pedoman
          Umum Pendataan RTLH</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">,
          yakni penjabaran secara umum yang berisi rambu-rambu utama sebagai
          acuan dalam pelaksanaan kegiatan pendataan Rumah Tidak Layak Huni
          (RTLH).</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <br/>

          </p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><b>1.3.3.</b></font><span style="font-variant: normal"><font face="Cambria, serif"><b>
          </b></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>STRUKTUR
          SUBSTANSI</b></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Buku
          Pedoman ini terbagi menjadi 4 Bab, yakni:</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>BAB
          1 PENDAHULUAN, </b></font></font>
          </p>
          <p class="western" align="justify" style="margin-left: 0.89in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Berisi
          mengenai latar</font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt">belakang,
          maksud-tujuan-sasaran, dan pengguna, prinsip substansi pedoman dan
          strukturnya.</font></font></p>
          <p class="western" align="justify" style="margin-right: 0.39in; margin-bottom: 0in">
                 <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>BAB
          2 OB</b></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US"><b>J</b></span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>EK
          PENDATAAN: RTLH dan SEGMENTASI MBR</b></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.89in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Berisi
          mengenai terminologi Rumah Tidak Layak Huni (RTLH) dan Indikatornya
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">dan
          </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Segmentasi
          pemilik RTLH yang difokuskan kepada kelas MBR (masyarakat
          berpenghasilan Rendah).</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>BAB
          3 PROGRAM PENDATAAN RTLH</b></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.89in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Berisi
          beberapa hal yang mendasari pelaksanaan program yang mencakup
          pendekatan program, komponen program, prasyarat program serta prinsip
          pelaksanaan program.</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>BAB
          4 TAHAPAN UMUM PROGRAM PENDATAAN RTLH</b></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.89in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Berisi
          uraian gambaran umum perihal tahapan kegiatan yang dilaksanakan dalam
          program, mulai dari kegiatan di </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">tingkat
          pusat</span></font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt">(PUPERA),
          tingkat pemerintah daerah (provinsi, </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">kabupaten/kota</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">/kecamatan)
          sampai kepada kegiatan pendataan yang dilaksanakan di</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">t</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ingkat
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">k</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">elurahan/desa.</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.89in; margin-right: 0.39in; margin-bottom: 0in">
          <br/>

          </p>
          <p class="western" align="center" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <span style="display: inline-block; border: 1.00pt solid #000001; padding: 0.01in"><font face="Cambria, serif"><font size="4" style="font-size: 16pt"><b><font color="#0070c0">BAB
          2</span></b></font></font></font></p>
          <p class="western" align="center" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <span style="display: inline-block; border: 1.00pt solid #000001; padding: 0.01in"><font face="Cambria, serif"><font size="4" style="font-size: 16pt"><b><font color="#0070c0">RTLH
          </b></font></font></font><font face="Cambria, serif"><font size="4" style="font-size: 16pt"><span lang="en-ID"><b><font color="#0070c0">DAN</b></span></font></font></font><font color="#0070c0">
          </font><font face="Cambria, serif"><font size="4" style="font-size: 16pt"><b><font color="#0070c0">SEGMENTASI
          MBR OBJEK PENDATAAN</span></b></font></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Ob</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">j</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ek
          pendataan yang dimaksud didalam kegiatan ini adaah Rumah Tidak Layak
          Huni (RTLH) dan segmentasi Masyarakat Berpenghasilan Rendah (MBR).</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <br/>

          </p>
          <p class="western" align="justify" style="margin-left: 0.69in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><b>2.1.</b></font><span style="font-variant: normal"><font face="Cambria, serif"><b>
          </b></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>RUMAH
          TIDAK LAYAK HUNI</b></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.69in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><b>2.1.1.</b></font><span style="font-variant: normal"><font face="Cambria, serif"><b>
          </b></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>PENGERTIAN</b></font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US"><b>RUMAH
          TIDAK LAYAK HUNI</b></span></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Berdasarkan</font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="pt-BR">Permenpera
          RI </span></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">No</font></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="pt-BR">.
          22/PERMEN/M/2008 tentang Standar Pelayanan Minimal&nbsp; (SPM) Bidang
          Perumahan Rakyat Daerah Provinsi dan Kab. /Kota yang dimaksud dengan
          </span></font></font><strong><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="pt-BR"><span style="font-weight: normal">RUMAH
          LAYAK HUNI (RLH) </span></span></font></font></strong><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="pt-BR">adalah</span></font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">R</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="pt-BR">umah
          yang memenuhi persyaratan keselamatan bangunan dan kecukupan minimum
          luas bangunan serta kesehatan penghuninya</span></font></font><font color="#365f91"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">.</font></font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Kriteria
          rumah layak huni ini tidak menghilangkan penggunaan teknologi dan
          bahan bangunan daerah setempat sesuai kearifan lokal daerah untuk
          menggunakan teknologi dan bahan bangunan dalam membangun rumah layak
          huni. </font></font>
          </p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Berdasarkan
          pengertian tersebut, maka yang dimaksud dengan </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">Rumah
          Tidak Layak Huni </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">adalah
          Rumah yang&nbsp; tidak memenuhi persyaratan : </font></font>
          </p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">RTLH
          = KSB + KHP + KLB + KML dan RTLH &lt; RLH</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Dimana
          :</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">RTLH
          : Rumah Tidak Layak Huni</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">KSB
          : Syarat Keselamatan bangunan</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">KH
          : Syarat Kesehatan penghuni</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">KLB
          : Syarat Kecukupan min</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">imal</span></font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Luas
          bangunan/jiwa</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">KML
          : Syarat Komponen material sesuai konteks lokal</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Kriteria
          rumah layak huni ini tidak menghilangkan penggunaan teknologi dan
          bahan bangunan daerah setempat sesuai kearifan lokal daerah untuk
          menggunakan teknologi dan bahan bangunan dalam membangun rumah layak
          huni.</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <br/>

          </p>
          <p class="western" align="justify" style="margin-left: 0.69in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><b>2.1.2.</b></font><span style="font-variant: normal"><font face="Cambria, serif"><b>
          </b></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>REFERENSI
          DASAR</b></font></font>
          </p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Beberapa
          perihal indikator Rumah Layak Huni adalah:</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">1.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">SNI
          03-1979-1990, Spesifikasi matra ruang untuk rumah dan gedung</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">;</span></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">2.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">SNI
          03-1728-1989 (Tata Cara Pelaksanaan Mendirikan Bangunan Gedung)</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">;</span></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">3.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">UU
          No 28 tahun 2002 Tentang Bangunan Gedung</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">;</span></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">4.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Permenpera
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">N</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">o
          22 tahun 2008 tentang Standar Pelayanan Minimum Bidang Perumahan
          Rakyat</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">;</span></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">5.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Peraturan
          Menteri Negara Perumahan Rakyat Nomor: 08/PERMEN/M/2007 tentang
          Pedoman Pembangunan Perumahan Swadaya;</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">6.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Keputusan
          Menteri Permukiman dan Prasarana Wilayah Nomor: 403/KPTS/M/2002
          tentang Pedoman Teknis Pembangunan Rumah Sederhana Sehat (Rs Sehat);
          dan</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">7.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Peraturan
          Menteri Kesehatan RI Nomor 1077/Menkes/PER/V/2011 tentang Pedoman
          Penyehatan Udara dalam Ruang Rumah</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">.</span></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <br/>

          </p>
          <p class="western" align="justify" style="margin-left: 0.69in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><b>2.1.3.</b></font><span style="font-variant: normal"><font face="Cambria, serif"><b>
          </b></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>PERSYARATAN
          TEKNIS</b></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Berikut
          adalah persyaratan teknis dari Rumah Layak Huni (RLH) yang mencakup
          aspek keselamatan, aspek kesehatan, dan aspek kecukupan luas ruang
          minimum - serta komponen material bangunan.</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><b>1.</b></font><span style="font-variant: normal"><font face="Cambria, serif"><b>
          </b></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Aspek
          Keselamatan Bangunan mencakup persyaratan struktur bawah (pondasi),
          persyaratan struktur tengan (kolom-balok) dan persyaratan atas (atap)</font></font></p>
          <center>
            <table width="380" cellpadding="2" cellspacing="0">
              <col width="372">
              <thead>
                <tr>
                  <td width="372" valign="top" bgcolor="#95b3d7" style="border: 1.50pt solid #365f91; padding-top: 0.02in; padding-bottom: 0.02in; padding-left: 0.05in; padding-right: 0.08in">
                    <p class="western" align="justify"><font face="Cambria, serif"><font size="2" style="font-size: 10pt"><b>Kotak
                    2.1.</b></font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="2" style="font-size: 10pt"><b>
                    </b></font></font></span><font face="Cambria, serif"><b>Persyaratan
                    Struktur Bawah (Pondasi)</b></font></p>
                  </td>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td width="372" valign="top" bgcolor="#ffffff" style="border: 1.50pt solid #365f91; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.05in; padding-right: 0.08in">
                    <p class="western" align="justify" style="margin-left: 0.25in"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">a</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
                    </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Pondasi
                    harus ditempatkan pada tanah yang mantap, yaitu ditempatkan pada
                    tanah keras, dasar pondasi diletakkan lebih dalam dari 45 cm
                    dibawah permukaan tanah.</font></font></p>
                    <p class="western" align="justify" style="margin-left: 0.25in"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">b.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
                    </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Pondasi
                    harus dihubungkan dengan balok </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">penghubung
                    </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US"><i>(sloof).
                    </i></span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">Balok
                    penghubung dapat terbuat dari kayu, beton bertulang, atau baja.</span></font></font></p>
                    <p class="western" align="justify" style="margin-left: 0.25in"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">c.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
                    </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Pondasi
                    tidak diletakkan terlalu dekat dengan dinding tebing. Untuk
                    mencegah longsor, tebing diberi dinding penahan yang terbuat
                    dari pasangan atau turap bambu maupun kayu.</font></font></p>
                    <p class="western" align="justify" style="margin-left: 0.25in"><br/>

                    </p>
                  </td>
                </tr>
                <tr>
                  <td width="372" valign="top" bgcolor="#95b3d7" style="border: 1.50pt solid #365f91; padding-top: 0.02in; padding-bottom: 0.02in; padding-left: 0.05in; padding-right: 0.08in">
                    <p class="western" align="justify"><font face="Cambria, serif"><font size="2" style="font-size: 10pt"><b>Kotak
                    2.2.</b></font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="2" style="font-size: 10pt"><b>
                    </b></font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>Persyaratan
                    Struktur Tengah</b></font></font></p>
                  </td>
                </tr>
                <tr>
                  <td width="372" valign="top" bgcolor="#ffffff" style="border: 1.50pt solid #365f91; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.05in; padding-right: 0.08in">
                    <p class="western" align="justify" style="margin-left: 0.25in"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">a.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
                    </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Bangunan
                    harus menggunakan kolom sebagai rangka pemikul.</font></font></p>
                    <p class="western" align="justify" style="margin-left: 0.25in"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">b.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
                    </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Rangka
                    bangunan (kolom, ring balok, dan sloof) harus memiliki hubungan
                    yang kuat dan kokoh</font></font></p>
                    <p class="western" align="justify" style="margin-left: 0.25in"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">c.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
                    </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Kolom
                    dapat terbuat dari kayu, beton bertulang, atau baja.</font></font></p>
                    <p class="western" align="justify" style="margin-left: 0.25in"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">d.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
                    </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Kolom
                    harus </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">dihubungkan
                    dengan kuat pada pondasi</span></font></font></p>
                    <p class="western" align="justify" style="margin-left: 0.25in"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">e.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
                    </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Pada
                    bagian akhir atau setiap kolom harus diikat dan disatukan dengan
                    balok keliling/ring balok dari kayu, beton bertulang atau baja</font></font></p>
                    <p class="western" align="justify" style="margin-left: 0.25in"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">f.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
                    </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Pada
                    rumah panggung antara tiang kayu harus diberi ikatan diagonal.</font></font></p>
                  </td>
                </tr>
                <tr>
                  <td width="372" valign="top" bgcolor="#95b3d7" style="border: 1.50pt solid #365f91; padding-top: 0.02in; padding-bottom: 0.02in; padding-left: 0.05in; padding-right: 0.08in">
                    <p class="western" align="justify"><font face="Cambria, serif"><font size="2" style="font-size: 10pt"><b>Kotak
                    2.3.</b></font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="2" style="font-size: 10pt"><b>
                    </b></font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>Persyaratan
                    Struktur Atas</b></font></font></p>
                  </td>
                </tr>
                <tr>
                  <td width="372" valign="top" bgcolor="#ffffff" style="border: 1.50pt solid #365f91; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.05in; padding-right: 0.08in">
                    <p class="western" align="justify" style="margin-left: 0.25in"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">a</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
                    </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Rangka
                    </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">atap</span></font></font>
                    <font face="Cambria, serif"><font size="3" style="font-size: 12pt">harus
                    kuat menahan beban atap</font></font></p>
                    <p class="western" align="justify" style="margin-left: 0.25in"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">b</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
                    </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Rangka
                    </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">atap</span></font></font>
                    <font face="Cambria, serif"><font size="3" style="font-size: 12pt">harus
                    diangker pada kedudukannya (pada kolom atau ring balok).</font></font></p>
                  </td>
                </tr>
              </tbody>
            </table>
          </center>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <br/>

          </p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><b>2.</b></font><span style="font-variant: normal"><font face="Cambria, serif"><b>
          </b></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>Aspek
          Kesehatan </b></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">mencakup
          persyaratan pencahayaan, penghawaan, dan utilitas rumah.</font></font></p>
          <center>
            <table width="380" cellpadding="2" cellspacing="0">
              <col width="372">
              <thead>
                <tr>
                  <td width="372" valign="top" bgcolor="#95b3d7" style="border: 1.50pt solid #365f91; padding-top: 0.02in; padding-bottom: 0.02in; padding-left: 0.05in; padding-right: 0.08in">
                    <p class="western" align="justify"><font face="Cambria, serif"><font size="2" style="font-size: 10pt"><b>Kotak
                    2.4.</b></font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="2" style="font-size: 10pt"><b>
                    </b></font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>Persyaratan
                    Pencahayaan</b></font></font></p>
                  </td>
                </tr>
                <tr>
                  <td width="372" valign="top" bgcolor="#ffffff" style="border: 1.50pt solid #365f91; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.05in; padding-right: 0.08in">
                    <p class="western" align="justify" style="margin-left: 0.25in"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">a</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
                    </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Sinar
                    matahari langsung dapat masuk ke ruangan utama minimum 1 (satu)
                    jam setiap hari.</font></font></p>
                    <p class="western" align="justify" style="margin-left: 0.25in"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">b</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
                    </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Pencahayaan
                    alami dan buatan di dalam ruang rumah diusahakan sesuai dengan
                    kebutuhan untuk melihat benda sekitar dan membaca.</font></font></p>
                    <p class="western" align="justify" style="margin-left: 0.25in"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">c</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
                    </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Luas
                    jendela/lubang dinding minimal 10% dari dinding yang berhadapan
                    dengan ruang terbuka.</font></font></p>
                  </td>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td width="372" valign="top" bgcolor="#95b3d7" style="border: 1.50pt solid #365f91; padding-top: 0.02in; padding-bottom: 0.02in; padding-left: 0.05in; padding-right: 0.08in">
                    <p class="western" align="justify"><font face="Cambria, serif"><font size="2" style="font-size: 10pt"><b>Kotak
                    2.5.</b></font></font></p>
                    <p class="western" align="justify"><span style="font-variant: normal">
                    </span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>Persyaratan
                    Penghawaan</b></font></font></p>
                  </td>
                </tr>
                <tr>
                  <td width="372" valign="top" bgcolor="#ffffff" style="border: 1.50pt solid #365f91; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.05in; padding-right: 0.08in">
                    <p class="western" align="justify" style="margin-left: 0.25in"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">a.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
                    </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Rumah
                    harus dilengkapi dengan ventilasi, minimal 10% luas lantai
                    dengan sistem ventilasi silang</font></font></p>
                    <p class="western" align="justify" style="margin-left: 0.25in"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">b.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
                    </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Lubang
                    penghawaan keluar tidak mengganggu kenyamanan bangunan
                    disekitarnya.</font></font></p>
                  </td>
                </tr>
                <tr>
                  <td width="372" valign="top" bgcolor="#95b3d7" style="border: 1.50pt solid #365f91; padding-top: 0.02in; padding-bottom: 0.02in; padding-left: 0.05in; padding-right: 0.08in">
                    <p class="western" align="justify">        <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US"><b>Kotak
                    2.6. </b></span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>Persyaratan
                    Utilitas</b></font></font></p>
                  </td>
                </tr>
                <tr>
                  <td width="372" valign="top" bgcolor="#ffffff" style="border: 1.50pt solid #365f91; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.05in; padding-right: 0.08in">
                    <p class="western" align="justify" style="margin-left: 0.25in"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">a</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
                    </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Setiap
                    rumah memiliki minimal 1 kamar mandi dan jamban didalam atau
                    luar bangunan rumah dan dilengkapi bangunan bawah septik</font></font>
                    <font face="Cambria, serif"><font size="3" style="font-size: 12pt">tank
                    atau dengan sanitasi komunal.</font></font></p>
                    <p class="western" align="justify" style="margin-left: 0.25in"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">b</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
                    </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Apabila
                    tersedia pembuangan air limbah kota atau s</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">i</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">stem
                    air limbah lingkungan, maka setiap rumah berhak mendapat
                    sambungan.</font></font></p>
                    <p class="western" align="justify" style="margin-left: 0.25in"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">c</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
                    </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Apabila
                    tidak tersedia sistem pembuangan air limbah kota atau sistem air
                    limbah lingkungan, setiap rumah harus dilengkapi dengan tangki
                    septik dan bidang resapan, atau tangki septik dengan sistem
                    serapan, sesuai SNI No. 03-2398-2002 tentang </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>Tata
                    cara perencanaan tangki septik dengan sistem serapan</i></font></font></p>
                    <p class="western" align="justify" style="margin-left: 0.25in"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">d</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
                    </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Apabila
                    tersedia sistem pembuangan air hujan kota atau sistem pembuangan
                    air hujan lingkungan, tiap rumah berhak mendapat sambungan.</font></font></p>
                    <p class="western" align="justify" style="margin-left: 0.25in"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">e</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
                    </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Jika
                    tidak tersedia sistem pembuangan air hujan kota, setiap rumah&nbsp;
                    harus memiliki sumur resapan yang berfungsi, sesuai dengan SNI
                    No 03 2453-2002, tentang </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>Tata
                    cara perencanaan teknik sumur resapan air hujan untuk lahan
                    pekarangan</i></font></font></p>
                    <p class="western" align="justify" style="margin-left: 0.25in"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">f</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
                    </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Setiap
                    rumah harus dilengkapi dengan system plambing untuk air bersih,
                    pembuangan air limbah dan pembuangan air hujan sesuai dengan SNI
                    03-6481-2000 tentang </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>Sistem
                    plambing</i></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">.</span></font></font></p>
                    <p class="western" align="justify" style="margin-left: 0.25in"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">g</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
                    </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Apabila
                    tersedia sistem penyediaan air bersih kota atau sistem
                    penyediaan lingkungan, maka tiap rumah berhak mendapat sambungan
                    atau sambungan halaman. Penyediaan air bersih dapat dilakukan
                    dengan sumur pompa dangkal atau sumur gali dengan jarak minimum
                    10 meter dari tangki septik dan bidang resapannya.</font></font></p>
                    <p class="western" align="justify" style="margin-left: 0.25in"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">h.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
                    </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Syarat
                    air minum yaitu Fisik: Jernih, tidak berasa, tidak berbau, suhu
                    &lt; suhu udara (sejuk), kekeruhan &lt; 1mg/liter. Kimia: tidak
                    mengandung racun, bahan organik, zat mineral yang berbahaya</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">.</span></font></font></p>
                  </td>
                </tr>
              </tbody>
            </table>
          </center>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <br/>

          </p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><b>3.</b></font><span style="font-variant: normal"><font face="Cambria, serif"><b>
          </b></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>Luas
          </b></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US"><b>dan
          </b></span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>Kebutuhan
          Ruang </b></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">mencakup
          ketentuan kecukupan luas, ketentuan organisasi ruang.</font></font></p>
          <center>
            <table width="380" cellpadding="2" cellspacing="0">
              <col width="372">
              <tr>
                <td width="372" valign="top" bgcolor="#95b3d7" style="border: 1.50pt solid #365f91; padding-top: 0.02in; padding-bottom: 0.02in; padding-left: 0.05in; padding-right: 0.08in">
                  <p class="western" align="justify" style="margin-left: 0.81in"><font face="Cambria, serif"><font size="2" style="font-size: 10pt"><b>Kotak
                  2.7.</b></font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="2" style="font-size: 10pt"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  </b></font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>Persyaratan
                  Kecukupan Luas Ruang/Jiwa</b></font></font></p>
                </td>
              </tr>
              <tr>
                <td width="372" valign="top" bgcolor="#ffffff" style="border: 1.50pt solid #365f91; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.05in; padding-right: 0.08in">
                  <p class="western" align="justify" style="margin-left: 0.25in"><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">a.
                  </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Ketentuan
                  luas minimal rumah layak huni antara 7,2 m</font></font><sup><font face="Cambria, serif"><font size="3" style="font-size: 12pt">2</font></font></sup><font face="Cambria, serif"><font size="3" style="font-size: 12pt">/orang
                  sampai dengan 12 m</font></font><sup><font face="Cambria, serif"><font size="3" style="font-size: 12pt">2</font></font></sup><font face="Cambria, serif"><font size="3" style="font-size: 12pt">/orang.
                  </font></font>
                  </p>
                  <p class="western" align="justify" style="margin-left: 0.25in"><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">b.
                  </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Tinggi
                  ruang minimum adalah 2,4 m. Tinggi ruang adalah jarak terpendek
                  dalam ruang diukur dari permukaan atas lantai sampai permukaan
                  bawah langit-langit atau sampai permukaan bawah kaso-kaso jika
                  tidak ada langit-langit.</font></font></p>
                </td>
              </tr>
            </table>
          </center>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>Ketentuan
          organisasi ruang. </b></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Organisasi
          ruang harus mengandung fungsi-fungsi untuk keluarga, yaitu ruang
          keluarga, dapur, kamar mandi dan kakus, serta ruang tidur.</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif">4.</font><span style="font-variant: normal"><font face="Cambria, serif">
          </font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Komponen
          bahan bangunan sesuai konteks lokal. </font></font>
          </p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Teknologi
          dan bahan bangunan rumah layak huni yang sesuai dengan kearifan lokal
          disesuaikan dengan adat dan budaya daerah setempat. </font></font>
          </p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <br/>

          </p>
          <center>
            <table width="380" cellpadding="2" cellspacing="0">
              <col width="372">
              <tr>
                <td width="372" valign="top" bgcolor="#95b3d7" style="border: 1.50pt solid #365f91; padding-top: 0.02in; padding-bottom: 0.02in; padding-left: 0.05in; padding-right: 0.08in">
                  <p class="western" align="justify"><font face="Cambria, serif"><font size="2" style="font-size: 10pt"><b>Kotak
                  2.8.</b></font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="2" style="font-size: 10pt"><b>
                  </b></font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>Persyaratan&nbsp;
                  Atap</b></font></font></p>
                </td>
              </tr>
              <tr>
                <td width="372" valign="top" bgcolor="#ffffff" style="border: 1.50pt solid #365f91; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.05in; padding-right: 0.08in">
                  <p class="western" align="justify" style="margin-left: 0.25in"><span style="font-variant: normal"><font face="Cambria, serif"><font size="2" style="font-size: 10pt">a.
                  </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Miring
                  atap harus disesuaikan dengan bahan penutup yang akan digunakan,
                  sehingga tidak akan mengakibatkan bocor.</font></font></p>
                  <p class="western" align="justify" style="margin-left: 0.25in"><span style="font-variant: normal"><font face="Cambria, serif"><font size="2" style="font-size: 10pt">b.
                  </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Bidang
                  atap harus merupakan bidang yang rata kecuali dikehendaki
                  bentuk-bentuk yang khusus, seperti parabola, kupola, dll.</font></font></p>
                  <p class="western" align="justify" style="margin-left: 0.25in"><span style="font-variant: normal"><font face="Cambria, serif"><font size="2" style="font-size: 10pt">c.
                  </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Atap
                  tidak bocor, sehingga tidak menimbulkan kelembaban yang tinggi
                  yang menyebabkan suburnya pertumbuhan mikroorganisme.</font></font></p>
                  <p class="western" align="justify" style="margin-left: 0.25in"><span style="font-variant: normal"><font face="Cambria, serif"><font size="2" style="font-size: 10pt">d.
                  </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Persentase
                  atap bocor&nbsp; sedang yaitu &lt;20 % dari luas atap dan
                  persentase atap bocor berat yaitu &gt; 20 % dari luas atap.</font></font></p>
                </td>
              </tr>
              <tr>
                <td width="372" valign="top" bgcolor="#95b3d7" style="border: 1.50pt solid #365f91; padding-top: 0.02in; padding-bottom: 0.02in; padding-left: 0.05in; padding-right: 0.08in">
                  <p class="western" align="justify"><font face="Cambria, serif"><font size="2" style="font-size: 10pt"><b>Kotak
                  2.9.</b></font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="2" style="font-size: 10pt"><b>
                  </b></font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>Persyaratan&nbsp;
                  Dinding</b></font></font></p>
                </td>
              </tr>
              <tr>
                <td width="372" valign="top" bgcolor="#ffffff" style="border: 1.50pt solid #365f91; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.05in; padding-right: 0.08in">
                  <p class="western" align="justify" style="margin-left: 0.25in"><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">a.
                  </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Dinding
                  harus dibuat sedemikian rupa sehingga dapat memikul berat
                  sendiri, berat angin, dan dalam hal merupakan dinding pemikul
                  pula harus dapat memikul beban-beban di atasnya.</font></font></p>
                  <p class="western" align="justify" style="margin-left: 0.25in"><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">b.
                  </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Dinding-dinding
                  di kamar mandi dan kakus, setinggi sekurang-kurangnya 1,5 m di
                  atas permukaan lantai harus rapat air.</font></font></p>
                  <p class="western" align="justify" style="margin-left: 0.25in"><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">c.
                  </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">Dinding
                  disebut rusak sedang jika kondisi dindign retak tembus</span></font></font></p>
                  <p class="western" align="justify" style="margin-left: 0.25in"><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">d.
                  </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">Dinding
                  disebut rusak berat jika kondisi dinding roboh, roboh sebagian,
                  mengalami perubahan bentuk (miring)</span></font></font></p>
                </td>
              </tr>
              <tr>
                <td width="372" valign="top" bgcolor="#95b3d7" style="border: 1.50pt solid #365f91; padding-top: 0.02in; padding-bottom: 0.02in; padding-left: 0.05in; padding-right: 0.08in">
                  <p class="western" align="justify"><font face="Cambria, serif"><font size="2" style="font-size: 10pt"><b>Kotak
                  2.10.</b></font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="2" style="font-size: 10pt"><b>
                  </b></font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>Persyaratan&nbsp;
                  Lantai</b></font></font></p>
                </td>
              </tr>
              <tr>
                <td width="372" valign="top" bgcolor="#ffffff" style="border: 1.50pt solid #365f91; padding-top: 0in; padding-bottom: 0.02in; padding-left: 0.05in; padding-right: 0.08in">
                  <p class="western" align="justify" style="margin-left: 0.22in"><span style="font-variant: normal"><font face="Cambria, serif"><font size="2" style="font-size: 10pt">a.
                  </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Lantai-lantai
                  harus kuat untuk menahan beban-beban yang akan timbul dan pula
                  harus diperhatikan lendutannya.</font></font></p>
                  <p class="western" align="justify" style="margin-left: 0.22in"><span style="font-variant: normal"><font face="Cambria, serif"><font size="2" style="font-size: 10pt">b.
                  </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Lantai
                  tidak lembab dan terbuat dari material yang mudah dibersihkan</font></font></p>
                </td>
              </tr>
            </table>
          </center>
          <p class="western" align="justify" style="margin-left: 0.69in; margin-right: 0.39in; margin-bottom: 0in">
          <br/>

          </p>
          <p class="western" align="justify" style="margin-left: 0.69in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><b>2.2.</b></font><span style="font-variant: normal"><font face="Cambria, serif"><b>
          </b></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>SEGMENTASI</b></font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>MASYARAKAT
          BERPENGHASILAN RENDAH</b></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">Mengacu
          pada studi dalam Roadmap Reformasi Kebijakan Perumahan yang </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">telah</font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">dilakukan
          oleh Bappenas dengan dukungan teknis dari World Bank, Kriteria
          Penghasilan Masyarakat di Indonesia dikelompokkan atas 10 </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US"><i>desile</i></span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">
          sebagai berikut;</span></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="2" style="font-size: 10pt"><b>Gambar/Diagram
          2. 1.</b></font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="2" style="font-size: 10pt"><b>
          </b></font></font></span>
          </p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>Kriteria
          Readiness</b></font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>Masyarakat
          Berpenghasilan Rendah di Indonesia</b></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">Informasi
          susenas BPS menunjukkan bahwa 45% rumah yang ada saat ini, atau
          sebanyak 28,9 juta unit, masih di bawah standar karena salah satu
          atau beberapa faktor berikut: ada satu bahan bangunan yang tidak
          layak, tidak ada akses air dan sanitasi, atau terlalu padat. Jumlah
          ini berkisar dari 7,5 juta unit yang dinilai terlalu padat, 22,3 juta
          unit tidak punya akses terhadap sanitasi yang lebih baik, 8,8 juta
          unit tanpa akses air yang aman, atau 6 juta unit tanpa atap, dinding
          atau lantai yang layak dan memadai. Tabel berikut menunjukkan
          distribusi perumahan yang tidak layak diantara beberapa karakteristik
          berdasarkan desil konsumsi rumah tangga</span></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">Terdapat
          skenario penanganan yang berbeda-beda untuk setiap kelompok
          masyarakat. Agenda pertama dalam Roadmap reformasi kebijakan
          perumahan adalah meningkatkan investasi disektor perumahan dan
          menargetkan kebijakan perumahan kepada 40% populasi paling bawah
          melalui tiga inisiatif besar berskala nasional, pada: </span></font></font>
          </p>
          <p class="western" align="justify" style="margin-left: 0.98in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">(i)</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">Penanganan
          perumahan kumuh secara komprehensif; </span></font></font>
          </p>
          <p class="western" align="justify" style="margin-left: 0.98in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">(ii)</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">Subsidi
          perumahan formal; </span></font></font>
          </p>
          <p class="western" align="justify" style="margin-left: 0.98in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">(iii)</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">Perbaikan
          rumah secara bertahap</span></font></font><font color="#ff0000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">.
          </span></font></font></font>
          </p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">Untuk
          mendapat gambaran umum mengenai Data Rumah Tidak Layak Huni di
          Indonesia serta gambaran kemampuan masyarakatnya, </span></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">Objek
          pendataan yang dijadikan target sasaran dalam kegiatan pendataan ini
          meliputi semua masyarakat dari desile 1 hingga 4, yaitu masyarakat
          dengan pengeluaran maksimum 2,6 juta rupiah setiap bulannya. Hal ini
          ditujukan agar data yang dihasilkan</span></font></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">
          tidak hanya dapat berguna dalam program/kebijakan perumahan swadaya,
          namun juga dapat menjadi acuan dalam kebijakan perumahan secara
          menyeluruh baik di pusat, provinsi, dan kabupaten/kota.</span></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="2" style="font-size: 10pt"><b>Gambar/Diagram
          2. 2.</b></font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="2" style="font-size: 10pt"><b>
          </b></font></font></span>
          </p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>Skenario
          Penanganan Menurut Segmentasi masyarakat</b></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <br/>

          </p>
          <p class="western" align="center" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <span style="display: inline-block; border: 1.00pt solid #000001; padding: 0.01in"><font face="Cambria, serif"><font size="4" style="font-size: 16pt"><b><font color="#0070c0">BAB
          3</span></b></font></font></font></p>
          <p class="western" align="center" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <span style="display: inline-block; border: 1.00pt solid #000001; padding: 0.01in"><font face="Cambria, serif"><font size="4" style="font-size: 16pt"><b><font color="#0070c0">URAIAN
          PROGRAM PENDATAAN RTLH</span></b></font></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>Pendekatan
          - Komponen Persyaratan - Prinsip Pelaksanaan</b></font></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><b>3.1.</b></font><span style="font-variant: normal"><font face="Cambria, serif"><b>
          </b></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>PENDEKATAN
          </b></font></font>
          </p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><b>3.1.1.</b></font><span style="font-variant: normal"><font face="Cambria, serif"><b>
          </b></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>REORIENTASI
          PARADIGMA PEMBANGUNAN</b></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Dalam
          program pendataan ini, dilakukan dengan adanya reorientasi paradigma
          pembangunan dimana terjadi perubahan pola dan peran stakeholder dalam
          pembangunan yakni:</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">1.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Pola
          pembangunan konvensional menjadi Pembangunan berbasis Komunitas
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>(community
          base development).</i></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">2.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Peran
          perencana/konsultan/ahli yang semula sebagai penentu menjadi
          fasilitator </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">embangunan</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">3.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Peran
          pemerintah yang semula sebagai Penyedia </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>(provider)</i></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          bergeser menjadi pemampu </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>(Enabler)
          </i></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">yang
          diwujudkan dengan kegiatan sosialisasi dan </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">embekalan.</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="2" style="font-size: 10pt"><b>Gambar/Diagram
          3. 1.</b></font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="2" style="font-size: 10pt"><b>
          </b></font></font></span>
          </p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>Reorientasi
          paradigma Pembangunan</b></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><b>3.1.2.</b></font><span style="font-variant: normal"><font face="Cambria, serif"><b>
          </b></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>PROGRAM
          INTEGRATIF</b></font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>FASILITASI
          </b></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i><b>TOP
          DOWN</b></i></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>
          – PARTISIPASI </b></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i><b>BOTTOM
          UP</b></i></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Pendekatan
          </font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Program</font></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          Integratif merupakan konsekuensi dari Reorientasi paradgiman
          pembangunan sebagaimana yang diuraikan diatas, sehingga menjadi bahwa
          program pendataan ini merupakan:</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">1.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Titik
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">t</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">emu
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">a</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ntara
          pendekatan </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>Top
          Down</i></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          (</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">emerintah)
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">d</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">an
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>Bottom
          Up</i></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          (</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">k</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">omunitas-</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">m</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">asyarakat)
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">a</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">gar
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">elaksanaan
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">rogram
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">endataan
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">l</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ebih
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">t</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">erarah
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">d</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">an
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">h</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">asil
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">endataan
          Triple A </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">l</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ebih
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">t</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">epat
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">s</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">asaran
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">k</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">arena
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">s</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">esuai
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">d</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">engan
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">k</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ondisi-</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">k</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ebutuhan
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">y</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ang
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">d</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">irumuskan
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">o</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">leh
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">m</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">asyarakat.</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">2.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Pelaksanaan
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">endataan
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">d</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ilakukan
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">d</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">engan
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">endekatan
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">d</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ari,
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">o</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">leh</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">,
          d</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">an
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">u</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ntuk
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">m</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">asyarakat,
          </font></font>
          </p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">3.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Reorientasi
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">eran
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">m</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">asyarakat
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">y</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ang
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">s</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">emula
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">asif
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">m</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">enjadi
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">a</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ktif
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">d</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">engan
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">engembangan
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">i</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">nstitusi
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">l</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">okal
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">d</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">i</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">t</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ingkat
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">m</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">asyarakat
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">m</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">elalui
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">k</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">egiatan
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">embekalan/</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">emberdayaan.</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="2" style="font-size: 10pt"><b>Gambar/Diagram
          3. 2.</b></font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="2" style="font-size: 10pt"><b>
          </b></font></font></span>
          </p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>Pendekatan
          </b></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i><b>Top
          Down</b></i></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>
          dan </b></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i><b>Bottom
          Up </b></i></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>dalam
          Program Pendataan RTLH</b></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>Pendekatan</b></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>
          </b></font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i><b>Top-Down
          </b></i></font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">merupakan
          arah pendekatan pelaksanaan program yang bergerak dari atas bergerak
          ke bawah, dalam hal ini adalah mulai dari </font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">tingkat
          pusat</span></font></font></font><font color="#000000"> –
          </font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">provinsi-kabupaten/kota</span></font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">.</font></font></font><font color="#000000">
          </font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Pendekatan
          ini diwujudkan dalam bentuk </font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">fasilitasi
          pelaksanaan program pendataan RTLH</span></font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">,
          yang dilakukan oleh pelaku fasilitasi di</font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">t</span></font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ingkat
          </font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">usat
          </font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">sampai
          dengan pemerintah daerah</span></font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">,
          (SKPD </font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">t</span></font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ingkat
          </font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">rovinsi
          dan </font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">kabupaten/kota</span></font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">)
          melalui kegiatan:</font></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.65in; margin-right: 0.39in; margin-bottom: 0in">
          <font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">1.</font></font></font><span style="font-variant: normal"><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">Bimbingan
          Teknis dan Pembekalan Tim Inti Fasilitasi Pendataan RTLH </span></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">yang
          dilakukan secara berjenjang dari, oleh dan untuk setiap pelaku
          program yang terlibat di setiap hirarki wilayah </font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">ad</span></font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ministratif
          (</font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">provinsi
          </span></font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">-
          </font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">kabupaten/kota
          </span></font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">-</font></font></font><font color="#000000">
          </font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">k</span></font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ecamatan).</font></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.65in; margin-right: 0.39in; margin-bottom: 0in">
          <font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">2.</font></font></font><span style="font-variant: normal"><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">Penyediaan
          panduan program berupa pedoman umum dan pedoman teknis dilengkapi
          dengan instrumen – instrumen pendukungnya. </span></font></font>
          </p>
          <p class="western" align="justify" style="margin-left: 0.65in; margin-right: 0.39in; margin-bottom: 0in">
          <font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">3.</font></font></font><span style="font-variant: normal"><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">Pengembangan
          sistem aplikasi pengelolaan database RTLH berbasis web yang
          memudahkan setiap</span></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">:</font></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.9in; margin-right: 0.39in; margin-bottom: 0in">
          <font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">a.</font></font></font><span style="font-variant: normal"><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></font></span><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Pelaksana
          pendataan dalam melakukan </font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">Input
          – Proses – Output data RTLH yang dapat diupdate kapan saja.</span></font></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.9in; margin-right: 0.39in; margin-bottom: 0in">
          <font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">b.</font></font></font><span style="font-variant: normal"><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></font></span><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Stakeholder
          untuk dapat mengakses data terkait RTLH untuk kebutuhan perencanaan
          program PKP</font></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>Pendekatan
          Bottom-Up </b></font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">merupakan
          pendekatan pelaksanaan pendataan yang dilakukan di wilayah
          administrasi paling bawah, yakni </font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">tingkat
          kelurahan/desa</span></font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">.
          Melalui pendekatan ini, </font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">peran
          dan partisipasi aktif masyarakat </span></font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">sebagai
          aktor utama sebagai pelaksana pendataan-pemetaan untuk menghasilkan
          seluruh muatan data terkait RTLH. Peran utama dari masyarakat dalam
          kegiatan pendataan ini dimulai dari:</font></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.65in; margin-right: 0.39in; margin-bottom: 0in">
          <font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">1.</font></font></font><span style="font-variant: normal"><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></font></span><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">Persiapan
          kegiatan pendataan</span></font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">;</font></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.65in; margin-right: 0.39in; margin-bottom: 0in">
          <font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">2.</font></font></font><span style="font-variant: normal"><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></font></span><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">Pemetaan
          swadaya</span></font></font></font><font color="#000000"> </font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">(untuk
          mengidentifikasi karakteristik RTLH, permasalahan dan potensi setiap
          lokasi dan unit RTLH); dan</font></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.65in; margin-right: 0.39in; margin-bottom: 0in">
          <font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">3.</font></font></font><span style="font-variant: normal"><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></font></span><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">Perencanaan
          Aksi</span></font></font></font><font color="#000000"> </font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">(community
          action plan)</span></font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">.</span></font></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Dengan
          demikian, maka gambaran karakteristik yang diperoleh merupakan hasil
          yang valid, akurat, dan sesuai dengan kondisi dan kebutuhan
          masyarakat. </font></font></font>
          </p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><b>3.1.3.</b></font><span style="font-variant: normal"><font face="Cambria, serif"><b>
          </b></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>PEMETAAN
          SWADAYA (</b></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i><b>COMMUNITY/SELF
          MAPPING)</b></i></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">Pemetaan
          swadaya </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US"><i>(Community
          Self Mapping)</i></span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">
          </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">adalah
          rangkaian kegiatan yang dilakukan sendiri oleh masyarakat </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">l</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">okal
          untuk mengumpulkan – menilai kondisi fisik RTLH dan pemiliknya di
          lingkungan mereka tinggal. Peran utama masyarakat dalam kegiatan
          pendataan ini didahului dengan proses pembekalan teknis dan dalam
          pelaksanaan pendataan didampingi oleh fasilitator kelurahan/TPM.
          Hasil dari pemetaan swadaya ini merupakan sumber data untuk
          merumuskan </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">program-program
          kegiatan selanjutnya</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">.
          Kegiatan pemetaan swadaya ini bertujuan untuk:</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">1.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Masyarakat
          menempuh proses pembelajaran yang benar untuk mengenali rumah dan
          lingkungannya sendiri;</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">2.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Mengumpulkan
          data tentang kondisi RTLH</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">;
          dan</span></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">3.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Mengumpulkan
          data dan informasi tentang berbagai masalah, kendala, potensi dan
          harapan-harapan yang ada di lingkungan permukimannnya.</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.79in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><b>3.1.4.</b></font><span style="font-variant: normal"><font face="Cambria, serif"><b>
          </b></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>PERUMUSAN
          RENCANA AKSI</b></font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i><b>(COMMUNITY
          ACTION PLAN/CAP)</b></i></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US"><i>Community
          Action Plan </i></span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">(CAP)
          merupakan salah satu metode untuk membangun kapasitas masyarakat
          dalam melakukan kegiatan-kegiatan yang tepat sesuai masalah,
          kabutuhan, dan potensi sumberdaya masyararakat itu sendiri. Dalam
          konteks pendataan RTLH, maka CAP merupakan rangkaian kegiatan rembuk
          masyarakat untuk merumuskan Program dan kegiatan yang disusun menurut
          skala prioritas kebutuhan masyarakat dalam jangka waktu tertentu.&nbsp;
          Pelaksanaan kegiatan ini bertujuan untuk: </font></font>
          </p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="1" style="font-size: 8pt">1.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="1" style="font-size: 8pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Menyusun
          rencana program pembangunan/peningkatan kualitas RTLH dalam rentang
          waktu 5 tahun secara komprehensif</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">;</span></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="1" style="font-size: 8pt">2.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="1" style="font-size: 8pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Menetapkan
          program prioritas yang diperlukan masyarakat dalam upaya peningkatan
          kualitas fisik RTLH dan PSU kawasan;</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="1" style="font-size: 8pt">3.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="1" style="font-size: 8pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Mencari
          peluang dan upaya menghubungkan atau channeling pendanaan program
          kepada stakeholder lain; dan</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="1" style="font-size: 8pt">4.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="1" style="font-size: 8pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Penyiapan
          program prioritas pada tahun pertama</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">.</span></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><b>3.2.</b></font><span style="font-variant: normal"><font face="Cambria, serif"><b>
          </b></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>KOMPONEN
          PROGRAM</b></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Bentuk
          komponen program dalam pendataan RTLH ini dapat dikelompokkan menjadi
          2, yakni: </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">rogram
          untuk Masyarakat Kelurahan/desa dan program untuk Pemerintah Daerah
          (Provinsi, Kota/kabupaten).</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><b>3.2.1.</b></font><span style="font-variant: normal"><font face="Cambria, serif"><b>
          </b></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>KOMPONEN
          UNTUK PEMERINTAH DAERAH</b></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Komponen
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">pro</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">gram
          untuk </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">emerintah
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">d</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">aerah
          dalam program pendataan RTLH ini dalam bentuk:</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif">1.</font><span style="font-variant: normal"><font face="Cambria, serif">
          </font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Bantuan
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">t</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">eknis
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">b</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">agi
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">emerintah
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">d</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">aerah
          berupa:</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.89in; margin-right: 0.39in; margin-bottom: 0in">
          <span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">a.
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Penyediaan
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">anduan
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">dan
          i</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">nstrumen
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">d</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">asar
          dalam pelaksanaan kegiatan pendataan, yang mencakup: Pedoman Umum,
          Pedoman Teknis, Sistem Aplikasi Database RTLH </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">dan
          </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Manualnya.</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.89in; margin-right: 0.39in; margin-bottom: 0in">
          <span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">b.
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Penguatan
          kapasitas pemerintah daerah dan stakeholder melalui
          sosialisasi-pembekalan/bimbingan teknis dan pembakalan tim inti
          pendataan RTLH yang diselenggarakan oleh Tim PUPERA perihal:</font></font></p>
          <p class="western" align="justify" style="margin-left: 1.14in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">1)</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Penyediaan
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">basis
          data</span></font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt">RTLH
          sebagai salah satu urusan wajib pemerintah daerah bidang perumahan</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">;</span></font></font></p>
          <p class="western" align="justify" style="margin-left: 1.14in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">2)</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Urgensi
          Kegiatan Pendataan sebagai </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>Entry
          Point</i></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          Penyelenggaraan PKP-RTLH yang lebih tepat sasaran</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">;
          dan</span></font></font></p>
          <p class="western" align="justify" style="margin-left: 1.14in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">3)</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Prinsip
          Program Pendataan RTLH dengan Pendekatan Teknis Triple</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">.</span></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif">2.</font><span style="font-variant: normal"><font face="Cambria, serif">
          </font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>Supply</i></font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">d</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ata
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">t</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">erkait
          RTLH </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">y</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ang
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>valid-update</i></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>.
          </b></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Dengan
          mengadopsi Pendekatan </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>bottom
          up,</i></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          dimana kegiatan pendataan dilakukan di </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">t</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ingkat
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">kelurahan/desa</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">,
          sehingga hasil pendataannya sesuai dengan kondisi </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">dan
          </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">kebutuhan
          masyarakat. </font></font>
          </p>
          <p class="western" align="justify" style="margin-left: 0.8in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><b>3.2.2.</b></font><span style="font-variant: normal"><font face="Cambria, serif"><b>
          </b></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>KOMPONEN
          PROGRAM TINGKAT KELURAHAN/</b></font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>DESA</b></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Komponen
          program dalam pelaksanaan kegiatan pendataan RTLH yang diperuntukan
          di </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">tingkat
          kelurahan/desa</span></font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt">adalah:</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>1.</b></font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>
          </b></font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>Bantuan
          Teknis </b></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US"><b>dan
          </b></span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>Pendampingan
          Pelaksanaan Kegiatan Pendataan</b></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">.
          </font></font>
          </p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Bantuan
          teknik diberikan melalui penugasan Fasilitator Kelurahan dan TPM
          beserta dukungan dana operasional untuk mendampingi masyarakat dalam
          rangka Pendataan Rumah Tidak Layak Huni. Kegiatan pendampingan
          masyarakat terdiri dari serangkaian kegiatan, mulai dari persiapan
          sosial, survei swadaya, merumuskan kebutuhan nyata dari hasil
          pendataan yang dilakukan secara swadaya, perumusan rencana tindak
          dalam bentuk </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">rogram
          dan kegiatan. </font></font>
          </p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Jenis
          kegiatan pendampingan mencakup:</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.89in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">1.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">Rembuk
          warga </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ditingkat
          komunitas maupun kelurahan/desa, baik bersifat sosialisasi maupun
          koordinasi;</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.89in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">2.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">Pelatihan
          dan pembekalan </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">perihal
          kriteria Rumah Tidak Layak Huni (RTLH) maupun Teknik Pemetaan </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>(Self
          Mapping)</i></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">dan
          </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>Community
          Action Plan </i></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">(</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">erumusan
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">r</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">encana
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">t</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">indak);</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.89in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">3.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">Pelaksanaan
          kegiatan pemetaan/survei swadaya, </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">dalam
          hal ini teknik pemetaan spasial unit RTLH dan pengisian form-form
          pendataan</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">;
          dan</span></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.89in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">4.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">Pelaksanaan
          </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US"><i>Community
          Action Plan</i></span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">
          </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">dalam
          hal merumuskan potensi, permasalahan dan kebutuhan </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">berdasarkan
          </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">prioritas.</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.79in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>3.3.</b></font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>
          </b></font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>PERSYARATAN
          PROGRAM</b></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Prasyarat
          dalam pelaksanaan program pendataan </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">dan
          </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">pengembangan
          sistem </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>database</i></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          RTLH adalah sebagai berikut:</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">1.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">Kesiapan
          </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US"><i>(readiness)
          </i></span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">tim
          fasilitasi program tingkat pusat-provinsi </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">yang
          akan memfasilitasi program pendataan RTLH di </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">tingkat
          kabupaten/kota</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">.
          Bentuk kesiapan ini merupakan salah</font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt">satu
          wujud komitmen pemerintah daerah dalam me</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">m</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">fasilitasi
          program PKP sebagai salah</font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt">satu
          wujud pelaksanaan kewajiban </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">daerah
          bidang perumahan</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">;</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">2.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">Kesiapan
          (Readiness) pemerintah daerah kabupaten/kota</span></font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">dalam</font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">memfasilitasi
          program pendataan dan mengembangkan sistem </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>database</i></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          RTLH ini. Adapun indikator </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">r</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">eadiness
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">kabupaten/kota</span></font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">ini
          dapat dilihat dari:</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">a.
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Keberadaan
          institusi </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>leading
          sector</i></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          bidang PKP-RTLH yang memiliki unit/divisi pengembangan database
          dilengkapi dengan SDM yang ditempatkan dengan prinsip </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>right
          person </i></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">dan
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>right
          place;</i></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">b.
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Kepemilikan
          dokumen penyelenggaraan PKP (RP3KP/RP4D) dan pengaturan Tata Ruang
          Wilayah (RTRW </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">kabupaten/kota</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">)
          yang masih berlaku;</font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">dan</span></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">c.
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Alokasi
          APBD untuk pelaksanaan pendataan dan pengembangan sistem </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>database</i></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          RTLH secara berkelanjutan.</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">3.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">Pembentukan
          Pokja pendataan RTLH-PKP. </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Kegiatan
          ini dilakukan dalam berbagai tingkatan Tingkat Kota-Kabupaten sampai
          kepada </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">tingkat
          kelurahan/desa </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">dimana
          pokja ini akan berperan sebagai penyelenggara-pengelola-pelaksana
          program, sesuai dengan Tupoksi </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">Pokja
          </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">menurut
          wilayah kerjanya;</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">4.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">Penerbitan
          payung hukum institusi/Pokja pendataan RTLH. </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Payung
          hukum ini dibutuhkan sebagai legalitas keberadaan dan </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">Tupoksi
          </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">institusi/Pokja
          yang dibangun di berbagai tingkatan, mulai dari </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">tingkat
          kabupaten/kota</span></font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt">s</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">ampai
          dengan tingkat kelurahan/desa</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">.
          Dengan adanya payung hukum ini, keberadaan institusi </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">dan
          </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">setiap
          personil SDM yang ada di</font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt">dalamnya
          menjadi resmi-sah dimata hukum, dan dapat terkendali kualitas
          kerjanya karena memiliki unsur </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US"><i>r</i></span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>eward
          dan </i></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US"><i>p</i></span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>unishment</i></font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">dalam
          pelaksanaannya;</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">5.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Pelaksanaan
          sosialisasi-internalisasi program pendataan RTLH. Kegiatan ini
          dilakukan dalam berbagai tingkatan mulai </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">tingkat
          provinsi</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">,
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">tingkat
          kabupaten/kota</span></font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt">sampai
          kepada ke </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">t</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ingkat
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">kelurahan/desa</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">.
          Pelaksanaan </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">s</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">osialisasi
          ini dilaksanakan secara berjenjang, sesuai hirarki wilayah kerjanya,
          melalui mekanisme lokakarya/rapat koordinasi.</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">6.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Pembekalan
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">t</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">eknis-nonteknis
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">s</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">emua
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ersonil
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">i</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">nstitusi/</font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">k</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">elembagaan
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">t</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">erkait
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">rogram
          RTLH. Pembekalan ini dilakukan di</font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt">semua
          tingkatan, mulai dari </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">t</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ingkat
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">rovinsi
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">dan
          t</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ingkat
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">d</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">esa/</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">k</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">elurahan,
          untuk memberikan pemahaman – pengetahuan/wawasan – ketrampilan
          dalam memfasilitasi – melaksanakan program pendataan RTLH sesuai
          dengan T</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">upoksi</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">nya
          masing-masing. Pembekalan ini dilakukan secara berjenjang sesuai
          hirarki wilayah kerjanya, dilaksanakan dengan metoda </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">bimbingan
          teknis dan pembekalan tim inti pendataan RTLH </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">yang
          di</font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt">dalamnya
          terdapat simulasi-simulasi praktis;</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">7.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>Collecting</i></font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">dan
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US"><i>r</i></span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>eview
          </i></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US"><i>d</i></span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>ata</i></font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">s</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ekunder
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">erihal
          RTLH. Kegiatan ini dilakukan dalam berbagai tingkatan </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">t</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ingkat
          Provinsi </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">sampai
          dengan t</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ingkat
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">kelurahan/desa</span></font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">sebagai
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>entry
          point</i></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          penyiapan perangkat kerja-data awal dalam pelaksanaan pendataan.
          Kegiatan ini dilakukan dengan </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">M</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">etoda
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US"><i>Desk
          Study </i></span></font></font>– <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">Rapat
          Koordinasi </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">dengan
          menggunakan beberapa instrumen acuan;</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">8.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Legalisasi
          hasil pendataan </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">RTLH
          </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">merupakan
          kegiatan penerbitan SK </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">l</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">urah/</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">k</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">epala
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">d</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">esa
          untuk memberikan kekuatan hukum dokumen </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">RTLH</span></font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">sebagai
          dokumen autentik hasil dari rangkaian kegiatan pendataan yang
          dilakukan masyarakat ditingkat </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">kelurahan/desa.
          L</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">egalisasi
          hasil pendataan dimaksudkan agar seluruh informasi yang
          dipublikasikan adalah bersifat final dan resmi;</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">9.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Komitmen
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">eran
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">a</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ktif
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">m</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">asyarakat
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">s</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ebagai
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">elaksana
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">endataan
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">di
          t</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ingkat
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">kelurahan/desa</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">.
          Prinsip ini diterapkan untuk menjamin perolehan data RTLH yang
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>valid-update</i></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          dan benar-benar dirumuskan masyarakat. Melalui mekanisme partisipasi
          aktif masyarakat ini, yang di</font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt">dalamnya
          terdapat unsur pemberdayaan, maka diharapkan di masa mendatang,
          masyarakat dapat melakukan seluruh tahapan pendataan secara mandiri,
          tanpa tergantung oleh </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">f</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">asilitator</font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">(k</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ecuali
          yang terkait dengan proses pembuatan gambar-gambar teknis yang memang
          diperlukan keahlian khusus). Adapun kegiatan partisipasi aktif
          masyarakat ini dilakukan dengan 2 tahapan besar, yakni: </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">emetaan
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">s</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">wadaya,
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US"><i>c</i></span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>ommunity
          </i></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US"><i>a</i></span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>ction
          </i></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US"><i>p</i></span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>lan</i></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">,
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">u</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ji
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ublik;</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">10.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">P</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">elaksanaan
          monitoring dan evaluasi berjenjang dalam tahapan program pendataan
          RTLH</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>.
          </b></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Kegiatan
          Monitoring dilakukan secara berkala pada:</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <br/>

          </p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">a.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">setiap
          tahapan proses dalam berbagai tingkatan mengawasai pelaksanaan
          kegiatan sesuai prinsip dan prosedur program dan menjamin output
          pendataan dapat optimal; dan</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">b.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">beberapa
          tahapan proses strategis yang berpotensi terjadi penyimpangan.</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Kegiatan
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">e</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">valuasi
          dilakukan untuk menilai dan mengukur apakah pendekatan dan hasil
          kegiatan pendataan di tingkat kelurahan/desa dan pengembangan sistem
          database RTLH di </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">t</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ingkat
          kabupaten/kota mengarah pada pencapaian tujuan program yang
          diharapkan</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">.</span></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.69in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>3.4</b></font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>
          </b></font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>PRINSIP
          PELAKSANAAN PROGRAM</b></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Prinsip-prinsip
          yang dikembangkan dalam program pendataan RTLH ini adalah:</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">1.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Membangun
          kapasitas pelaku; Prinsip ini sudah harus ada dibenak semua pelaku
          bahwa salah</font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt">satu
          yang akan dibangun di</font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt">dalam
          program pendataan ini adalah kapasitas setiap pelaku yang terlibat
          (mulai dari </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">t</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ingkat
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">rovinsi
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">sampai
          dengan t</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ingkat
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">m</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">asyarakat
          yang ada di </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">t</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ingkat
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">kelurahan/desa</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">);</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">2.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Pengutamaan
          konteks lokal</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>;</b></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          Upaya pendataan RTLH ini disesuaikan dengan konteks lokal. Artinya,
          terdapat beberapa indikator-indikator RTLH yang disesuaikan dengan
          budaya dan kearifan lokal;</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">3.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font color="#231f20"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Pembelajaran.
          Tahap pembelajaran ini merupakan tahap pengenalan-pemahaman yang
          diperuntukan</font></font></font><font color="#231f20"> </font><font color="#231f20"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">bagi
          setiap pelaku mulai dari </font></font></font><font color="#231f20"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">t</span></font></font></font><font color="#231f20"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ingkat
          </font></font></font><font color="#231f20"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font></font><font color="#231f20"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">rovinsi,
          </font></font></font><font color="#231f20"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">tingkat
          kabupaten/kota</span></font></font></font><font color="#231f20"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">,
          </font></font></font><font color="#231f20"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">tingkat
          kecamatan-kelurahan/desa</span></font></font></font><font color="#231f20">
          </font><font color="#231f20"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">untuk
          dapat memahami, menerima dan menyepakati setiap tahapan kegiatan yang
          dikembangkan di</font></font></font><font color="#231f20"> </font><font color="#231f20"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">dalam
          program pendataan;</font></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">4.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font color="#231f20"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Keberlanjutan.
          Tahap keberlanjutan di</font></font></font><font color="#231f20">
          </font><font color="#231f20"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">mulai
          dengan proses penyiapan pelaku program (</font></font></font><font color="#231f20"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">t</span></font></font></font><font color="#231f20"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ingkat
          </font></font></font><font color="#231f20"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font></font><font color="#231f20"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">rovinsi
          sampai </font></font></font><font color="#231f20"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">tingkat
          kelurahan/desa</span></font></font></font><font color="#231f20"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">)
          agar mampu melanjutkan penyelenggaraan program pendataan RTLH secara
          mandiri. Proses penyiapan ini membutuhkan waktu setidaknya minimal
          satu tahun. Pada tahap keberlanjutan, setiap pelaku yang terlibat
          mampu menjalankan secara mandiri untuk setiap tahapan kegiatannya
          tanpa menutup kemungkinan terjadinya inovasi-inovasi yang dapat
          mengoptimalkan penyelenggaraan program;</font></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">5.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Tata
          kelola kepemerintahan yang baik (</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>Good
          Governance</i></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">)
          Prinsip ini menjadikan Program Pendataan RTLH ini sebagai pemicu dan
          pemacu untuk membangun kapasitas pemerintah daerah dan masyarakat,
          agar mampu melaksanakan dan mengelola program ini secara mandiri,
          dengan menerapkan tata kelola yang baik </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>(good
          governance)</i></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">;</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">6.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Keterbukaan</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>,</b></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          mengajarkan kepada semua pelaku untuk saling terbuka juga terhadap
          pembaruan atau inovasi-inovasi teknis pendataan RTLH;</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">7.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Transparansi,
          Mengajak semua pelaku untuk dapat menunjukan peran, kontribusi dan
          tanggung jawabnya secara jelas dan gamblang (transparan) dalam
          pelaksanaan program;</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">8.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Akuntabilitas</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>;</b></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          Mengajak semua pelaku yang terlibat untuk mampu
          mempertanggung-jawabkan tindakannya dalam menjalankan T</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">upoksi</span></font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">(Tugas
          Pokok </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">dan
          </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Fungsi)
          dalam Program dan bersedia untuk memperoleh akibatnya dengan adanya
          penerapan sistem </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>Reward</i></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">dan
          </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>Punishment</i></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">.</span></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">9.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Demokrasi;
          mengajak semua pelaku untuk mendengar dan mempertimbangkan
          kepentingan bersama dalam pengambilan keputusan terkait program, dan</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">10.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Musyawarah</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>;</b></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          Musyawarah harus menjadi mekanisme utama dalam pengambilan keputusan
          program yang terkait dengan kepentingan publik (umum). </font></font>
          </p>
          <p class="western" align="center" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <span style="display: inline-block; border: 1.00pt solid #000001; padding: 0.01in"><font face="Cambria, serif"><font size="4" style="font-size: 16pt"><b><font color="#0070c0">BAB
          4</span></b></font></font></font></p>
          <p class="western" align="center" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <span style="display: inline-block; border: 1.00pt solid #000001; padding: 0.01in"><font face="Cambria, serif"><font size="4" style="font-size: 16pt"><b><font color="#0070c0">TAHAPAN
          PROGRAM PENDATAAN RTLH</span></b></font></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Tahapan
          Program Pendataan RTLH ini dapat digambarkan secara ringkas,
          sebagaimana yang terlihat di</font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt">dalam
          diagram berikut.</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="2" style="font-size: 10pt"><span lang="en-US"><b>Gambar/Diagram
          4. 1.</b></span></font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="2" style="font-size: 10pt"><span lang="en-US"><b>
          </b></span></font></font></span>
          </p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>Prinsip
          Kegiatan Program Pendataan RTLH</b></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.71in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><b>4.1.</b></font><span style="font-variant: normal"><font face="Cambria, serif"><b>
          </b></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>PERSIAPAN
          PROGRAM TINGKAT PUSAT-DAERAH</b></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Kegiatan
          persiapan program Pendataan RTLH di tingkat pemerintah daerah
          (</font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">rovinsi–</font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">kabupaten/kota</span></font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">)
          merupakan langkah awal pemerintah daerah untuk menjalankan
          kewajibannya di dalam penyelenggaraan bidang perumahan, yakni
          menyediakan basis data bidang RTLH-PKP yang terupdate dan valid.
          </font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>Database</i></font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          ini akan dijadikan dasar pengambilan keputusan dalam program
          pembangunan/</font></font></font><font color="#000000"> </font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">peningkatan
          kualitas RTLH. Kewajiban penyediaan basis data RTLH-PKP ini sesuai
          dengan amanat UU No. </font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">23</span></font></font></font><font color="#000000">
          </font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Tahun
          20</font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">1</span></font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">4
          tentang Pemerintah Daerah</font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">.</span></font></font></font><font color="#000000">&nbsp;
          </font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Langkah-langkah
          persiapan program pendataan RTLH/PKP di tingkat pemerintah daerah
          (mulai dari tingkat provinsi </font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">sampai
          dengan tingkat kecamatan</span></font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">)
          terdiri dari 4 kegiatan utama, yakni:</font></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>4.1.1</b></font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>
          </b></font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>PERSIAPAN
          FASILITASI PROGRAM PENDATAAN TINGKAT</b></font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>PUSAT-PROVINSI</b></font></font></p>
          <center>
            <table width="425" cellpadding="2" cellspacing="0">
              <col width="417">
              <tr>
                <td width="417" valign="top" bgcolor="#ffffff" style="border: 1.50pt solid #365f91; padding-top: 0.02in; padding-bottom: 0.02in; padding-left: 0.05in; padding-right: 0.08in">
                  <p class="western" align="justify" style="margin-left: 0.1in"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Persiapan</font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
                  fasilitasi program pendataan RTLH di Tingkat Pusat-provinsi
                  adalah serangkaian kegiatan awal yang dilaksanakan untuk: </font></font></font>
                  </p>
                  <p class="western" align="justify" style="margin-left: 0.35in; margin-right: 0.22in">
                  <font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">1.</font></font></font><span style="font-variant: normal"><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
                  </font></font></font></span><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Mengkondisikan
                  Tim PUPERA dan SKPD Bidang Perumahan Tingkat Provinsi untuk
                  menyadari, memahami dan siap menjalankan kewajibannya dalam
                  memfasilitasi program pendataan RTLH.</font></font></font></p>
                  <p class="western" align="justify" style="margin-left: 0.35in; margin-right: 0.22in">
                  <font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">2.</font></font></font><span style="font-variant: normal"><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
                  </font></font></font></span><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Membentuk-menetapkan
                  Tim Fasilitasi Program Tingkat Provinsi</font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">.</span></font></font></font></p>
                  <p class="western" align="justify" style="margin-left: 0.35in; margin-right: 0.22in">
                  <font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>3.</b></font></font></font><span style="font-variant: normal"><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>
                  </b></font></font></font></span><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Membangun
                  </font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>Readiness
                  </i></font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Tim
                  fasilitasi Program Tingkat Provinsi untuk untuk menjalankan
                  fasilitasi program pendataan RTLH di Tingkat Kota/kabupaten</font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">.</span></font></font></font></p>
                </td>
              </tr>
            </table>
          </center>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Rangkaian
          kegiatan persiapan yang dilakukan di Tingkat Pusat-Provinsi ini
          adalah </font></font></font>
          </p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US"><b>Gambar/Diagram
          4.2.</b></span></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>Persiapan
          Fasilitasi Program Tingkat Pusat-Prov</b></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US"><b>insi</b></span></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">1.</font></font></font><span style="font-variant: normal"><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></font></span><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Pembekalan
          tim inti fasilitasi program pendataan di tingkat pusat, berupa
          </font></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">kegiatan
          penguatan wawasan-pengetahuan-kemampuan serta ketrampilan seluruh Tim
          (PUPERA dan Perwakilan SKPD </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">tingkat
          provinsi</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">)
          dalam memfasilitasi pelaksanaan program pendataan RTLH di Tingkat
          Provinsi. </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">Pada</span></font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">kegiatan
          ini juga, termasuk di</font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt">dalamnya
          dilaksanakan sosialisasi berbagai hal terkait dengan pelaksanaan
          program pendataan RTLH dan fasilitasinya sebagai wujud komitmen
          urusan wajib </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">pemerintah
          daerah</span></font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt">di</font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">b</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">idang
          Perumahan.</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">2.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Pembentukan
          dan </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">enetapan
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">t</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">im
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">f</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">asilitasi
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">rogram
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">t</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ingkat
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">rovinsi
          dan </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">enerbitan
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ayung
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">h</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ukum.
          Tahapan ini merupakan rangkaian kegiatan untuk membentuk dan
          menetapkan tim fasilitasi program pendataan RTLH </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">tingkat
          </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Provinsi.
          Pembangunan tim ini juga termasuk kepada penyiapan perangkat-rencana
          kerja yang akan dilakukan tim inti dalam memfasilitasi program
          pendataan RTLH.</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>4.1.</b></font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>
          </b></font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>PERSIAPAN
          FASILITASI PROGRAM PENDATAAN</b></font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>TINGKAT</b></font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>PROVINSI
          dan KABUPATEN</b></font></font></p>
          <center>
            <table width="425" cellpadding="2" cellspacing="0">
              <col width="417">
              <tr>
                <td width="417" valign="top" bgcolor="#ffffff" style="border: 1.50pt solid #365f91; padding-top: 0.02in; padding-bottom: 0.02in; padding-left: 0.05in; padding-right: 0.08in">
                  <p class="western" align="justify" style="margin-left: 0.1in; margin-right: 0.12in">
                  <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Persiapan
                  fasilitasi program pendataan tingkat provinsi dan kabupaten
                  adalah serangkaian kegiatan awal yang dilaksanakan untuk: </font></font>
                  </p>
                  <p class="western" align="justify" style="margin-left: 0.35in; margin-right: 0.12in">
                  <font face="Cambria, serif"><font size="3" style="font-size: 12pt">1.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
                  </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Mengkondisikan
                  stakeholder tingkat provinsi dan Perwakilan SKPD bidang perumahan
                  di </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">t</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ingkat
                  </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">kabupaten/
                  kota</span></font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt">untuk
                  menyadari, memahami dan siap menjalankan kewajibannya dalam
                  memfasilitasi program pengembangan basis data RTLH di Tingkat
                  Kota/kabupaten. </font></font>
                  </p>
                  <p class="western" align="justify" style="margin-left: 0.35in; margin-right: 0.12in">
                  <font face="Cambria, serif"><font size="3" style="font-size: 12pt">2.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
                  </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Memilih-menetapkan
                  Kota/Kabupaten yang diprioritaskan terlebih dahulu sebagai lokasi
                  Demoplot (ujicoba) kegiatan pendataan-pengembangan sistem
                  </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>database
                  </i></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">RTLH
                  berbasis web </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>(online).</i></font></font></p>
                </td>
              </tr>
            </table>
          </center>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Rangkaian
          kegiatan persiapan yang dilakukan di </font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">tingkat
          provinsi-kabupaten/kota</span></font></font></font><font color="#000000">
          </font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">adalah
          </font></font></font>
          </p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US"><b>Gambar/Diagram
          4.3.</b></span></font></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>Persiapan
          Fasilitasi Progr</b></font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US"><b>a</b></span></font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>m
          Tingkat Prov</b></font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US"><b>insi</b></span></font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>-</b></font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US"><b>Kabupaten/Kota</b></span></font></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">1.</font></font></font><span style="font-variant: normal"><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></font></span><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Pembekalan
          program di tingkat provinsi-</font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">kebupaten/kota</span></font></font></font><font color="#000000">
          </font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">adalah
          </font></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">kegiatan
          penguatan wawasan-pengetahuan-kemampuan serta ketrampilan tim
          fasilitasi program tingkat provinsi dan perwakilan SKPD </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">tingkat
          kabupaten/kota</span></font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt">dalam
          pelaksanaan program pendataan RTLH. </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">Pada</span></font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">kegiatan
          ini juga, termasuk di</font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt">dalamnya
          dilaksanakan sosialisasi untuk pemasyarakatan berbagai hal terkait
          dengan pelaksanaan program pendataan RTLH dan fasilitasinya sebagai
          wujud komitmen urusan wajib </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">emerintah
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">d</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">aerah
          di </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">b</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">idang
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">erumahan.
          Pelaksanaan kegiatan pembekalan dilakukan di </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">t</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ingkat
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">rovinsi
          (</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">i</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">bukota
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">rovinsi)
          dengan mengundang perwakilan SKPD bidang perumahan dari setiap
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">kabupaten/kota</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">.
          Kegiatan dilakukan dengan berbagai cara, seperti lokakarya, workshop.</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">2.</font></font></font><span style="font-variant: normal"><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></font></span><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Penilaian
          dan </font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">enetapan
          </font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">kabupaten/kota
          sebagai sebagai demoplot program </span></font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">adalah
          </font></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">serangkaian
          kegiatan yang dilakukan tim fasilitasi program tingkat provinsi untuk
          menilai-memilih-menetapkan </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">kabupaten/kota</span></font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">yang
          memenuhi kriteria administratif sebagai Demoplot (lokasi uji coba)
          program pendataan RTLH.</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">3.</font></font></font><span style="font-variant: normal"><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></font></span><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>Collecting</i></font></font></font><font color="#000000">
          </font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">dan
          </font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US"><i>r</i></span></font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>eview
          </i></font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US"><i>d</i></span></font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>ata</i></font></font></font><font color="#000000">
          </font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">s</span></font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ekunder
          RTLH </font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">t</span></font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ingkat
          </font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">rovinsi-</font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">kabupaten/kota</span></font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">.
          </font></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Kegiatan
          ini merupakan mengumpulkan seluruh data sekunder yang terkait dengan
          kondisi RTLH yang sudah ada di tingkat provinsi. Data-data ini bisa
          diperoleh dari berbagai institusi pelaksana pendata (BPS), d</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">a</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">pat
          pula berasal dari data proyek/program sejenis yang sudah ada di
          tingkat nasional-tingkat provinsi. Setelah dikumpulkan, maka
          dilakukan review mengenai karakteristik data RTLH tersebut.</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>4.1.3.</b></font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>
          </b></font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>PEMBANGUNAN
          POKJA PENDATAAN RTLH TINGKAT </b></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US"><b>KABUPATEN/KOTA</b></span></font></font></p>
          <center>
            <table width="425" cellpadding="2" cellspacing="0">
              <col width="417">
              <tr>
                <td width="417" valign="top" bgcolor="#ffffff" style="border: 1.50pt solid #365f91; padding-top: 0.02in; padding-bottom: 0.02in; padding-left: 0.05in; padding-right: 0.08in">
                  <p class="western" align="justify"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Tahapan
                  ini merupakan kegiatan membangun institusi yang akan menjalankan
                  pelaksanaan program Pendataan RTLH di </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">tingkat
                  kabupaten/kota</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">.
                  Pembangunan institusi ini juga termasuk kepada penyiapan
                  perangkat-rencana kerja yang digunakan pada saat pendataan maupun
                  saat pengelolaan hasil pendataan. Bila </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">kabupaten/kota</span></font></font>
                  <font face="Cambria, serif"><font size="3" style="font-size: 12pt">sudah
                  memiliki Institusi (Pokja/unit kerja/dinas) menangani bidang
                  </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">erumahan,
                  sebaiknya pengembangan database RTLH ini menjadi bagian T</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">upoksi</span></font></font>
                  <font face="Cambria, serif"><font size="3" style="font-size: 12pt">institusi
                  tersebut dengan menambahkan beberapa SDM yang menangani kegiatan
                  pengembangan </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>database</i></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">.</font></font></p>
                </td>
              </tr>
            </table>
          </center>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Rangkaian
          kegiatan dalam pembangunan Pokja Pendataan RTLH Tingkat
          Kota/Kabupaten ini adalah:</font></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US"><b>Gambar/Diagram
          4.4.</b></span></font></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>Pembangunan
          Pokja Pendataan RTLH </b></font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US"><b>T</b></span></font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>ingkat
          </b></font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US"><b>Kabupaten/Kota</b></span></font></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>1.</b></font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>
          </b></font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Pembentukan
          dan </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">enetapan
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">okja
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">endataan
          RTLH </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">tingkat
          kabupaten/kota</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>.</b></font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Tahapan
          ini merupakan rangkaian kegiatan di</font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt">dalam
          pembentukan-penetapan institusi pendataan RTLH yang akan
          mengawal-melaksanakan program dan mengelola hasil pendataan RTLH di
          Tingkat Kota/Kabupaten. Beberapa pertimbangan yang dapat dirujuk
          dalam pembentukan:</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.94in; margin-right: 0.39in; margin-bottom: 0in">
          <span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">a
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Pembentukan
          Pokja dapat dilakukan dengan cara membentuk baru atau merevitalisasi
          Pokja yang sudah/pernah ada;</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.94in; margin-right: 0.39in; margin-bottom: 0in">
          <span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">b
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Cara
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">engusulan-</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">emilihan
          kandidat personil Pokja dapat dilakukan dengan cara mekanisme
          tertutup atau pengusulan terbuka;</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.94in; margin-right: 0.39in; margin-bottom: 0in">
          <span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">c
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Pembentukan
          tim ini di</font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt">dasari
          pertimbangan pemilihan anggota tim yang memenuhi kriteria </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US"><i>right
          person</i></span></font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>dan
          </i></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US"><i>right
          place</i></span></font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt">(</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">emilihan
          kandidat anggota tim untuk menempatkan jabatannya disesuaikan dengan
          wawasan-pengetahuan-pengalaman serta komitmen kerja cukup di
          bidangnya);</font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">dan</span></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.94in; margin-right: 0.39in; margin-bottom: 0in">
          <span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">d
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Struktur
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">o</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">rganisasi
          Pokja </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">endataan
          ini memiliki tim yang bertugas untuk melakukan pengelolaan data
          (Operator/Administrator) </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">t</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ingkat
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">kabupaten/kota</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">,
          dan tim </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>entry</i></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">/input
          data di </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">tingkat
          kabupaten/kota – kecamatan.</span></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>2.</b></font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>
          </b></font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Penerbitan
          payung hukum pokja</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>.
          </b></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Kegiatan
          ini merupakan upaya memberikan kekuatan hukum akan keberadaan Pokja
          Pendataan RTLH di Tingkat Kota/kabupaten. Kekuatan hukum yang
          dimaksud disini adalah keberadaan Surat Keputusan (SK)
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">walikota/bupati</span></font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">perihal
          pengukuhan-penetapan Pokja Pendataan;</font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">dan</span></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>3.</b></font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>
          </b></font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Pembekalan
          pokja pendataan RTLH tingkat kota/kab.-kecamatan adalah kegiatan
          penguatan wawasan-pengetahuan-kemampuan serta ketrampilan Pokja RTLH
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">tingkat
          kabupaten/kota</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">-</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">k</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ecamatan
          dalam pelaksanaan program pendataan RTLH. </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">Pada</span></font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">kegiatan
          ini juga, termasuk di</font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt">dalamnya
          dilaksanakan sosialisasi berbagai hal terkait dengan pelaksanaan
          program pendataan RTLH sebagai wujud komitmen urusan wajib pemerintah
          daerah di </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">bidang
          perumahan</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">.
          Pelaksanaan pembekalan ini dilakukan di </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">ibukota
          kabupaten/kota</span></font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt">dengan
          mengundang seluruh personil Pokja </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">pendataan
          tingkat kabupaten/kota</span></font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt">dan
          perwakilan SKPD </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">tingkat
          kecamatan</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">.
          Pelaksanaan Pembekalan dilakukan dengan cara </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">loka
          karya/workshop-pelatihan/</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US"><i>training</i></span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">.</span></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>4.1.4.</b></font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>
          </b></font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>PERSIAPAN
          PELAKSANAAN PROGRAM</b></font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>PENDATAAN</b></font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>TINGKAT
          </b></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US"><b>KABUPATEN/KOTA</b></span></font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>-
          KECAMATAN </b></font></font>
          </p>
          <center>
            <table width="425" cellpadding="2" cellspacing="0">
              <col width="417">
              <tr>
                <td width="417" valign="top" bgcolor="#ffffff" style="border: 1.50pt solid #365f91; padding-top: 0.02in; padding-bottom: 0.02in; padding-left: 0.05in; padding-right: 0.08in">
                  <p class="western" align="justify" style="margin-left: 0.1in; margin-right: 0.12in">
                  <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Persiapan
                  Kegiatan Pendataan RTLH di </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">tingkat
                  kabupaten/kota/kecamatan </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">adalah
                  serangkaian kegiatan awal yang dilaksanakan oleh Pokja Pendataan
                  RTLH di </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">tingkat
                  kabupaten/kota/kecamatan </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">guna
                  menyiapkan perangkat-rencana kerja yang akan dilakukan.</font></font></p>
                </td>
              </tr>
            </table>
          </center>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Rangkaian
          kegiatan dalam persiapan pelaksanaan program Pendataan RTLH Tingkat
          Kota/Kabupaten ini adalah:</font></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US"><b>Gambar/Diagram
          4.5.</b></span></font></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>Persiapan
          Pelaksanaan Pendataan Tingkat </b></font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US"><b>Kabupaten/Kota</b></span></font></font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>.
          Kecamatan</b></font></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">1.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Penyiapan</font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          perangkat kerja</font></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          adalah kegiatan mengadakan sarana dan prasarana yang dibutuhkan oleh
          tim untuk melaksanakan kegiatan pendataan dan membangun sistem
          pengembangan database RTLH di Tingkat </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">Kabupaten/kota</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">.
          Adapun perangkat kerja yang dimaksud adalah: Ruang/Posko Kerja,
          Peralatan Sistem Aplikasi Pendataan, Instrumen Pendataan (Form-form
          Pendataan)</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">;</span></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">2.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Perumusan
          rencana </font></font><font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">kerja</font></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          merupakan upaya-upaya perumusan rencana kerja didalam pelaksanaan
          pendataan RTLH dan pengolahan datanya di </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">t</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ingkat
          K</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">abupaten</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">/kabupaten.</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">3.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Review
          data sekunder RTLH tingkat </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">kabupaten/kota</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">.
          Kegiatan ini merupakan mengumpulkan seluruh data sekunder yang
          terkait dengan kondisi RTLH yang sudah ada di tingkat </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">kabupaten/kota.
          </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Data-data
          ini bisa diperoleh dari berbagai institusi pelaksana pendata (BPS),
          d</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">a</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">pat
          pula berasal dari data&nbsp; proyek/program sejenis yang sudah ada di
          tingkat </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">kabupaten/kota</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">.
          Setelah dikumpulkan, maka dilakukan </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>review</i></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          mengenai karakteristik data RTLH tersebut.</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">4.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Pembekalan
          pokja pendataan RTLH tingkat kecamatan-kelurahan/desa adalah kegiatan
          penguatan wawasan-pengetahuan-kemampuan serta ket</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">e</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">rampilan
          Pokja RTLH tingkat kecamatan – kelurahan/desa dalam pelaksanaan
          program pendataan RTLH. Pelaksanaan pembekalan ini dilakukan di
          tingkat kecamatan dengan mengundang seluruh personil Pokja </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">endataan
          tingkat kecamatan dan setiap perwakilan perangkat kelurahan/desa yang
          ada di wilayah Kecamatan. Pelaksanaan </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">embekalan
          dilakukan dengan cara </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">loka
          karya/workshop-pelatihan/</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US"><i>training</i></span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">.</span></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><b>4.2.</b></font><span style="font-variant: normal"><font face="Cambria, serif"><b>
          </b></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>PELAKSANAAN
          PENDATAAN TINGKAT KELURAHAN/DESA</b></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Pendataan
          di tingkat kelurahan/desa, adalah serangkaian kegiatan yang dilakukan
          untuk mengumpulkan sejumlah data-data yang terkait dengan RTLH (Rumah
          Tidak Layak Huni) di </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">t</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ingkat
          Kelurahan/desa. Adapun tahapan pelaksanaan Pendataan RTLH di </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">tingkat
          kelurahan/desa</span></font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt">mencakup
          persiapan sosial, pembangunan pokja Pendataan dan perangkat kerjanya,
          serta Pelaksanaan Pendataan.</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>4.2.1</b></font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>
          </b></font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>PERSIAPAN
          SOSIAL</b></font></font></p>
          <center>
            <table width="425" cellpadding="2" cellspacing="0">
              <col width="417">
              <tr>
                <td width="417" valign="top" bgcolor="#ffffff" style="border: 1.50pt solid #365f91; padding-top: 0.02in; padding-bottom: 0.02in; padding-left: 0.05in; padding-right: 0.08in">
                  <p class="western" align="justify" style="margin-left: 0.1in; margin-right: 0.12in">
                  <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Proses
                  persiapan sosial masyarakat adalah serangkaian proses awal yang
                  dilakukan guna mewujudkan</font></font><font color="#231f20"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
                  proses partisipatif masyarakat untuk membangun kesiapan
                  </font></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">pelaksanaan</font></font><font color="#231f20"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
                  pendataan RTLH-PKP. Kesiapan tersebut tidak hanya ditetapkan oleh
                  perangkat kelurahan/tokoh-tokoh masyarakat, namun melibatkan
                  representasi sebagian besar masyarakat.</font></font></font></p>
                </td>
              </tr>
            </table>
          </center>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Persiapan
          sosial ini dimaksudkan untuk:</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">a.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Menciptakan
          kondisi dimana masyarakat menyadari mengenai manfaat kegiatan
          pendataan RTLH-PKP;</font></font>
          </p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">b.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">melibatkan
          masyarakat secara aktif sehingga meningkatkan pemahaman peran dan
          tanggung jawab masyarakat dan aparat pemerintah desa untuk terlibat
          aktif dalam kegiatan pendataan RTLH-PKP.</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Adapun
          kegiatan yang dilakukan dalam tahap persiapan sosial ini adalah :</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US"><b>Gambar/Diagram
          4.6.</b></span></font></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>Persiapan</b></font></font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>Sosial</b></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">1.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Orientasi
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">w</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ilayah
          dan </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">emetaan
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">s</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">osial,
          adalah kegiatan berkeliling desa untuk memperoleh informasi awal
          mengenai kondisi wilayah dan peta sosial-masyarakat setempat. Untuk
          lokasi-lokasi dimana tim pelaksana fasilitasi sudah mengenali dengan
          baik pihak-pihak yang akan dilibatkan dalam proses pendataan, maka
          kegiatan ini tidak perlu dilakukan.</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">2.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Sosialisasi
          dan </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">k</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ontrak
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">s</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">osial</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>,
          </b></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Kontrak
          sosial adalah perjanjian tertulis antara masyarakat dengan pemerintah
          kota/kabupaten yang membidangi pendataan (Pokja RTLH/PKP), yang mana
          di</font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt">dalamnya,
          masyarakat berkomitmen akan terlibat aktif dalam kegiatan pendataan
          RTLH di </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">t</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ingkat
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">kelurahan/desa</span></font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">dimana
          mereka tinggal. Kontrak ini merupakan salah satu bentuk formal dari
          komitmen masyarakat yang merefleksikan keterikatan secara mendalam
          terhadap nilai-nilai sosial-moral dalam rangka menjamin
          terselenggaranya kegiatan pendataan RTLH-PKP sesuai dengan
          tujuan-prinsip-dasar dan pendekatan proses.</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><b>4.3.</b></font><span style="font-variant: normal"><font face="Cambria, serif"><b>
          </b></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>PEMBANGUNAN
          POKJA PENDATAAN TINGKAT</b></font></font> <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>KELURAHAN/DESA</b></font></font></p>
          <center>
            <table width="425" cellpadding="2" cellspacing="0">
              <col width="417">
              <tr>
                <td width="417" valign="top" bgcolor="#ffffff" style="border: 1.50pt solid #365f91; padding-top: 0.02in; padding-bottom: 0.02in; padding-left: 0.05in; padding-right: 0.08in">
                  <p class="western" align="justify" style="margin-right: 0.12in"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Tahapan
                  </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">i</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ni
                  </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">m</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">erupakan
                  </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">k</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">egiatan
                  </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">embentukan-</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">embangunan
                  </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">i</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">nstitusi-</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">k</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">elembagaan
                  </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">y</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ang
                  </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">a</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">kan
                  </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">m</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">engawal-</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">m</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">engelola
                  </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">d</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">an
                  Melaksanakan Kegiatan Pendataan RTLH </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">di
                  t</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ingkat
                  </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">kelurahan/desa</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">.
                  Pembangunan </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">i</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">nstitusi
                  Ini </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">j</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">uga
                  </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">t</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ermasuk
                  </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">k</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">epada
                  </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">enyiapan
                  </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">erangkat
                  dan </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">r</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">encana
                  </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">k</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">erja
                  </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">y</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ang
                  </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">a</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">kan
                  </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">d</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ilakukan
                  </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ada
                  </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">elaksanaan
                  </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">endataan.
                  </font></font>
                  </p>
                </td>
              </tr>
            </table>
          </center>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Adapun
          rangkaian kegiatan yang dilakukan pada tahap ini adalah</font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">:</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font color="#000000"><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US"><b>Gambar/Diagram
          4.7.</b></span></font></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>Pembangunan
          Pokja Pendataan Tingkat Kelurahan/desa</b></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif">1.</font><span style="font-variant: normal"><font face="Cambria, serif">
          </font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Pembentukan
          Pokja </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">endataan.
          Tahapan ini merupakan kegiatan membentuk kelompok kerja (Pokja) yang
          akan menjalankan kegiatan pendataan-RTLH di </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">tingkat
          kelurahan/desa</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">.
          Dasar pembentukan tim ini didasari sejumlah kriteria yang ditentukan
          oleh masyarakat desa melalui proses </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">refleksi
          kepemimpinan</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">.</font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Beberapa
          hal yang perlu diperhatikan dalam pembentukan Pokja ini adalah:</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">a.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Dibentuk
          dari, oleh dan untuk masyarakat desa</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">;</span></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">b.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Cara
          pembentukan pokja ini dapat dilakukan dengan cara pembentukan baru
          atau revitalisasi P</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">okja
          </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">yang
          pernah/sudah ada dengan beberapa penyesuaian-penyempurnaan aspek
          struktur organisasi-ketrampilan dasar; dan</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">c.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Struktur
          organisasi </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">P</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">okja
          Pendataan ini minimum terdiri dari penasihat/pengawas (kepala
          desa/lurah), pengurus inti (ketua-sekretaris-bendahara), Tim
          pemetaan, tim validasi, kader pendataan.</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif">2.</font><span style="font-variant: normal"><font face="Cambria, serif">
          </font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Penerbitan
          payung hukum</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>,
          </b></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">merupakan
          upaya memberikan kekuatan hukum akan keberadaan Pokja </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">endataan
          RTLH-PKP di </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">tingkat
          kelurahan/desa</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">.
          Kekuatan hukum yang dimaksud disini adalah keberadaan Surat Keputusan
          (SK) kepala desa/lurah perihal pengukuhan-penetapan Pokja pendataan
          RTLH</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">;</span></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif">3.</font><span style="font-variant: normal"><font face="Cambria, serif">
          </font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Pembekalan
          Pokja</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>,
          </b></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">merupakan
          kegiatan penguatan wawasan-pengetahuan-kemampuan serta ketrampilan
          tim pokja pendataan- pemetaan swadaya dalam melaksanakan kegiatan
          pendataan RTLH</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">;</span></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif">4.</font><span style="font-variant: normal"><font face="Cambria, serif">
          </font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Penyiapan
          perangkat dan rencana kerja. Tahapan ini merupakan rangkaian kegiatan
          yang dilakukan Pokja </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">t</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ingkat
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">kelurahan/desa</span></font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">dalam
          menyiapkan:</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">a.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Perangkat
          kerja (sarana, prasarana kerja) yang diperlukan dalam pelaksanaan
          pendataan; dan</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">b.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Rencana
          kerja yang akan dilakukan dalam pelaksanaan pendataan.</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif">5.</font><span style="font-variant: normal"><font face="Cambria, serif">
          </font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>Collecting</i></font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">dan
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">r</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>eview</i></font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">d</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ata
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">s</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ekunder
          RTLH. Kegiatan ini merupakan collecting dan review kondisi basis data
          sekunder yang sudah tersedia di </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">tingkat
          </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">kelurahan/desa
          yang terkait </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">b</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">idang
          RTLH-PKP. Data-data sekunder ini bisa diperoleh dari berbagai sumber,
          baik data proyek/program sejenis yang sudah pernah dilakukan di
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">tingkat
          kelurahan/ desa</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">.
          Tim pemetaan swadaya dapat memanfaatkan data atau informasi yang
          diperoleh melalui kegiatan serupa, yang dilakukan oleh PNPM Mandiri
          Perkotaan atau Perdesaan selama data atau informasi itu dinilai masih
          relevan atau valid.</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <br/>

          </p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><b>4.4.</b></font><span style="font-variant: normal"><font face="Cambria, serif"><b>
          </b></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>PELAKSANAAN
          PENDATAAN</b></font></font></p>
          <center>
            <table width="448" cellpadding="2" cellspacing="0">
              <col width="440">
              <tr>
                <td width="440" valign="top" bgcolor="#ffffff" style="border: 1.50pt solid #365f91; padding-top: 0.02in; padding-bottom: 0.02in; padding-left: 0.05in; padding-right: 0.08in">
                  <p class="western" align="justify"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Kegiatan
                  ini merupakan kegiatan kunci untuk menghasilkan database yang
                  baik dan dapat digunakan didalam pengambilan keputusan
                  pembangunan/ peningkatan kualitas RTLH di </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">tingkat
                  kelurahan/desa</span></font></font></p>
                </td>
              </tr>
            </table>
          </center>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <br/>

          </p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Adapun
          rangkaian kegiatan yang dilakukan pada tahap ini adalah:</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.39in; margin-right: 0.39in; margin-bottom: 0in">
          <br/>

          </p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif">1.</font><span style="font-variant: normal"><font face="Cambria, serif">
          </font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">Pendataan</span></font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Swadaya
          adalah rangkaian kegiatan yang dilakukan oleh masyarakat setempat
          untuk mengumpulkan – menilai data RTLH terkait kondisi fisik RTLH
          dan pemilik RTLH. Kegiatan ini dilakukan dengan partisipasi aktif
          masyarakat yang dapat didampingi oleh Faskel (Fasilitator
          Kelurahan)/TPM. </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">Pendataan</span></font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Swadaya
          ini dapat dilakukan dengan 2 cara, yakni:</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.94in; margin-right: 0.38in; margin-bottom: 0in; line-height: 120%; orphans: 2; widows: 2; page-break-before: auto">
          <span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">a.
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">Pendataan
          </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US"><i>On
          Site</i></span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">,
          Pendataan unit RTLH dilakukan secara lansung melalui </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">penelusuran
          lokasi/survey transek</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">.
          Dengan metoda ini, Tim pemetaan secara langsung memplotting lokasi
          unit RTLH dalam peta dan melakukan verifikasi data RTLH sesuai dengan
          form pendataan yang sudah disediakan;</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.94in; margin-right: 0.38in; margin-bottom: 0in; line-height: 120%; orphans: 2; widows: 2">
          <span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">b.
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">Pendataan
          </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><i>Offsite</i></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">,
          Pendataan unit RTLH yang dilakukan secara tidak langsung, dimana
          proses pemetaan diawali rembuk warga, yang dihadiri Ketua RW-RT,
          Tokoh masyarakat, wanita, dll. Dalam acara yang dipimpin oleh tim
          pemetaan dan difasilitasi oleh faskel/TPM ini, dilakukan identifikasi
          pemilik RTLH dan lokasinya dan pengambilan foto rumah yang
          diidentifikasi. Setelah semuanya terpetakan, dilanjutkan dengan
          kunjungan langsung ke setiap lokasi unit RTLH yang teridentifikasi.</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.94in; margin-right: 0.38in; margin-bottom: 0in; line-height: 120%; orphans: 2; widows: 2">
          <br/>

          </p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif">2.</font><span style="font-variant: normal"><font face="Cambria, serif">
          </font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Rekap
          data hasil </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">pendataan
          </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">dan
          </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">analisis.
          Kegiatan ini merupakan penyusunan ulang-merapihkan data hasil
          pemetaan dalam bentuk yang lebih terstruktur berupa:</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <br/>

          </p>
          <p class="western" align="justify" style="margin-left: 1in; margin-right: 0.38in; margin-bottom: 0in; line-height: 120%; orphans: 2; widows: 2; page-break-before: auto">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">1.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Penyempurnaan
          Form penilaian indikator RTLH-MBR </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">(questioner)
          </span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">untuk
          setiap unit yang teridentifikasi, terutama terkait dengan pencatatan
          kode foto.</font></font></p>
          <p class="western" align="justify" style="margin-left: 1in; margin-right: 0.38in; margin-bottom: 0in; line-height: 120%; orphans: 2; widows: 2">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">Setelah
          data hasil pendataan sudah terekap semua, kemudian dilakukan analisis
          secara bersama-sama untuk menentukan permasalahan-potensi yang
          melekat pada kondisi fisik RTLH dalam skala desa. Kegiatan ini
          dilakukan bersama-sama tim pemetaan dengan bantuan teknis dari tim
          fasilitator/TPM.</font></font></p>
          <p class="western" align="justify" style="margin-left: 1in; margin-right: 0.38in; margin-bottom: 0in; line-height: 120%; orphans: 2; widows: 2">
          <br/>

          </p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif">3.</font><span style="font-variant: normal"><font face="Cambria, serif">
          </font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Perumusan
          rencana tindak. Kegiatan ini merupakan rangkaian kegiatan rembuk
          warga untuk merumuskan Program dan kegiatan yang disusun menurut
          skala prioritas dalam jangka waktu tertentu (misal 5 tahun)
          menggunakan pendekatan berdasarkan kebutuhan masyarakat desa.
          Penyusunan rencana tindak ini disusun secara komprehensif sesuai
          dengan kebutuhan desa berdasarkan permasalahan dan potensi.</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <br/>

          </p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif">4.</font><span style="font-variant: normal"><font face="Cambria, serif">
          </font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Perumusan
          aturan bersama. Aturan </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">bersama</span></font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">adalah
          Aturan-aturan kesepakatan dan komitmen warga/komunitas di&nbsp;
          kelurahan/desa, untuk mewujudkan:</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.89in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">1.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Visi
          misi kualitas permukiman yang teratur, aman dan sehat-layak huni;</font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-ID">dan</span></font></font></p>
          <p class="western" align="justify" style="margin-left: 0.89in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">2.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Mewujudkan
          program dan kegiatan yang telah dirumuskan dalam agenda program
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">p</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">eningkatan
          </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">k</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">ualitas
          RTLH.</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.89in; margin-right: 0.39in; margin-bottom: 0in">
          <br/>

          </p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif">5.</font><span style="font-variant: normal"><font face="Cambria, serif">
          </font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Uji
          publik. Uji publik dilakukan dalam rangka memberi kesempatan
          masyarakat untuk mencermati dan melakukan koreksi-penyempurnaan
          (apabila perlu) terhadap draft dokumen hasil pendataan RTLH-PKP.
          Kegiatan Uji publik ini dilakukan untuk:</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.9in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">1.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Mempublikasikan
          semua proses dan hasil yang disepakati bersama selama kegiatan
          pendataan;</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.9in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">2.</font></font><span style="font-variant: normal"><font face="Cambria, serif"><font size="3" style="font-size: 12pt">
          </font></font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Memberikan
          waktu kepada setiap anggota masyarakat untuk menyampaikan
          sanggahan-masukan-saran terhadap hasil pendataan secara menyeluruh.</font></font></p>
          <p class="western" align="justify" style="margin-left: 0.9in; margin-right: 0.39in; margin-bottom: 0in">
          <br/>

          </p>
          <p class="western" align="justify" style="margin-left: 0.64in; margin-right: 0.39in; margin-bottom: 0in">
          <font face="Cambria, serif">6.</font><span style="font-variant: normal"><font face="Cambria, serif">
          </font></span><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Penerbitan
          S</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">K</span></font></font>
          <font face="Cambria, serif"><font size="3" style="font-size: 12pt">kepala
          desa/lurah atas dokumen hasil pendataan</font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><b>.
          </b></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">Kegiatan
          ini merupakan upaya memberikan kekuatan hukum sebagai hasil akhir
          kegiatan pendataan yang dilakukan oleh masyarakat di </font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt"><span lang="en-US">tingkat
          kelurahan/desa</span></font></font><font face="Cambria, serif"><font size="3" style="font-size: 12pt">.
          Kekuatan hukum yang dimaksud disini adalah keberadaan Surat Keputusan
          (SK) kepala desa/lurah.</font></font></p>
          <p class="western" align="justify" style="margin-bottom: 0in; line-height: 100%">
          <br/>

          </p>
            <div class="gap"></div>
        </div>



        <footer id="main-footer">
            <?php include_once "layout_front/footer.php"; ?>
        </footer>

        <script src="<?php echo base_url('asset/frontend') ?>/js/jquery.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/bootstrap.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/slimmenu.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/datepicker.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/bootstrap-timepicker.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/nicescroll.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/dropit.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/ionrangeslider.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/icheck.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/fotorama.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/typeahead.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/card-payment.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/magnific.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/owl-carousel.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/fitvids.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/tweet.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/countdown.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/gridrotator.js"></script>
        <script src="<?php echo base_url('asset/frontend') ?>/js/custom.js"></script>
    </div>
</body>

</html>
