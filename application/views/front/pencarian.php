<link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/bootstrap.css">
<script src="<?php echo base_url('asset/frontend'); ?>/js/bootstrap.js"></script>
<div class="container">
  <div class="row">
    <div class="col-md-4">
      <form action="<?php echo site_url('front/pencarian'); ?>" method="post">
        <div class="form-group">
          <label>Kecamatan</label>
          <select class="form-control" name="kecamatan" required>
            <option value="">Pilih Kecamatan</option>
            <?php foreach ($kecamatan->result() as $row): ?>
              <option value="<?php echo $row->id_kecamatan; ?>"><?php echo $row->nm_kecamatan; ?></option>
            <?php endforeach; ?>
          </select>
        </div>
        <input type="submit" name="btnSubmit" value="Ok" class="btn btn-primary">
      </form>
    </div>
  </div>
</div>
