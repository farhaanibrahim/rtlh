<!DOCTYPE HTML>
<?php foreach($instansi->result() as $is_row); ?>
<html>

<head>
    <title><?php echo $title; ?></title>


    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="Template, html, premium, themeforest" />
    <meta name="description" content="Traveler - Premium template for travel companies">
    <meta name="author" content="Tsoy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- GOOGLE FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600' rel='stylesheet' type='text/css'>
    <!-- /GOOGLE FONTS -->
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/icomoon.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/styles.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/mystyles.css">
    <script src="<?php echo base_url('asset/frontend'); ?>/js/modernizr.js"></script>
	<!-- /FAVICON---->
	<link rel="shortcut icon" href="<?php echo base_url("upload/$is_row->logo"); ?>"/>

</head>

<body>

    <!-- FACEBOOK WIDGET -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- /FACEBOOK WIDGET -->
    <div class="global-wrap">
        <header id="main-header">
			<?php include_once "layout_front/header.php";  ?>
		</header>

        <div class="container">
            <h4 style="margin:70px 0 0 0; font-size:48px;">Login on Sistem Informasi RLTH</h4>
        </div>

        <div class="gap"></div>


        <div class="container">
            <div class="row" data-gutter="60">
                <div class="col-md-7">
                    <h3>Selamat Datang di <?php echo $is_row->nama ?></h3>
                    <p><?php echo $is_row->tentang_singkat ?></p>
                </div>
                <div class="col-md-5">
                    <h3>Login</h3>
                    <form method="post" id="form_login" action="<?php echo site_url('backend/login') ?>">
                        <div class="form-group form-group-icon-left"><i class="fa fa-user input-icon input-icon-show"></i>
                            <label>Username</label>
                            <input class="form-control" name="username" placeholder="Username" type="text" autofocus />
                        </div>
                        <div class="form-group form-group-icon-left"><i class="fa fa-lock input-icon input-icon-show"></i>
                            <label>Password</label>
                            <input class="form-control" name="password" type="password" placeholder="Password" />
                        </div>
                        <input class="btn btn-primary" type="submit" value="Sign in" />
                    </form>
					<br>
					<div class="alert alert-success" style="display:none;" id="info_sukses">
						<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
						</button>
						<p class="text-small">Login Berhasil.. Mohon Tunggu halaman selanjutnya.. </p>
					</div>
					<div class="alert alert-danger" style="display:none;" id="info_gagal">
						<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
						</button>
						<p class="text-small">Login Gagal!!.. Username & Password tidak cocok.</p>
					</div>
					<div class="progress progress-striped active" id="prog_bar" style="display:none;">
						<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
							<span class="info_prog2">Mohon Tunggu.. </span>
						</div>
					</div>

                </div>
			</div>
        </div>



        <div class="gap"></div>
		<footer id="main-footer">
            <?php include_once "layout_front/footer.php"; ?>
        </footer>
		
        <script src="<?php echo base_url('asset/frontend'); ?>/js/jquery.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/jquery.form.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/bootstrap.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/slimmenu.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/bootstrap-timepicker.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/nicescroll.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/dropit.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/ionrangeslider.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/icheck.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/fotorama.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/typeahead.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/card-payment.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/magnific.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/owl-carousel.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/fitvids.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/tweet.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/countdown.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/gridrotator.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/custom.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				var options2 = { 
					beforeSend: function() {
						$("#info_sukses").hide();
						$("#info_gagal").hide();	
						$("#prog_bar").show();	
					},
					success: function() {
					},
					complete: function(response) {
						$("#prog_bar").hide();
						if(response.responseText == '1'){
							$("#info_sukses").show();
							window.location = "<?php echo site_url('backend') ?>";
						}else{
							$("#info_gagal").show();
						}
					},
					error: function(){
						//$("#form_edit_ads #message").html("<font color='red'> ERROR: unable to upload files</font>");
					}
				}; 
				$("#form_login").ajaxForm(options2);
			});
		</script>

    </div>
</body>

</html>


