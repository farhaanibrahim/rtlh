<!DOCTYPE HTML>
<?php foreach($instansi->result() as $is_row); ?>
<?php foreach($data_login->result() as $is_row2); ?>
<?php foreach($data_row->result() as $row); ?>
<html>

<head>
    <title><?php echo $title; ?></title>


    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="Template, html, premium, themeforest" />
    <meta name="description" content="Traveler - Premium template for travel companies">
    <meta name="author" content="Tsoy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

     <!-- GOOGLE FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600' rel='stylesheet' type='text/css'>
    <!-- /GOOGLE FONTS -->
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/icomoon.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/styles.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/mystyles.css">
    <script src="<?php echo base_url('asset/frontend'); ?>/js/modernizr.js"></script>
	<!-- /FAVICON---->
	<link rel="shortcut icon" href="<?php echo base_url("upload/$is_row->logo"); ?>"/>


</head>

<body>

    <!-- FACEBOOK WIDGET -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- /FACEBOOK WIDGET -->
    <div class="global-wrap">
        <header id="main-header">
			<?php include_once "layout_back/header.php";  ?>
		</header>
		
		<div class="container">
            <h1 class="page-title">Edit | RTLH</h1>
        </div>




        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <?php include_once "layout_back/menu_nav.php"; ?>
                </div>
                <div class="col-md-9">
                    <form action="<?php echo site_url('backend/edit_rtlh_ac'); ?>" method="post" enctype="multipart/form-data" id="form_input">
						<div class="row">
							<div class="col-md-4">
							<h4>A. Identifikasi Lokasi</h4>
								<div class="form-group form-group-icon-left"><i class="fa fa-user input-icon"></i>
									<label>Kecamatan</label>
									<select name="kecamatan" id="kecamatan" class="form-control" required>
										<option value="<?php echo $row->rtlh_kecamatan ?>"><?php echo $row->nm_kecamatan ?></option>
										<option value="">Pilih Kecamatan disini</option>
										<?php foreach($kecamatan->result() as $kec){ ?>
											<option value="<?php echo $kec->id_kecamatan ?>"><?php echo $kec->nm_kecamatan ?></option>
										<?php } ?>
									</select>
								</div>
								<div class="form-group form-group-icon-left"><i class="fa fa-user input-icon"></i>
									<label>Kelurahan</label>
									<select name="kelurahan" id="kelurahan" class="form-control" required>
										<option value="<?php echo $row->rtlh_kelurahan ?>"><?php echo $row->nm_desa; ?></option>
										<option value="">-- Silahkan Pilih Kelurahan disini --</option>
									</select>
								</div>
								<div class="form-group form-group-icon-left"><i class="fa fa-user input-icon"></i>
									<label>RW</label>
									<select name="rw" id="rw" class="form-control" required>
									<option value="<?php echo $row->rtlh_rw ?>">RW No : <?php echo $row->rtlh_rw.'- '.$row->nm_rw ?></option>
										<option value="">-- Silahkan Pilih RW disini --</option>
									</select>
								</div>
								<div class="form-group form-group-icon-left"><i class="fa fa-user input-icon"></i>
									<label>RT</label>
									<select name="rt" id="rt" class="form-control" required>
										<option value="<?php echo $row->rtlh_rt ?>">RT No : <?php echo $row->rtlh_rt.'- '.$row->nm_rt ?></option>
										<option value="">-- Silahkan Pilih RT disini --</option>
									</select>
								</div>
								<div class="form-group form-group-icon-left"><i class="fa fa-envelope input-icon"></i>
									<label>No KTP</label>
									<input name="no_ktp" class="form-control" id="no_ktp" placeholder="ex: 01" type="number" value="<?php echo $row->no_ktp ?>" required/>
								</div>
								<div class="form-group form-group-icon-left"><i class="fa fa-envelope input-icon"></i>
									<label>No KK</label>
									<input name="no_kk" class="form-control" id="no_k" placeholder="ex: 01" type="number" value="<?php echo $row->no_kk ?>" required/>
								</div>
								<div class="form-group form-group-icon-left"><i class="fa fa-envelope input-icon"></i>
									<label>Nama Pemilik</label>
									<input name="nm_pemilik" class="form-control" id="nm_pemilik" placeholder="Nama Pemilik KTP" value="<?php echo $row->nm_pemilik ?>" type="text" />
								</div>
								<div class="form-group form-group-icon-left"><i class="fa fa-user input-icon"></i>
									<label>Jenis Kelamin</label>
									<select name="jenis_kelamin" id="jenis_kelamin" class="form-control" required>
										<option value="<?php echo $row->jenis_kelamin ?>">Sebelumnya : <?php echo $row->jenis_kelamin ?></option>
										<option value="Pria">Pria</option>
										<option value="Wanita">Wanita</option>
									</select>
								</div>
								<div class="form-group form-group-icon-left"><i class="fa fa-user input-icon"></i>
									<label>Status Pernikahan</label>
									<select name="status_pernikahan" id="status_pernikahan" class="form-control" required>
										<option value="<?php echo $row->status_pernikahan ?>">Sebelumnya : <?php echo $row->status_pernikahan; ?></option>
										<option value="single/lajang">Single/Lajang</option>
										<option value="menikah">Menikah</option>
										<option value="duda/janda">Duda/Janda</option>
									</select>
								</div>
								<div class="form-group form-group-icon-left"><i class="fa fa-user input-icon"></i>
									<label>Prioritas</label>
									<select name="prioritas" id="prioritas" class="form-control" required>
										<option value="<?php echo $row->prioritas ?>">Sebelumnya : <?php echo $row->prioritas ?></option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
									</select>
								</div>
								<div class="form-group form-group-icon-left"><i class="fa fa-list input-icon"></i>
									<label>Latitude</label>
									<input class="form-control" id="lat" name="lat" placeholder="" type="text" value="<?php echo $row->latitude ?>" />
								</div>
								<div class="form-group form-group-icon-left"><i class="fa fa-list input-icon"></i>
									<label>Longtitude</label>
									<input class="form-control" id="lon" name="lon" placeholder="" type="text" value="<?php echo $row->longitude ?>" />
								</div>
								<div class="form-group form-group-icon-left"><i class="fa fa-list input-icon"></i>
									<label>Alamat</label>
									<textarea name="almt" id="almt" class="form-control" ><?php echo $row->rtlh_almt ?></textarea>
								</div>
								<div class="form-group form-group-icon-left"><i class="fa fa-list input-icon"></i>
									<label>Keterangan</label>
									<textarea name="keterangan" id="keterangan" class="form-control" ><?php echo $row->keterangan ?></textarea>
								</div>
							</div>
							
							<div class="col-md-4">
								<h4>B. Pekerjaan & Penghasilan</h4>
								<div class="form-group form-group-icon-left"><i class="fa fa-envelope input-icon"></i>
									<label>Bantuan yang diterima</label>
									<input name="bantuan_yang_diterima" class="form-control" id="bantuan_yang_diterima" placeholder="Bantuan yang diterima" value="<?php echo $row->bantuan_yang_diterima ?>" type="text"/>
								</div>
								<div class="form-group form-group-icon-left"><i class="fa fa-user input-icon"></i>
									<label>Pekerjaan</label>
									<select name="pekerjaan" id="pekerjaan" class="form-control" required>
										<option value="<?php echo $row->pekerjaan ?>">Sebelumnya : <?php echo $row->pekerjaan ?></option>
										<option value="PNS/Polri/TNI">PNS/Polri/TNI</option>
										<option value="Karyawan Swasta">Karyawan Swasta</option>
										<option value="Petani">Petani</option>
										<option value="Buruh">Buruh</option>
										<option value="Lainnya">Lainnya</option>
									</select>
								</div>
								<div class="form-group form-group-icon-left"><i class="fa fa-user input-icon"></i>
									<label>Intensitas Penghasilan</label>
									<select name="intensitas_penghasilan" id="intensitas_penghasilan" class="form-control" >
										<option value="<?php echo $row->intensitas_penghasilan; ?>">Sebelumnya : <?php echo $row->intensitas_penghasilan ?></option>
										<option value="Tetap">Tetap</option>
										<option value="Tidak Tetap">Tidak Tetap</option>
									</select>
								</div>
								<div class="form-group form-group-icon-left"><i class="fa fa-user input-icon"></i>
									<label>Rentang Penghasilan Bulanan</label>
									<select name="rentang_pengahasilan_bulanan" id="rentang_pengahasilan_bulanan" class="form-control" required>
										<option value="<?php echo $row->rentang_pengahasilan_bulanan; ?>">Sebelumnya : <?php echo $row->rentang_pengahasilan_bulanan ?></option>
										<option value="> 2Juta"> > 2Juta </option>
										<option value="1,8 - 2Juta">1,8 - 2Juta</option>
										<option value="1,3 - 1,7Juta">1,3 - 1,7Juta</option>
										<option value="1 - 1,3Juta">1,3 - 1,7Juta</option>
										<option value="> 1Juta"> > 1Juta </option>
									</select>
								</div>
								<div class="gap gap-small"></div>
								<h4>D. Dimensi, Tipe, dan Fungsi</h4>
								<div class="form-group form-group-icon-left"><i class="fa fa-envelope input-icon"></i>
									<label>Luas Bangunan</label>
									<input name="luas_bangunan" class="form-control" id="luas_bangunan" placeholder="Luas Bangunan" type="number" value="<?php echo $row->luas_bangunan ?>" required/>
								</div>
								<div class="form-group form-group-icon-left"><i class="fa fa-user input-icon"></i>
									<label>Tipe Bangunan</label>
									<select name="tipe_bangunan" id="tipe_bangunan" class="form-control" required>
										<option value="<?php echo $row->tipe_bangunan; ?>">Sebelumnya : <?php echo $row->tipe_bangunan ?></option>
										<option value="Tunggal">Tunggal</option>
										<option value="Deret/Berhempit">Deret/Berhempit</option>
									</select>
								</div>
								<div class="form-group form-group-icon-left"><i class="fa fa-user input-icon"></i>
									<label>Status Fungsi Bangunan</label>
									<select name="fungsi_bangunan" id="fungsi_bangunan" class="form-control" required>
										<option value="<?php echo $row->fungsi_bangunan?>">Sebelumnya : <?php echo $row->fungsi_bangunan ?></option>
										<option value="Hunian Murni">Hunian Murni</option>
										<option value="Fungsi Ganda">Fungsi Ganda</option>
									</select>
								</div>
								<div class="gap gap-small"></div>
								<h4>F. Kepemilikan Aset Non Lahan</h4>
								<div class="form-group form-group-icon-left"><i class="fa fa-user input-icon"></i>
									<label>Kendaraan Roda Dua</label>
									<select name="kendaraan_roda_dua" id="kendaraan_roda_dua" class="form-control" required>
										<option value="<?php echo $row->kendaraan_roda_dua ?>">Sebelumnya : <?php echo $row->kendaraan_roda_dua ?></option>
										<option value="Motor">Motor</option>
										<option value="Sepeda">Sepeda</option>
										<option value="Becak">Becak</option>
									</select>
								</div>
								<div class="form-group form-group-icon-left"><i class="fa fa-user input-icon"></i>
									<label>Sarana Usaha</label>
									<select name="sarana_usaha" id="sarana_usaha" class="form-control" required>
										<option value="<?php echo $row->sarana_usaha ?>">Sebelumnya : <?php echo $row->sarana_usaha ?></option>
										<option value="Warung">Warung</option>
										<option value="Gerobak">Gerobak</option>
										<option value="Pikulan">Pikulan</option>
									</select>
								</div>
								<div class="form-group form-group-icon-left"><i class="fa fa-user input-icon"></i>
									<label>Hewan Ternak</label>
									<select name="hewan_ternak" id="hewan_ternak" class="form-control" required>
										<option value="<?php echo $row->hewan_ternak ?>">Sebelumnya : <?php echo $row->hewan_ternak; ?></option>
										<option value="Ada">Ada</option>
										<option value="Tidak Ada">Tidak Ada</option>
									</select>
								</div>
								<div class="gap gap-small"></div>
								<h4>H. Usia & Kondisi Fisik</h4>
								<div class="form-group form-group-icon-left"><i class="fa fa-envelope input-icon"></i>
									<label>Usia</label>
									<input name="usia" class="form-control" id="usia" placeholder="ex: 01" type="number" value="<?php echo $row->usia ?>" required/>
								</div>
								<div class="form-group form-group-icon-left"><i class="fa fa-user input-icon"></i>
									<label>Kategori Usia</label>
									<select name="kategori_usia" id="kategori_usia" class="form-control" required>
										<option value="<?php echo $row->kategori_usia ?>">Sebelumnya : <?php echo $row->kategori_usia ?></option>
										<option value="Produktif">Produktif</option>
										<option value="Lansia">Lansia</option>
									</select>
								</div>
								<div class="form-group form-group-icon-left"><i class="fa fa-user input-icon"></i>
									<label>Status Fisik</label>
									<select name="status_fisik" id="status_fisik" class="form-control" required>
										<option value="<?php echo $row->status_fisik ?>">Sebelumnya : <?php echo $row->status_fisik ?></option>
										<option value="Sehat">Sehat </option>
										<option value="Cacat">Cacat</option>
									</select>
								</div>
								<div class="gap gap-small"></div>
							</div>
							
							<div class="col-md-4">
								<h4>C. Kepemilikan</h4>
								<div class="form-group form-group-icon-left"><i class="fa fa-envelope input-icon"></i>
									<label>Luas Tanah</label>
									<input name="luas_tanah" class="form-control" id="luas_tanah" placeholder="Luas Tanah" type="number" value="<?php echo $row->luas_tanah ?>" required/>
								</div>
								<div class="form-group form-group-icon-left"><i class="fa fa-user input-icon"></i>
									<label>Status Tanah</label>
									<select name="status_tanah" id="status_tanah" class="form-control" required>
										<option value="<?php echo $row->status_tanah ?>">Sebelumnya : <?php echo $row->status_tanah; ?></option>
										<option value="SHM">SHM</option>
										<option value="HGB">HBG</option>
										<option value="Sewa Pakai">Sewa Pakai</option>
										<option value="Girik">Girik</option>
										<option value="Ilegal">Ilegal</option>
									</select>
								</div>
								<div class="form-group form-group-icon-left"><i class="fa fa-user input-icon"></i>
									<label>Status Penghunian Bangunan</label>
									<select name="status_penguhian_bangunan" id="status_penguhian_bangunan" class="form-control" required>
										<option value="<?php echo $row->status_penguhian_bangunan ?>">Sebelumnya : <?php echo $row->status_penguhian_bangunan ?></option>
										<option value="Milik Sendiri">Milik Sendiri</option>
										<option value="Milik Ortu/Keluarga">Milik Ortu/Keluarga</option>
										<option value="Sewa/Kontrak">Sewa/Kontrak</option>
										<option value="Bebas Pakai">Bebas Pakai</option>
										<option value="Lainnya">Lainnya</option>
									</select>
								</div>
								<div class="gap gap-small"></div>
							</div>
							<div class="col-md-4">
								
								<div class="gap gap-small"></div>
							</div>
							<div class="col-md-4">
								<h4>E. Kepadatan Penghuni</h4>
								<div class="form-group form-group-icon-left"><i class="fa fa-users input-icon"></i>
									<label>Jumlah Penghuni</label>
									<input name="jumlah_penghuni" class="form-control" id="jumlah_penghuni" placeholder="Jumlah Penghuni" type="number" value="<?php echo $row->jumlah_penghuni ?>"  required/>
								</div>
								<div class="form-group form-group-icon-left"><i class="fa fa-users input-icon"></i>
									<label>Jumlah Dewasa</label>
									<input name="jumlah_dewasa" class="form-control" id="jumlah_dewasa" placeholder="Jumlah Dewasa" type="number" value="<?php echo $row->jumlah_dewasa ?>" required/>
								</div>
								<div class="form-group form-group-icon-left"><i class="fa fa-users input-icon"></i>
									<label>Jumlah Anak</label>
									<input name="jumlah_anak" class="form-control" id="jumlah_anak" placeholder="Jumlah Anak" type="number" value="<?php echo $row->jumlah_anak ?>" required/>
								</div>
								<div class="form-group form-group-icon-left"><i class="fa fa-users input-icon"></i>
									<label>Jumlah Balita</label>
									<input name="jumlah_balita" class="form-control" id="jumlah_balita" placeholder="Jumlah Balita" type="number" value="<?php echo $row->jumlah_balita ?>" required/>
								</div>
								<div class="form-group form-group-icon-left"><i class="fa fa-users input-icon"></i>
									<label>Ruang Gerak Perjiwa</label>
									<input name="ruang_gerak_perjiwa" class="form-control" id="ruang_gerak_perjiwa" placeholder="Ruang Gerak Perjiwa m2/jiwa" value="<?php echo $row->ruang_gerak_perjiwa ?>" type="number" required/>
								</div>	
								<div class="gap gap-small"></div>
							</div>
							<div class="col-md-4">
								
								<div class="gap gap-small"></div>
							</div>
							<div class="col-md-4">
								<h4>G. Foto</h4>
								<div class="form-group form-group-icon-left"><i class="fa fa-users input-icon"></i>
									<label>Sisi 1</label>
									<input name="Sisi1" class="form-control" id="sisi1"  type="file"/>
									<input name="Sisi1_lama" value="<?php echo $row->sisi1 ?>" type="hidden"/>
								</div><div class="form-group form-group-icon-left"><i class="fa fa-users input-icon"></i>
									<label>Sisi 2</label>
									<input name="Sisi2" class="form-control" id="sisi2"  type="file"/>
									<input name="Sisi2_lama" value="<?php echo $row->sisi2 ?>" type="hidden"/>
								</div><div class="form-group form-group-icon-left"><i class="fa fa-users input-icon"></i>
									<label>Sisi 3</label>
									<input name="Sisi3" class="form-control" id="sisi3"  type="file"/>
									<input name="Sisi3_lama" value="<?php echo $row->sisi3 ?>" type="hidden"/>
								</div><div class="form-group form-group-icon-left"><i class="fa fa-users input-icon"></i>
									<label>Sisi 4</label>
									<input name="Sisi4" class="form-control" id="sisi4"  type="file"/>
									<input name="Sisi4_lama" value="<?php echo $row->sisi4 ?>" type="hidden"/>
								</div><div class="form-group form-group-icon-left"><i class="fa fa-users input-icon"></i>
									<label>Sisi 5</label>
									<input name="Sisi5" class="form-control" id="sisi5"  type="file"/>
									<input name="Sisi5_lama" value="<?php echo $row->sisi5 ?>" type="hidden"/>
								</div><div class="form-group form-group-icon-left"><i class="fa fa-users input-icon"></i>
									<label>Sisi 6</label>
									<input name="Sisi6" class="form-control" id="sisi6"  type="file"/>
									<input name="Sisi6_lama" value="<?php echo $row->sisi6 ?>" type="hidden"/>
								</div>
								<div class="gap gap-small"></div>
								<input name="id_rtlh" value="<?php echo $row->id_rtlh ?>" type="hidden"/>
							</div>
							
							<div class="col-md-12" align="right">
								<input type="submit" class="btn btn-primary" placeholder="Alamat" value="Simpan"><br><br>
								<div class="progress progress-striped active" id="prog_bar" style="display:none;">
									<div class="progress-bar progress-bar-primary" id="prog_bar_loading" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
										<span class="info_prog">Mohon Tunggu.. </span>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="alert alert-success" style="display:none;" id="info_sukses">
									<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
									</button>
									<p class="text-small"><span class="fa fa-check"></span> Edit Data Berhasil, data RT telah berhasil diinput.. </p>
								</div>
								<div class="alert alert-warning" style="display:none;" id="info_nomor">
									<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
									</button>
									<p class="text-small"><span class="fa fa-warning"></span> Maaf.. Nomor KTP pada RTLH ini sudah pernah diinput, silahkan coba input dengan nomor lain</p>
								</div>
								<div class="alert alert-danger" style="display:none;" id="info_gagal">
									<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
									</button>
									<p class="text-small"><span class="fa fa-warning"></span> Maaf Edit Gagal.. Silahkan coba beberapa lagi</p>
								</div>
								<div class="alert alert-danger" style="display:none;" id="info_gagal2">
									<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
									</button>
									<p class="text-small"><span class="fa fa-warning"></span> Maaf Input Gagal.. pada :</p>
									<p class="info_gagal2_sub">
									</p>
								</div>
							</div>
						</div>
					</form>
                </div>
            </div>
        </div>



        <div class="gap"></div>
		<footer id="main-footer">
            <?php include_once "layout_back/footer.php"; ?>
        </footer>
	   
        <script src="<?php echo base_url('asset/frontend'); ?>/js/jquery.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/jquery.form.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/bootstrap.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/slimmenu.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/bootstrap-timepicker.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/nicescroll.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/dropit.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/ionrangeslider.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/icheck.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/fotorama.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/typeahead.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/card-payment.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/magnific.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/owl-carousel.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/fitvids.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/tweet.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/countdown.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/gridrotator.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/custom.js"></script>

    </div>
</body>

</html>
<script type="text/javascript">
$(function () {
	var options2 = { 
		beforeSend: function() {
			$("#info_sukses").hide();
			$("#info_gagal").hide();
			$("#info_gagal2").hide();
			$("#info_nomor").hide();	
			$("#prog_bar").show();	
		},
		uploadProgress: function(event, position, total, percentComplete) {
			$("#prog_bar #prog_bar_loading").width(percentComplete+'%');
			//$("#progress_input_div #progress_input").html(percentComplete+'%');
			$("#prog2 .info_prog").html("Mohon Tunggu, Progress : "+percentComplete+'%');
		},
		success: function() {
			$("#prog_bar #prog_bar_loading").width('100%');
			$("#prog2 .info_prog").html('Complete 100%');
		},
		complete: function(response) {
			$("#prog_bar").hide();
			if(response.responseText == '1'){
				$("#info_sukses").show();
			}else if(response.responseText == '2'){
				$("#info_nomor").show();
			}else if(response.responseText == '0'){
				$("#info_gagal").show();
			}else{
				$("#info_gagal2 .info_gagal2_sub").html(response.responseText);
				$("#info_gagal2").show();
			}
		},
		error: function(){
			//$("#form_edit_ads #message").html("<font color='red'> ERROR: unable to upload files</font>");
		}
	}; 
	$("#form_input").ajaxForm(options2);

	$("#kecamatan").change(function(){
		var id_kecamatan = $("#kecamatan option:selected").val();
		$.ajax({
			url: "<?php echo site_url('backend/desa2')?>",
			type: "POST",
			data	: "id_kecamatan="+id_kecamatan,
			success : function (msg) {
				document.getElementById("kelurahan").disabled = false;
				$("#kelurahan").html(msg);
				//$("#kelurahan").css("color","black");
			}
		});
	});
	$("#kelurahan").change(function(){
		var id_kelurahan = $("#kelurahan option:selected").val();
		$.ajax({
			url: "<?php echo site_url('backend/rw2')?>",
			type: "POST",
			data	: "id_kelurahan="+id_kelurahan,
			success : function (msg) {
				if(msg == 'kosong'){
					alert("Maaf data RW dari kelurahan yang dipilih belum tersedia, silahkan ganti pilihan kelurahan lain");
				}else{
					document.getElementById("rw").disabled = false;
					$("#rw").html(msg);
				}
			}
		});
	});
	$("#rw").change(function(){
		var rw = $("#rw option:selected").val();
		$.ajax({
			url: "<?php echo site_url('backend/rt2')?>",
			type: "POST",
			data	: "no_rw="+rw,
			success : function (msg) {
				if(msg == 'kosong'){
					alert("Maaf data RT dari RW yang dipilih belum tersedia, silahkan ganti pilihan RW lain");
				}else{
					$("#rt").html(msg);
					document.getElementById("rt").disabled = false;
				}				
			}
		});
	});
	$("#tgl_survey").datepicker({
		format: 'yyyy-mm-dd'
	});
});
</script>