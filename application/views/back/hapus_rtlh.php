<!DOCTYPE HTML>
<?php foreach($instansi->result() as $is_row); ?>
<?php foreach($data_login->result() as $is_row2); ?>
<?php foreach($data_row->result() as $row); ?>
<html>

<head>
    <title><?php echo $title; ?></title>


    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="Template, html, premium, themeforest" />
    <meta name="description" content="Traveler - Premium template for travel companies">
    <meta name="author" content="Tsoy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

     <!-- GOOGLE FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600' rel='stylesheet' type='text/css'>
    <!-- /GOOGLE FONTS -->
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/icomoon.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/styles.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/mystyles.css">
    <script src="<?php echo base_url('asset/frontend'); ?>/js/modernizr.js"></script>
	<!-- /FAVICON---->
	<link rel="shortcut icon" href="<?php echo base_url("upload/$is_row->logo"); ?>"/>


</head>

<body>

    <!-- FACEBOOK WIDGET -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- /FACEBOOK WIDGET -->
    <div class="global-wrap">
        <header id="main-header">
			<?php include_once "layout_back/header.php";  ?>
		</header>
		
		<div class="container">
            <h1 class="page-title">Hapus | RTLH</h1>
        </div>




        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <?php include_once "layout_back/menu_nav.php"; ?>
                </div>
                <div class="col-md-9">
                    <form action="<?php echo site_url("backend/hapus_rtlh_ac/$row->id_rtlh"); ?>" method="post" enctype="multipart/form-data" id="form_input">
						<div class="row">
							<div class="col-md-12">
								<ul class="nav nav-tabs" id="myTab">
									<li class="active">
										<a href="#google-map-tab" data-toggle="tab"> 
										<h4><i class="fa fa-warning"></i>
											Konfirmasi Hapus
										</h4>
										</a>
									</li>
								</ul>
								<div class="tab-content">
									<div class="tab-pane fade in active" id="google-map-tab">
										<br>
										<p style="font-size:18px;">
											Apa anda yakin ingin menghapus data RTLH ini?
										</p>
										<table width="100%">
											<tr>
												<th>Atribut</th>
												<th width="10%">  </th>
												<th>Keterangan</th>
											</tr><tr>
												
												<td>No KTP</td>
												<td> : </td>
												<td><?php echo $row->no_ktp; ?></td>
											</tr><tr>
												
												<td>Nama Pemilik</td>
												<td> : </td>
												<td><?php echo $row->nm_pemilik; ?></td>
											</tr><tr>
												
												<td>Kecamatan</td>
												<td> : </td>
												<td><?php echo $row->nm_kecamatan; ?></td>
											</tr><tr>
												
												<td>Kelurahan</td>
												<td> : </td>
												<td><?php echo $row->nm_desa; ?></td>
											</tr><tr>
												
												<td>RW</td>
												<td> : </td>
												<td>RW No <?php echo $row->no_rw." - ".$row->nm_rw; ?></td>
											</tr><tr>
												
												<td>RT</td>
												<td> : </td>
												<td>RT No <?php echo $row->no_rt." - ".$row->nm_rt; ?></td>
											</tr>
										</table>
									</div>
								</div>
								<div class="gap gap-small"></div>
							</div>
							<div class="col-md-12" align="right">
								<button type="submit" class="btn btn-success">Ya, Hapus</button>
								<a href="<?php echo site_url('backend/list_rtlh'); ?>" class="btn btn btn-danger"><span class="fa fa-trash-o"></span>Tidak, Jangan hapus data ini</span></a>
								<br><br>
								<div class="progress progress-striped active" id="prog_bar" style="display:none;">
									<div class="progress-bar progress-bar-primary" id="prog_bar_loading" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
										<span class="info_prog">Mohon Tunggu.. </span>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="alert alert-success" style="display:none;" id="info_sukses">
									<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
									</button>
									<p class="text-small"><span class="fa fa-check"></span> Hapus Data Berhasil, data RTLH telah berhasil dihapus..
										<a href="<?php echo site_url('backend/list_rtlh'); ?>" class="btn btn-sm btn-success">List Data</a>
									</p>
								</div>
								<div class="alert alert-danger" style="display:none;" id="info_gagal">
									<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
									</button>
									<p class="text-small"><span class="fa fa-warning"></span> Maaf Hapus Gagal.. Silahkan coba beberapa lagi.</p>
								</div>
							</div>
						</div>
					</form>
                </div>
            </div>
        </div>



        <div class="gap"></div>
		<footer id="main-footer">
            <?php include_once "layout_back/footer.php"; ?>
        </footer>
	   
        <script src="<?php echo base_url('asset/frontend'); ?>/js/jquery.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/jquery.form.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/bootstrap.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/slimmenu.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/bootstrap-timepicker.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/nicescroll.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/dropit.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/ionrangeslider.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/icheck.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/fotorama.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/typeahead.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/card-payment.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/magnific.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/owl-carousel.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/fitvids.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/tweet.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/countdown.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/gridrotator.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/custom.js"></script>

    </div>
</body>

</html>
<script type="text/javascript">
$(function () {
	var options2 = { 
		beforeSend: function() {
			$("#info_sukses").hide();
			$("#info_gagal").hide();	
			$("#info_nomor").hide();	
			$("#prog_bar").show();	
		},
		uploadProgress: function(event, position, total, percentComplete) {
			$("#prog_bar #prog_bar_loading").width(percentComplete+'%');
			//$("#progress_input_div #progress_input").html(percentComplete+'%');
			$("#prog2 .info_prog").html("Mohon Tunggu, Progress : "+percentComplete+'%');
		},
		success: function() {
			$("#prog_bar #prog_bar_loading").width('100%');
			$("#prog2 .info_prog").html('Complete 100%');
		},
		complete: function(response) {
			$("#prog_bar").hide();
			if(response.responseText == '1'){
				$("#info_sukses").show();
				alert("Hapus Sukses, Data RLTH ini sudah berhasil dihapus");
				window.location = "<?php echo site_url("backend/list_rtlh/"); ?>";
			}else if(response.responseText == '2'){
				$("#info_nomor").show();
			}else{
				$("#info_gagal").show();
			}
		},
		error: function(){
			//$("#form_edit_ads #message").html("<font color='red'> ERROR: unable to upload files</font>");
		}
	}; 
	$("#form_input").ajaxForm(options2);

});
</script>