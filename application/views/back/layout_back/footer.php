<div class="container">
                <div class="row row-wrap">
                    <div class="col-md-8">
                        <a class="logo" href="index.html">
                            <img src="<?php echo base_url("upload/$is_row->logo"); ?>" style="width:10%;" alt="Image Alternative text" title="Image Title" />
							<span style="font-size:30px;">&nbsp<?php echo $is_row->nama ?></span>
                        </a>
                        <p class="mb20"><?php echo $is_row->tentang_singkat; ?></p>
                        <ul class="list list-horizontal list-space">
                            <li>
                                <a class="fa fa-facebook box-icon-normal round animate-icon-bottom-to-top" href="#"></a>
                            </li>
                            <li>
                                <a class="fa fa-twitter box-icon-normal round animate-icon-bottom-to-top" href="#"></a>
                            </li>
                            <li>
                                <a class="fa fa-google-plus box-icon-normal round animate-icon-bottom-to-top" href="#"></a>
                            </li>
                            <li>
                                <a class="fa fa-linkedin box-icon-normal round animate-icon-bottom-to-top" href="#"></a>
                            </li>
                            <li>
                                <a class="fa fa-pinterest box-icon-normal round animate-icon-bottom-to-top" href="#"></a>
                            </li>
                        </ul>
                    </div>
					
                    <div class="col-md-4" align="right">
                        <h4>Have Questions?</h4>
                        <h4 class="text-color">+<?php echo $is_row->no_telp; ?></h4>
                        <h4><a href="#" class="text-color"><?php echo $is_row->email; ?></a></h4>
                        <p>24/7 Dedicated Customer Support</p>
                    </div>

                </div>
            </div>
			<div class="container" align="center">
				<span style="font-size:14px;">© 2016. Sistem Informasi Kearsipan Surat. All right reserved.</span>
			</div>