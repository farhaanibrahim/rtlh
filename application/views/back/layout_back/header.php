<div class="header-top">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-sm-6">
                            <a class="logo" href="<?php echo base_url(); ?>" style="font-size:28px;">
                                <img src="<?php echo base_url("upload/$is_row->logo") ?>" style="width:5%;" alt="Image Alternative text" title="Image Title" />
								<?php echo $is_row->nama ?>
                            </a>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div class="top-user-area clearfix">
                                <ul class="top-user-area-list list list-horizontal list-border">
                									<?php
                									foreach($data_login->result() as $login){}
                                                    if ($this->session->userdata('admin_valid') == FALSE && $this->session->userdata('admin_id') == "") {
                									?>
                                                    <li>
                										<a href="<?php echo site_url('front/login') ?>">
                										Sign In <span class="fa fa-sign-in"></span>
                										</a>
                                                    </li>
                									<?php
                									}else{
                									?>
                									<li class="top-user-area-avatar">
                                    <a href="<?php echo site_url('backend') ?>">
                                    <span class="fa fa-user" style="font-size:18px;"></span> Hi, <?php echo $login->username ?></a>
                                  </li>
                                  <li>
                										<a href="<?php echo site_url('backend/logout') ?>">Sign Out <span class="fa fa-sign-out"></span></a>
                                  </li>
                									<?php
                									}
                									?>
                								</ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="nav">
                    <ul class="slimmenu" id="slimmenu">
                        <li class=""><a href="<?php echo base_url(); ?>">Home</a></li>
                        <li class=""><a href="<?php echo site_url('front/profile'); ?>">Profil</a></li>
                        <li class=""><a href="<?php echo site_url('front/pedoman_umum'); ?>">Pedoman Umum</a></li>
                        <li class=""><a href="<?php echo site_url('front/petunjuk_teknis'); ?>">Petunjuk Teknis</a></li>
                        <li class=""><a href="<?php echo site_url('front/map'); ?>">Map</a></li>
					</ul>
                </div>
            </div>
