<aside class="user-profile-sidebar">
  <div class="user-profile-avatar text-center">
    <?php /*<img src="<?php echo base_url("upload/$is_row->logo"); ?>" alt="Image Alternative text" title="AMaze" />*/ ?>
    <h5><?php echo $this->session->userdata('nama'); ?></h5>
    <h5><?php echo $this->session->userdata('nip'); ?></h5>
  </div>
  <ul class="list user-profile-nav">
    <li><a href="<?php echo site_url('backend/laporan'); ?>"><i class="fa fa-home"></i>Laporan</a>
    </li>
		<li><a href="<?php echo site_url('backend/list_rtlh'); ?>"><i class="fa fa-home"></i>RLTH</a>
    </li>
    <li><a href="<?php echo site_url('backend/list_rw'); ?>"><i class="fa fa-bars"></i>RW</a>
    </li>
    <li><a href="<?php echo site_url('backend/list_rt'); ?>"><i class="fa fa-bars"></i>RT</a>
    </li>
		<li><a href="<?php echo site_url('backend/setting_web'); ?>"><i class="fa fa-gear"></i>Setting Website</a>
    </li>
		<li><a href="<?php echo site_url('backend/setting_web'); ?>"><i class="fa fa-user"></i>Manajemen Pengguna</a>
    </li>
  </ul>
</aside>
