<!DOCTYPE HTML>
<?php foreach($instansi->result() as $is_row); ?>
<?php foreach($data_login->result() as $is_row2); ?>
<?php foreach($edit_input->result() as $is_row3); ?>
<html>

<head>
    <title><?php echo $title; ?></title>


    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="Template, html, premium, themeforest" />
    <meta name="description" content="Traveler - Premium template for travel companies">
    <meta name="author" content="Tsoy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

     <!-- GOOGLE FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600' rel='stylesheet' type='text/css'>
    <!-- /GOOGLE FONTS -->
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/icomoon.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/styles.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/mystyles.css">
    <script src="<?php echo base_url('asset/frontend'); ?>/js/modernizr.js"></script>
	<!-- /FAVICON---->
	<link rel="shortcut icon" href="<?php echo base_url("upload/$is_row->logo"); ?>"/>


</head>

<body>

    <!-- FACEBOOK WIDGET -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- /FACEBOOK WIDGET -->
    <div class="global-wrap">
        <header id="main-header">
			<?php include_once "layout_back/header.php";  ?>
		</header>

		<div class="container">
      <h1 class="page-title">Input Bantuan RTLH</h1>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-md-3">
          <?php include_once "layout_back/menu_nav.php"; ?>
        </div>
        <div class="col-md-9">
          <form action="<?php echo site_url('backend/edit_input_bantuan'); ?>" method="post">
            <?php echo $this->session->flashdata('edit_bantuan_sukses'); ?>
            <div class="form-group">
              <label for="">Nama Bantuan</label>
              <input type="text" name="nm-bantuan" id="nm_bantuan" class="form-control" placeholder="Nama Bantuan" required value="<?php echo $is_row3->nm_bantuan; ?>">
              <label for="">Instansi</label>
              <input type="text" name="instansi" id="instansi" class="form-control" placeholder="Instansi" required value="<?php echo $is_row3->instansi; ?>">
              <label for="">Jumlah Bantuan</label>
              <input type="text" name="jml_bantuan" class="form-control" placeholder="ex: 8000000" required value="<?php echo $is_row3->jml_bantuan; ?>">
              <label for="">Tahun</label>
              <input type="text" name="tahun" id="tahun" class="form-control" placeholder="Tahun" required value="<?php echo $is_row3->tahun; ?>">
            </div>
            <input type="submit" name="btnSubmit" class="btn btn-primary" value="Simpan">
          </form>
        </div>
      </div>
    </div>



        <div class="gap"></div>
		    <footer id="main-footer">
            <?php include_once "layout_back/footer.php"; ?>
        </footer>

        <script src="<?php echo base_url('asset/frontend'); ?>/js/jquery.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/jquery.form.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/bootstrap.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/slimmenu.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/bootstrap-timepicker.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/nicescroll.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/dropit.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/ionrangeslider.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/icheck.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/fotorama.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/typeahead.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/card-payment.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/magnific.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/owl-carousel.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/fitvids.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/tweet.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/countdown.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/gridrotator.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/custom.js"></script>

    </div>
</body>

</html>
<script type="text/javascript">
function ajaxSearch()
{
  var no_ktp = $('#no_ktp').val();

  clearTimeout($.data(this, 'timer'));

  if (no_ktp.length === 0) {
    document.getElementById("no_ktp").reset();
    $('#suggestions').hide();
    $('#form_bantuan').hide();
  } else {
    $.ajax({
        type: "GET",
        url: "<?php echo site_url('backend/bantuan'); ?>",
        data: "no_ktp="+no_ktp,
        success: function (data) {
          document.getElementById("form_bantuan").hidden = false;
          document.getElementById("nm_bantuan").disabled = false;
          document.getElementById("instansi").disabled = false;
          document.getElementById("tahun").disabled = false;
          $('#suggestions').html(data);
        }
     });
  }
 }
</script>
