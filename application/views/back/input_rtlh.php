<!DOCTYPE HTML>
<?php foreach($instansi->result() as $is_row); ?>
<?php foreach($data_login->result() as $is_row2); ?>
<html>

<head>
    <title><?php echo $title; ?></title>


    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="Template, html, premium, themeforest" />
    <meta name="description" content="Traveler - Premium template for travel companies">
    <meta name="author" content="Tsoy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

     <!-- GOOGLE FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600' rel='stylesheet' type='text/css'>
    <!-- /GOOGLE FONTS -->
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/icomoon.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/styles.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/mystyles.css">
    <script src="<?php echo base_url('asset/frontend'); ?>/js/modernizr.js"></script>
	<!-- /FAVICON---->
	<link rel="shortcut icon" href="<?php echo base_url("upload/$is_row->logo"); ?>"/>


</head>

<body>

    <!-- FACEBOOK WIDGET -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- /FACEBOOK WIDGET -->
    <div class="global-wrap">
        <header id="main-header">
			<?php include_once "layout_back/header.php";  ?>
		</header>

		<div class="container">
            <h1 class="page-title">Tambah | RTLH</h1>
        </div>




        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <?php include_once "layout_back/menu_nav.php"; ?>
                </div>
                <div class="col-md-9">
                  <form action="<?php echo site_url('backend/tambah_rtlh_ac'); ?>" method="post" enctype="multipart/form-data" id="form_input">
                    <div class="search-tabs search-tabs-bg">
                              <div class="tabbable">
                                  <ul class="nav nav-tabs" id="myTab">
                                      <li class="active"><a href="#tab-1" data-toggle="tab"><i class="fa">A</i></a>
                                      </li>
                                      <li><a href="#tab-2" data-toggle="tab"><i class="fa">B</i> </a>
                                      </li>
                                      <li><a href="#tab-3" data-toggle="tab"><i class="fa">C</i> </a>
                                      </li>
                                      <li><a href="#tab-4" data-toggle="tab"><i class="fa">D</i> </a>
                                      </li>
                                      <li><a href="#tab-5" data-toggle="tab"><i class="fa">E</i> </a>
                                      </li>
                                      <li><a href="#tab-6" data-toggle="tab"><i class="fa">F</i> </a>
                                      </li>
                                      <li><a href="#tab-7" data-toggle="tab"><i class="fa">G</i> </a>
                                      </li>
                                      <li><a href="#tab-8" data-toggle="tab"><i class="fa">H</i> </a>
                                      </li>
                                      <li style="background:green;"><a href="<?php echo site_url('backend/input_bantuan'); ?>"><i class="fa fa-plus"></i>Tambah Bantuan</a>
                                      </li>
                                  </ul>
                                  <div class="tab-content">
                                      <div class="tab-pane fade in active" id="tab-1">
                                          <h2>Identifikasi</h2>
                                            <div class="col-md-6">
                                              <table>
                                                <tr>
                                                  <td><label>Kecamatan</label></td>
                                                  <td>:</td>
                                                  <td><div class="form-group">
                                                    <select name="kecamatan" id="kecamatan" class="form-control" required>
                                                      <option value="">Pilih Kecamatan disini</option>
                                                      <?php foreach($kecamatan->result() as $kec){ ?>
                                                        <option value="<?php echo $kec->id_kecamatan ?>"><?php echo $kec->nm_kecamatan ?></option>
                                                      <?php } ?>
                                                    </select>
                                                  </div></td>
                                                </tr>
                                                <tr>
                                                  <td><label>Kelurahan</label></td>
                                                  <td>:</td>
                                                  <td><div class="form-group form-group-icon-left">
                                                    <select name="kelurahan" id="kelurahan" class="form-control" required>
                                                      <option value="">-- Silahkan Pilih Kelurahan disini --</option>
                                                    </select>
                                                  </div></td>
                                                </tr>
                                                <tr>
                                                  <td><label>RW</label></td>
                                                  <td>:</td>
                                                  <td><div class="form-group form-group-icon-left">
                                                    <select name="rw" id="rw" class="form-control" required>
                                                      <option value="">-- Silahkan Pilih RW disini --</option>
                                                    </select>
                                                  </div></td>
                                                </tr>
                                                <tr>
                                                  <td><label>RT</label></td>
                                                  <td>:</td>
                                                  <td><div class="form-group form-group-icon-left">
                                                    <select name="rt" id="rt" class="form-control" required>
                                                      <option value="">-- Silahkan Pilih RT disini --</option>
                                                    </select>
                                                  </div></td>
                                                </tr>
                                                <tr>
                                                  <td><label>No KTP</label></td>
                                                  <td>:</td>
                                                  <td><div class="form-group form-group-icon-left">
                                                    <input name="no_ktp" class="form-control" id="no_ktp" placeholder="ex: 01" type="number"  required/>
                                                  </div></td>
                                                </tr>
                                                <tr>
                                                  <td><label>No KK</label></td>
                                                  <td>:</td>
                                                  <td><div class="form-group form-group-icon-left">
                                                    <input name="no_kk" class="form-control" id="no_k" placeholder="ex: 01" type="number"  required/>
                                                  </div></td>
                                                </tr>
                                                <tr>
                                                  <td><label>Nama Pemilik</label></td>
                                                  <td>:</td>
                                                  <td><div class="form-group form-group-icon-left">
                                                    <input name="nm_pemilik" class="form-control" id="nm_pemilik" placeholder="Nama Pemilik KTP" type="text" />
                                                  </div></td>
                                                </tr>
                                              </table>
                                            </div>
                                            <div class="col-md-6">
                                              <table>
                                                <tr>
                                                  <td><label>Jenis Kelamin</label></td>
                                                  <td>:</td>
                                                  <td><div class="form-group form-group-icon-left">
                                                    <select name="jenis_kelamin" id="jenis_kelamin" class="form-control" required>
                                                      <option value="Pria">Pria</option>
                                                      <option value="Wanita">Wanita</option>
                                                    </select>
                                                  </div></td>
                                                </tr>
                                                <tr>
                                                  <td><label>Status Pernikahan</label></td>
                                                  <td>:</td>
                                                  <td><div class="form-group form-group-icon-left">
                                                    <select name="status_pernikahan" id="status_pernikahan" class="form-control" required>
                                                      <option value="">Pilih Status Pernikahan</option>
                                                      <option value="single/lajang">Single/Lajang</option>
                                                      <option value="menikah">Menikah</option>
                                                      <option value="duda/janda">Duda/Janda</option>
                                                    </select>
                                                  </div></td>
                                                </tr>
                                                <tr>
                                                  <td><label>Prioritas</label></td>
                                                  <td>:</td>
                                                  <td><div class="form-group form-group-icon-left">
                                                    <select name="prioritas" id="prioritas" class="form-control" required>
                                                      <option value="">Pilih Prioritas</option>
                                                      <option value="1">1</option>
                                                      <option value="2">2</option>
                                                      <option value="3">3</option>
                                                    </select>
                                                  </div></td>
                                                </tr>
                                                <tr>
                                                  <td><label>Latitude</label></td>
                                                  <td>:</td>
                                                  <td><div class="form-group form-group-icon-left">
                                                    <input class="form-control" id="lat" name="lat" placeholder="" type="text" />
                                                  </div></td>
                                                </tr>
                                                <tr>
                                                  <td><label>Longtitude</label></td>
                                                  <td>:</td>
                                                  <td><div class="form-group form-group-icon-left">
                                                    <input class="form-control" id="lon" name="lon" placeholder="" type="text" />
                                                  </div></td>
                                                </tr>
                                                <tr>
                                                  <td><label>Alamat</label></td>
                                                  <td>:</td>
                                                  <td><div class="form-group form-group-icon-left">
                                                    <textarea name="almt" id="almt" class="form-control" ></textarea>
                                                  </div></td>
                                                </tr>
                                                <tr>
                                                  <td><label>Keterangan</label></td>
                                                  <td>:</td>
                                                  <td><div class="form-group form-group-icon-left">
                                                    <textarea name="keterangan" id="keterangan" class="form-control" ></textarea>
                                                  </div></td>
                                                </tr>
                                              </table>
                                            </div>
                                            <a href="#tab-2" data-toggle="tab" class="btn btn-primary">Selanjutnya</a>
                                      </div>
                                      <div class="tab-pane fade" id="tab-2">
                                          <h2>Pekerjaan & Penghasilan</h2>
                                          <div class="form-group form-group-icon-left"><i class="fa fa-envelope input-icon"></i>
                                            <label>Bantuan yang pernah diterima</label>
                                            <input name="bantuan_yang_diterima" class="form-control" id="bantuan_yang_diterima" placeholder="Bantuan yang diterima" type="text"/>
                                          </div>
                                          <div class="form-group form-group-icon-left"><i class="fa fa-user input-icon"></i>
                                            <label>Pekerjaan</label>
                                            <select name="pekerjaan" id="pekerjaan" class="form-control" required>
                                              <option value="">Pilih Pekerjaan</option>
                                              <option value="PNS/Polri/TNI">PNS/Polri/TNI</option>
                                              <option value="Karyawan Swasta">Karyawan Swasta</option>
                                              <option value="Petani">Petani</option>
                                              <option value="Buruh">Buruh</option>
                                              <option value="Lainnya">Lainnya</option>
                                              <option value="Tidak Kerja">Tidak Kerja</option>
                                            </select>
                                          </div>
                                          <div class="form-group form-group-icon-left"><i class="fa fa-user input-icon"></i>
                                            <label>Intensitas Penghasilan</label>
                                            <select name="intensitas_penghasilan" id="intensitas_penghasilan" class="form-control" >
                                              <option value="">Pilih Intensitas Penghasilan</option>
                                              <option value="Tetap">Tetap</option>
                                              <option value="Tidak Tetap">Tidak Tetap</option>
                                            </select>
                                          </div>
                                          <div class="form-group form-group-icon-left"><i class="fa fa-user input-icon"></i>
                                            <label>Rentang Penghasilan Bulanan</label>
                                            <select name="rentang_pengahasilan_bulanan" id="rentang_pengahasilan_bulanan" class="form-control" required>
                                              <option value="">Pilih Rentang Penghasilan Bulanan</option>
                                              <option value="> 2Juta"> > 2Juta </option>
                                              <option value="1,8 - 2Juta">1,8 - 2Juta</option>
                                              <option value="1,3 - 1,7Juta">1,3 - 1,7Juta</option>
                                              <option value="1 - 1,7Juta">1 - 1,7Juta</option>
                                              <option value="< 1Juta"> < 1Juta </option>
                                            </select>
                                          </div>
                                          <a href="#tab-3" data-toggle="tab" class="btn btn-primary">Selanjutnya</a>
                                      </div>
                                      <div class="tab-pane fade" id="tab-3">
                                          <h2>Kepemilikan</h2>
                                          <div class="form-group form-group-icon-left"><i class="fa fa-envelope input-icon"></i>
                                            <label>Luas Tanah</label>
                                            <input name="luas_tanah" class="form-control" id="luas_tanah" placeholder="Luas Tanah" type="number"  required/>
                                          </div>
                                          <div class="form-group form-group-icon-left"><i class="fa fa-user input-icon"></i>
                                            <label>Status Tanah</label>
                                            <select name="status_tanah" id="status_tanah" class="form-control" required>
                                              <option value="">Pilih Status Tanah</option>
                                              <option value="SHM">SHM</option>
                                              <option value="HGB">HGB</option>
                                              <option value="Sewa Pakai">Sewa Pakai</option>
                                              <option value="Girik">Girik</option>
                                              <option value="Ilegal">Ilegal</option>
                                              <option value="Lainnya">Lainnya</option>
                                            </select>
                                          </div>
                                          <div class="form-group form-group-icon-left"><i class="fa fa-user input-icon"></i>
                                            <label>Status Penghunian Bangunan</label>
                                            <select name="status_penguhian_bangunan" id="status_penguhian_bangunan" class="form-control" required>
                                              <option value="">Pilih Status Penghunian Bangunan</option>
                                              <option value="Milik Sendiri">Milik Sendiri</option>
                                              <option value="Milik Ortu/Keluarga">Milik Ortu/Keluarga</option>
                                              <option value="Sewa/Kontrak">Sewa/Kontrak</option>
                                              <option value="Bebas Pakai">Bebas Pakai</option>
                                              <option value="Lainnya">Lainnya</option>
                                            </select>
                                          </div>
                                          <a href="#tab-4" data-toggle="tab" class="btn btn-primary">Selanjutnya</a>
                                      </div>
                                      <div class="tab-pane fade" id="tab-4">
                                          <h2>Dimensi, Tipe, dan Fungsi</h2>
                                          <div class="form-group form-group-icon-left"><i class="fa fa-envelope input-icon"></i>
                                            <label>Luas Bangunan</label>
                                            <input name="luas_bangunan" class="form-control" id="luas_bangunan" placeholder="Luas Bangunan" type="number"  required/>
                                          </div>
                                          <div class="form-group form-group-icon-left"><i class="fa fa-user input-icon"></i>
                                            <label>Tipe Bangunan</label>
                                            <select name="tipe_bangunan" id="tipe_bangunan" class="form-control" required>
                                              <option value="">Pilih Tipe Bangunan</option>
                                              <option value="Deret/Berhempit">Deret/Berhempit</option>
                                              <option value="Tunggal">Tunggal</option>
                                            </select>
                                          </div>
                                          <div class="form-group form-group-icon-left"><i class="fa fa-user input-icon"></i>
                                            <label>Status Fungsi Bangunan</label>
                                            <select name="fungsi_bangunan" id="fungsi_bangunan" class="form-control" required>
                                              <option value="">Pilih Fungsi Rumah</option>
                                              <option value="Hunian Murni">Hunian Murni</option>
                                              <option value="Fungsi Ganda">Fungsi Ganda</option>
                                            </select>
                                          </div>
                                          <a href="#tab-5" data-toggle="tab" class="btn btn-primary">Selanjutnya</a>
                                      </div>
                                      <div class="tab-pane fade" id="tab-5">
                                          <h2>Kepadatan Penghuni</h2>
                                          <div class="form-group form-group-icon-left"><i class="fa fa-users input-icon"></i>
                                            <label>Jumlah Penghuni</label>
                                            <input name="jumlah_penghuni" class="form-control" id="jumlah_penghuni" placeholder="Jumlah Penghuni" type="number"  required/>
                                          </div>
                                          <div class="form-group form-group-icon-left"><i class="fa fa-users input-icon"></i>
                                            <label>Jumlah Dewasa</label>
                                            <input name="jumlah_dewasa" class="form-control" id="jumlah_dewasa" placeholder="Jumlah Dewasa" type="number" required/>
                                          </div>
                                          <div class="form-group form-group-icon-left"><i class="fa fa-users input-icon"></i>
                                            <label>Jumlah Anak</label>
                                            <input name="jumlah_anak" class="form-control" id="jumlah_anak" placeholder="Jumlah Anak" type="number" required/>
                                          </div>
                                          <div class="form-group form-group-icon-left"><i class="fa fa-users input-icon"></i>
                                            <label>Jumlah Balita</label>
                                            <input name="jumlah_balita" class="form-control" id="jumlah_balita" placeholder="Jumlah Balita" type="number" required/>
                                          </div>
                                          <div class="form-group form-group-icon-left"><i class="fa fa-users input-icon"></i>
                                            <label>Ruang Gerak Perjiwa</label>
                                            <input name="ruang_gerak_perjiwa" class="form-control" id="ruang_gerak_perjiwa" placeholder="Ruang Gerak Perjiwa m2/jiwa" type="number" required/>
                                          </div>
                                          <a href="#tab-6" data-toggle="tab" class="btn btn-primary">Selanjutnya</a>
                                      </div>
                                      <div class="tab-pane fade" id="tab-6">
                                          <h2>Kepemilikan Aset Non Lahan</h2>
                                          <div class="form-group form-group-icon-left"><i class="fa fa-user input-icon"></i>
                                            <label>Kendaraan Roda Dua</label>
                                            <select name="kendaraan_roda_dua" id="kendaraan_roda_dua" class="form-control" required>
                                              <option value="">Pilih Kendaraan Roda Dua</option>
                                              <option value="Motor">Motor</option>
                                              <option value="Sepeda">Sepeda</option>
                                              <option value="Becak">Becak</option>
                                              <option value="Angkot">Angkot</option>
                                              <option value="Tidak Ada">Tidak Ada</option>
                                            </select>
                                          </div>
                                          <div class="form-group form-group-icon-left"><i class="fa fa-user input-icon"></i>
                                            <label>Sarana Usaha</label>
                                            <select name="sarana_usaha" id="sarana_usaha" class="form-control" required>
                                              <option value="">Pilih Sarana Usaha</option>
                                              <option value="Warung">Warung</option>
                                              <option value="Gerobak">Gerobak</option>
                                              <option value="Pikulan">Pikulan</option>
                                              <option value="Jahitan">Jahitan</option>
                                              <option value="Bengkel">Bengkel</option>
                                              <option value="Counter">Counter</option>
                                              <option value="Asongan">Asongan</option>
                                              <option value="Tidak Ada">Tidak Ada</option>
                                            </select>
                                          </div>
                                          <div class="form-group form-group-icon-left"><i class="fa fa-user input-icon"></i>
                                            <label>Hewan Ternak</label>
                                            <select name="hewan_ternak" id="hewan_ternak" class="form-control" required>
                                              <option value="">Pilih Hewan Ternak</option>
                                              <option value="Ada">Ada</option>
                                              <option value="Tidak Ada">Tidak Ada</option>
                                            </select>
                                          </div>
                                          <a href="#tab-7" data-toggle="tab" class="btn btn-primary">Selanjutnya</a>
                                      </div>
                                      <div class="tab-pane fade" id="tab-7">
                                          <h2>Foto</h2>
                                          <div class="form-group form-group-icon-left"><i class="fa fa-file-image-o input-icon"></i>
                                            <label>Sisi 1</label>
                                            <input name="Sisi1" class="form-control" id="sisi1" placeholder="Jumlah Penghuni" type="file" required/>
                                          </div><div class="form-group form-group-icon-left"><i class="fa fa-file-image-o  input-icon"></i>
                                            <label>Sisi 2</label>
                                            <input name="Sisi2" class="form-control" id="sisi2" placeholder="Jumlah Penghuni" type="file" required/>
                                          </div><div class="form-group form-group-icon-left"><i class="fa fa-file-image-o  input-icon"></i>
                                            <label>Sisi 3</label>
                                            <input name="Sisi3" class="form-control" id="sisi3" placeholder="Jumlah Penghuni" type="file" required/>
                                          </div><div class="form-group form-group-icon-left"><i class="fa fa-file-image-o  input-icon"></i>
                                            <label>Sisi 4</label>
                                            <input name="Sisi4" class="form-control" id="sisi4" placeholder="Jumlah Penghuni" type="file" required/>
                                          </div><div class="form-group form-group-icon-left"><i class="fa fa-file-image-o  input-icon"></i>
                                            <label>Sisi 5</label>
                                            <input name="Sisi5" class="form-control" id="sisi5" placeholder="Jumlah Penghuni" type="file" required/>
                                          </div><div class="form-group form-group-icon-left"><i class="fa fa-file-image-o  input-icon"></i>
                                            <label>Sisi 6</label>
                                            <input name="Sisi6" class="form-control" id="sisi6" placeholder="Jumlah Penghuni" type="file" required/>
                                          </div>
                                          <a href="#tab-8" data-toggle="tab" class="btn btn-primary">Selanjutnya</a>
                                      </div>
                                      <div class="tab-pane fade" id="tab-8">
                                          <h2>Usia & Kondisi Fisik</h2>
                                          <div class="form-group form-group-icon-left"><i class="fa fa-envelope input-icon"></i>
                                            <label>Usia</label>
                                            <input name="usia" class="form-control" id="usia" placeholder="ex: 01" type="number"  required/>
                                          </div>
                                          <div class="form-group form-group-icon-left"><i class="fa fa-user input-icon"></i>
                                            <label>Kategori Usia</label>
                                            <select name="kategori_usia" id="kategori_usia" class="form-control" required>
                                              <option value="">Pilih Kategori Usia</option>
                                              <option value="Produktif">Produktif</option>
                                              <option value="Lansia">Lansia</option>
                                            </select>
                                          </div>
                                          <div class="form-group form-group-icon-left"><i class="fa fa-user input-icon"></i>
                                            <label>Status Fisik</label>
                                            <select name="status_fisik" id="status_fisik" class="form-control" required>
                                              <option value="">Pilih Status Fisik</option>
                                              <option value="Sehat">Sehat</option>
                                              <option value="Cacat">Cacat</option>
                                            </select>
                                          </div>
                                          <input type="submit" class="btn btn-primary" placeholder="Alamat" value="Simpan">
                                      </div>
                                  </div>
                              </div>
                              <div class="col-md-12" align="right">
                                <br><br>
                                <div class="progress progress-striped active" id="prog_bar" style="display:none;">
                                  <div class="progress-bar progress-bar-primary" id="prog_bar_loading" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                    <span class="info_prog">Mohon Tunggu.. </span>
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-12">
                                <div class="alert alert-success" style="display:none;" id="info_sukses">
                                  <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
                                  </button>
                                  <p class="text-small"><span class="fa fa-check"></span> Tambah Data Berhasil, data RT telah berhasil diinput.. </p>
                                </div>
                                <div class="alert alert-warning" style="display:none;" id="info_nomor">
                                  <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
                                  </button>
                                  <p class="text-small"><span class="fa fa-warning"></span> Maaf.. Nomor KTP pada RTLH ini sudah pernah diinput, silahkan coba input dengan nomor lain</p>
                                </div>
                                <div class="alert alert-danger" style="display:none;" id="info_gagal">
                                  <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
                                  </button>
                                  <p class="text-small"><span class="fa fa-warning"></span> Maaf Input Gagal.. Silahkan coba beberapa lagi</p>
                                </div>
                                <div class="alert alert-danger" style="display:none;" id="info_gagal2">
                                  <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
                                  </button>
                                  <p class="text-small"><span class="fa fa-warning"></span> Maaf Input Gagal.. pada :</p>
                                  <p id="info_gagal2_sub">
                                  </p>
                                </div>
                              </div>
                          </div>
                  </form>
                </div>
            </div>
        </div>



        <div class="gap"></div>
		<footer id="main-footer">
            <?php include_once "layout_back/footer.php"; ?>
        </footer>

        <script src="<?php echo base_url('asset/frontend'); ?>/js/jquery.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/jquery.form.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/bootstrap.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/slimmenu.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/bootstrap-timepicker.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/nicescroll.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/dropit.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/ionrangeslider.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/icheck.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/fotorama.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/typeahead.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/card-payment.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/magnific.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/owl-carousel.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/fitvids.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/tweet.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/countdown.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/gridrotator.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/custom.js"></script>

    </div>
</body>

</html>
<script type="text/javascript">
$(function () {
	var options2 = {
		beforeSend: function() {
			$("#info_sukses").hide();
			$("#info_gagal").hide();
			$("#info_gagal2").hide();
			$("#info_nomor").hide();
			$("#prog_bar").show();
		},
		uploadProgress: function(event, position, total, percentComplete) {
			$("#prog_bar #prog_bar_loading").width(percentComplete+'%');
			//$("#progress_input_div #progress_input").html(percentComplete+'%');
			$("#prog2 .info_prog").html("Mohon Tunggu, Progress : "+percentComplete+'%');
		},
		success: function() {
			$("#prog_bar #prog_bar_loading").width('100%');
			$("#prog2 .info_prog").html('Complete 100%');
		},
		complete: function(response) {
			$("#prog_bar").hide();
			if(response.responseText == '1'){
				$("#info_sukses").show();
			}else if(response.responseText == '2'){
				$("#info_nomor").show();
			}else if(response.responseText == '0'){
				$("#info_gagal").show();
			}else{
				$("#info_gagal2 #info_gagal2_sub").html(response.responseText);
				$("#info_gagal2").show();
			}
		},
		error: function(){
			//$("#form_edit_ads #message").html("<font color='red'> ERROR: unable to upload files</font>");
		}
	};
	$("#form_input").ajaxForm(options2);

	$("#kecamatan").change(function(){
		var id_kecamatan = $("#kecamatan option:selected").val();
		$.ajax({
			url: "<?php echo site_url('backend/desa2')?>",
			type: "POST",
			data	: "id_kecamatan="+id_kecamatan,
			success : function (msg) {
				document.getElementById("kelurahan").disabled = false;
				$("#kelurahan").html(msg);
				//$("#kelurahan").css("color","black");
			}
		});
	});
	$("#kelurahan").change(function(){
		var id_kelurahan = $("#kelurahan option:selected").val();
		$.ajax({
			url: "<?php echo site_url('backend/rw2')?>",
			type: "POST",
			data	: "id_kelurahan="+id_kelurahan,
			success : function (msg) {
				if(msg == 'kosong'){
					alert("Maaf data RW dari kelurahan yang dipilih belum tersedia, silahkan ganti pilihan kelurahan lain");
				}else{
					document.getElementById("rw").disabled = false;
					$("#rw").html(msg);
				}
			}
		});
	});
	$("#rw").change(function(){
		var rw = $("#rw option:selected").val();
		$.ajax({
			url: "<?php echo site_url('backend/rt2')?>",
			type: "POST",
			data	: "no_rw="+rw,
			success : function (msg) {
				if(msg == 'kosong'){
					alert("Maaf data RT dari RW yang dipilih belum tersedia, silahkan ganti pilihan RW lain");
				}else{
					$("#rt").html(msg);
					document.getElementById("rt").disabled = false;
				}
			}
		});
	});
	$("#tgl_survey").datepicker({
		format: 'yyyy-mm-dd'
	});
});
</script>
