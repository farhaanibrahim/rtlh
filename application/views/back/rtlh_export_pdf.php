<!DOCTYPE HTML>
<?php foreach ($show_rtlh->result() as $title) {} ?>
<?php foreach ($instansi->result() as $is_row) {} ?>
<html>

<head>
    <title>Export</title>


    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="Template, html, premium, themeforest" />
    <meta name="description" content="Traveler - Premium template for travel companies">
    <meta name="author" content="Tsoy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/bootstrap.css">
</head>

<body>
  <style media="screen">
  thead:before, thead:after { display: none; }
  tbody:before, tbody:after { display: none; }
  strong { padding-left: 300px;}
  hr.line {border-top: 3px double #8c8b8b;}
  </style>
        <div class="container">
            <div class="row">
              <div class="col-md-12">
                <br><br>
                <table border="0">
                  <tr>
                    <td><img src="./upload/kota_bogor.jpg" style="width:10%;"></td>
                    <td class="text-center">
                      <strong>PEMERINTAH KOTA BOGOR</strong><br>
                      <strong>LAPORAN RTLH KECAMATAN <?php echo strtoupper($title->nm_kecamatan); ?></strong>
                    </td>
                  </tr>
                </table>
              </div>
              <hr class="line">
        				<div class="col-md-12">
        					<table class="table" border="1">
        							<thead>
                        <tr>
          								<th>No</th>
          								<th>No KTP</th>
          								<th>Nama Pemilik</th>
          								<th>Prioritas</th>
          								<th>Alamat</th>
          							</tr>
        							</thead>
        							<tbody>
                        <?php
          							$no = 1;
          							foreach($show_rtlh->result() as $row): ?>
          							<tr>
          								<td><?php echo $no; ?></td>
          								<td><?php echo $row->no_ktp; ?></td>
          								<td><?php echo $row->nm_pemilik; ?></td>
          								<td><?php echo $row->prioritas; ?></td>
          								<td><?php echo $row->nm_kecamatan." - ".$row->nm_desa." - "."RW".$row->no_rw." - RT ".$row->no_rt; ?></td>
          							</tr>
          							<?php
          							$no++;
          							endforeach; ?>
        							</tbody>
        					</table>
        				</div>
      			</div>
        </div>
      </body>
</html>
