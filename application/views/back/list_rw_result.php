<!DOCTYPE HTML>
<?php foreach($instansi->result() as $is_row); ?>
<?php foreach($data_login->result() as $is_row2); ?>
<html>

<head>
    <title><?php echo $title; ?></title>


    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="Template, html, premium, themeforest" />
    <meta name="description" content="Traveler - Premium template for travel companies">
    <meta name="author" content="Tsoy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

     <!-- GOOGLE FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600' rel='stylesheet' type='text/css'>
    <!-- /GOOGLE FONTS -->
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/icomoon.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/styles.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/mystyles.css">
    <script src="<?php echo base_url('asset/frontend'); ?>/js/modernizr.js"></script>
	<link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/js/DataTables/media/css/jquery.dataTables.min.css">
	
	<!-- /FAVICON---->
	<link rel="shortcut icon" href="<?php echo base_url("upload/$is_row->logo"); ?>"/>


</head>

<body>

    <!-- FACEBOOK WIDGET -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- /FACEBOOK WIDGET -->
    <div class="global-wrap">
        <header id="main-header">
			<?php include_once "layout_back/header.php";  ?>
		</header>
		
		<div class="container">
            <h1 class="page-title">Data | RW</h1>
        </div>




        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <?php include_once "layout_back/menu_nav.php"; ?>
                </div>
				<div class="col-md-9">
					<table id="data_tabel" style="color:black;">
						<thead>
							<tr>
								<th>No</th>
								<th>Kecamatan</th>
								<th>Kelurahan</th>
								<th>No RW</th>
								<th>Nama RW</th>
								<th>Telp</th>
								<th>Alamat</th>
								<th>Opsi</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$no = 1;
							foreach($show_rw->result() as $row): ?>
							<tr>
								<td><?php echo $no; ?></td>
								<td><?php echo $row->nm_kecamatan ?></td>
								<td><?php echo $row->nm_desa ?></td>
								<td><?php echo $row->no_rw ?></td>
								<td><?php echo $row->nm_rw ?></td>
								<td><?php echo $row->telp ?></td>
								<td><?php echo $row->almt ?></td>
								<td>
									<a href="<?php echo site_url("backend/edit_rw/$row->id_rw"); ?>" class="btn btn-sm btn-warning"><span class="fa fa-pencil"></span></a>
									<a href="<?php echo site_url("backend/hapus_rw/$row->id_rw"); ?>" class="btn btn-sm btn-danger"><span class="fa fa-trash-o"></span></a>
								</td>
							</tr>
							<?php 
							$no++;
							endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
        </div>



        <div class="gap"></div>
		<footer id="main-footer">
            <?php include_once "layout_back/footer.php"; ?>
        </footer>
	   
        <script src="<?php echo base_url('asset/frontend'); ?>/js/jquery.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/bootstrap.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/slimmenu.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/bootstrap-timepicker.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/nicescroll.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/dropit.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/ionrangeslider.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/icheck.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/fotorama.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/typeahead.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/card-payment.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/magnific.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/owl-carousel.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/fitvids.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/tweet.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/countdown.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/gridrotator.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/custom.js"></script>
		
		<script src="<?php echo base_url('asset/frontend'); ?>/js/DataTables/media/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/DataTables/media/js/dataTables.bootstrap.js"></script>

    </div>
</body>

</html>
<script type="text/javascript">
$(function () {
	$('#data_tabel').dataTable();
});
</script>