<!DOCTYPE HTML>
<?php foreach($instansi->result() as $is_row); ?>
<?php foreach($data_login->result() as $is_row2); ?>
<html>

<head>
    <title><?php echo $title; ?></title>


    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="Template, html, premium, themeforest" />
    <meta name="description" content="Traveler - Premium template for travel companies">
    <meta name="author" content="Tsoy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

     <!-- GOOGLE FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600' rel='stylesheet' type='text/css'>
    <!-- /GOOGLE FONTS -->
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/icomoon.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/styles.css">
    <link rel="stylesheet" href="<?php echo base_url('asset/frontend'); ?>/css/mystyles.css">
    <script src="<?php echo base_url('asset/frontend'); ?>/js/modernizr.js"></script>
	<!-- /FAVICON---->
	<link rel="shortcut icon" href="<?php echo base_url("upload/$is_row->logo"); ?>"/>


</head>

<body>

    <!-- FACEBOOK WIDGET -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- /FACEBOOK WIDGET -->
    <div class="global-wrap">
        <header id="main-header">
			<?php include_once "layout_back/header.php";  ?>
		</header>
		
		<div class="container">
            <h1 class="page-title">Tambah | RW</h1>
        </div>




        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <?php include_once "layout_back/menu_nav.php"; ?>
                </div>
                <div class="col-md-9">
                    <form action="<?php echo site_url('backend/tambah_rw_ac'); ?>" method="post" id="form_input">
						<div class="row">
							<div class="col-md-12">
								<div class="alert alert-success" style="display:none;" id="info_sukses">
									<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
									</button>
									<p class="text-small"><span class="fa fa-check"></span> Tambah Data Berhasil, data RW telah berhasil diinput.. </p>
								</div>
								<div class="alert alert-warning" style="display:none;" id="info_nomor">
									<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
									</button>
									<p class="text-small"><span class="fa fa-warning"></span> Maaf.. Nomor RW ini sudah pernah diinput, silahkan coba input dengan nomor lain</p>
								</div>
								<div class="alert alert-danger" style="display:none;" id="info_gagal">
									<button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span>
									</button>
									<p class="text-small"><span class="fa fa-warning"></span> Maaf Input Gagal.. Silahkan coba beberapa lagi</p>
								</div>
							</div>
							<div class="col-md-6">
							<h4>Alamat Detail</h4>
								<div class="form-group form-group-icon-left"><i class="fa fa-user input-icon"></i>
									<label>Kecamatan</label>
									<select name="kecamatan" id="kecamatan" class="form-control" required>
										<option value="">Pilih Kecamatan disini</option>
										<?php foreach($kecamatan->result() as $kec){ ?>
											<option value="<?php echo $kec->id_kecamatan ?>"><?php echo $kec->nm_kecamatan ?></option>
										<?php } ?>
									</select>
								</div>
								<div class="form-group form-group-icon-left"><i class="fa fa-user input-icon"></i>
									<label>Kelurahan</label>
									<select name="kelurahan" id="kelurahan" class="form-control" required disabled>
										<option value="">-- Silahkan Pilih Kelurahan disini --</option>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<h4>Informasi RW</h4>
								<div class="form-group form-group-icon-left"><i class="fa fa-envelope input-icon"></i>
									<label>No RW</label>
									<input name="no_rw" class="form-control" id="no_rw" disabled placeholder="ex: 01" type="number" />
								</div>
								<div class="form-group form-group-icon-left"><i class="fa fa-envelope input-icon"></i>
									<label>Nama RW</label>
									<input name="nm_rw" class="form-control" id="nm_rw" disabled placeholder="Nama RW" type="text" />
								</div>
								<div class="form-group form-group-icon-left"><i class="fa fa-phone input-icon"></i>
									<label>Telp</label>
									<input class="form-control" id="telp" name="telp" disabled placeholder="Telepon" type="text" />
								</div>
								<div class="form-group form-group-icon-left"><i class="fa fa-globe input-icon"></i>
									<label>Alamat</label>
									<textarea name="almt" id="almt" disabled class="form-control" ></textarea>
								</div>
								<div class="gap gap-small"></div>
							</div>
							<div class="col-md-12" align="right">
								<input type="submit" class="btn btn-primary" placeholder="Alamat" value="Simpan"><br><br>
								<div class="progress progress-striped active" id="prog_bar" style="display:none;">
									<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
										<span class="info_prog2">Mohon Tunggu.. </span>
									</div>
								</div>
							</div>
						</div>
					</form>
                </div>
            </div>
        </div>



        <div class="gap"></div>
		<footer id="main-footer">
            <?php include_once "layout_back/footer.php"; ?>
        </footer>
	   
        <script src="<?php echo base_url('asset/frontend'); ?>/js/jquery.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/jquery.form.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/bootstrap.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/slimmenu.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/bootstrap-timepicker.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/nicescroll.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/dropit.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/ionrangeslider.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/icheck.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/fotorama.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/typeahead.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/card-payment.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/magnific.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/owl-carousel.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/fitvids.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/tweet.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/countdown.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/gridrotator.js"></script>
        <script src="<?php echo base_url('asset/frontend'); ?>/js/custom.js"></script>

    </div>
</body>

</html>
<script type="text/javascript">
$(function () {
	var options2 = { 
		beforeSend: function() {
			$("#info_sukses").hide();
			$("#info_gagal").hide();	
			$("#info_nomor").hide();	
			$("#prog_bar").show();	
		},
		success: function() {
		},
		complete: function(response) {
			$("#prog_bar").hide();
			if(response.responseText == '1'){
				$("#info_sukses").show();
			}else if(response.responseText == '2'){
				$("#info_nomor").show();
			}else{
				$("#info_gagal").show();
			}
		},
		error: function(){
			//$("#form_edit_ads #message").html("<font color='red'> ERROR: unable to upload files</font>");
		}
	}; 
	$("#form_input").ajaxForm(options2);

	$("#kecamatan").change(function(){
		var id_kecamatan = $("#kecamatan option:selected").val();
		$.ajax({
			url: "<?php echo site_url('backend/desa2')?>",
			type: "POST",
			data	: "id_kecamatan="+id_kecamatan,
			success : function (msg) {
				document.getElementById("kelurahan").disabled = false;
				$("#kelurahan").html(msg);
				//$("#kelurahan").css("color","black");
			}
		});
	});
	$("#kelurahan").change(function(){
		var id_kecamatan = $("#kelurahan option:selected").val();
		if($(this).val() == ""){
			document.getElementById("no_rw").disabled = true;
			document.getElementById("nm_rw").disabled = true;
			document.getElementById("telp").disabled = true;
			document.getElementById("almt").disabled = true;
		}else{
			document.getElementById("no_rw").disabled = false;
			document.getElementById("nm_rw").disabled = false;
			document.getElementById("telp").disabled = false;
			document.getElementById("almt").disabled = false;
		}
	});
});
</script>