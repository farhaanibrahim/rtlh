<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Front extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('front_model');
		$this->load->model('back_model');
	}
	function index(){
		$data['title'] = 'Sistem Informasi RTLH';
		$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
		$data['kecamatan'] = $this->back_model->kecamatan();
		$data['instansi'] = $this->front_model->data_tr_instansi();
		$this->load->view('front/index',$data);
	}
	function profile(){
		$data['title'] = 'Profil | Sistem Informasi RTLH';
		$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
		$data['instansi'] = $this->front_model->data_tr_instansi();
		$this->load->view('front/profile',$data);
	}
	function cari_rtlh($tipe){
		if($tipe == 'baru'){
			$this->session->unset_userdata('config_search');
			if( isset($_POST['kecamatan']) != "" ){
				$config_search['kecamatan'] = $_POST['kecamatan'];
			}
			if( isset($_POST['kelurahan']) != "" ){
				$config_search['kelurahan'] = $_POST['kelurahan'];
			}
			if( isset($_POST['rw']) != "" ){
				$config_search['rw'] = $_POST['rw'];
			}
			if( isset($_POST['rt']) != "" ){
				$config_search['rt'] = $_POST['rt'];
			}

			$data['title'] = 'Cari RTLH | Sistem Informasi Rumah Tidak Layak Huni';
			$data['title2'] = 'Hasil Pencarian RTLH';

			$this->session->set_userdata('config_search',$config_search);
		}else{
			//$this->session->set_userdata('config_search',$config_search);
			$config_search = $this->session->userdata('config_search');
		}

		$data['cari_rtlh'] = $this->front_model->cari_rtlh($config_search);
		if(!isset($_GET['per_page'])){

			$data['data_row'] = $this->front_model->cari_rtlh2('0',$config_search);

		}else{
			$data['data_row'] = $this->front_model->cari_rtlh2($_GET['per_page'],$config_search);
		}

		$this->load->library('pagination');

		$config['page_query_string'] = TRUE;
		$config['base_url'] = site_url("front/cari_rtlh/lama").'/?';
		$config['total_rows'] = $data['cari_rtlh']->num_rows();
		$config['per_page'] = 3;
		$config['first_link'] = 'First';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';
		$config['cur_tag_open'] = "<li class='active'><a>";
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = "<li>";
		$config['num_tag_close'] = '</li>';

		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();

		$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
		$data['kecamatan'] = $this->back_model->kecamatan();
		$data['instansi'] = $this->front_model->data_tr_instansi();
		$this->load->view('front/list_rtlh',$data);
	}
	function detail_lokasi_rtlh($id){
		$data['data_row'] = $this->front_model->cari_rtlh_where_id($id);
		if($data['data_row']->num_rows() > 0){
			$data['title'] = 'Peta Informasi | Sistem Informasi RTLH';
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['kecamatan'] = $this->back_model->kecamatan();
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('front/maps_details',$data);
		}else{
			redirect("front");
		}
	}
	function login(){
		if($this->session->userdata('admin_id')){
			redirect("backend");
		}else{
			$data['title'] = 'Login | Sistem Informasi RTLH';
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('front/login',$data);
		}
	}

	function map(){
		$data['title'] = 'MAP | Sistem Informasi RTLH';
		$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
		$data['instansi'] = $this->front_model->data_tr_instansi();
		$data['firstPriority'] = $this->back_model->mark_first_priority();
		$data['secondPriority'] = $this->back_model->mark_second_priority();
		$data['thirdPriority'] = $this->back_model->mark_third_priority();

		$this->load->view('front/front_map',$data);
	}

	function pedoman_umum()
	{
		$data['title'] = 'Pedoman Umum | Sistem Informasi RTLH';
		$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
		$data['kecamatan'] = $this->back_model->kecamatan();
		$data['instansi'] = $this->front_model->data_tr_instansi();
		$this->load->view('front/pedoman_umum',$data);
	}

	function petunjuk_teknis()
	{
		$data['title'] = 'Petunjuk Teknis | Sistem Informasi RTLH';
		$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
		$data['kecamatan'] = $this->back_model->kecamatan();
		$data['instansi'] = $this->front_model->data_tr_instansi();
		$this->load->view('front/petunjuk_teknis',$data);
	}

	function pencarian()
	{
		if (isset($_POST['btnSubmit'])) {
			$kecamatan = $this->input->post('kecamatan');
			$data['kecamatan'] = $kecamatan;
			$data['judul_report'] = $this->back_model->kecamatan2($kecamatan);
			$data['jml_p1'] = $this->back_model->get_jml_p1($kecamatan)->num_rows();
			$data['jml_p2'] = $this->back_model->get_jml_p2($kecamatan)->num_rows();
			$data['jml_p3'] = $this->back_model->get_jml_p3($kecamatan)->num_rows();

			$this->load->view('front/hasil_pencarian',$data);
		} else {
			$data['kecamatan'] = $this->back_model->kecamatan();
			$this->load->view('front/pencarian',$data);
		}

	}
	function chartToPDF($kecamatan)
	{
		$this->load->library('pdfgenerator');

		$data['kecamatan'] = $kecamatan;
		$data['judul_report'] = $this->back_model->kecamatan2($kecamatan);
		$data['jml_p1'] = $this->back_model->get_jml_p1($kecamatan)->num_rows();
		$data['jml_p2'] = $this->back_model->get_jml_p2($kecamatan)->num_rows();
		$data['jml_p3'] = $this->back_model->get_jml_p3($kecamatan)->num_rows();
		$data['report'] = $this->back_model->getReport();

		$html = $this->load->view('front/chart2pdf', $data, true);

		$this->pdfgenerator->generate($html,'chart');
	}
}
?>
