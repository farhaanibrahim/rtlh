<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Backend extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('front_model');
		$this->load->model('back_model');
	}
	function index(){
		if($this->session->userdata('admin_id')){
			$data['cari_rtlh'] = $this->front_model->show_rtlh();
			if(!isset($_GET['per_page'])){
				$data['data_row'] = $this->front_model->show_rtlh2('0');

			}else{
				$data['data_row'] = $this->front_model->show_rtlh2($_GET['per_page']);
			}
			$this->load->library('pagination');

			$config['page_query_string'] = TRUE;
			$config['base_url'] = site_url("backend").'/?';
			$config['total_rows'] = $data['cari_rtlh']->num_rows();
			$config['per_page'] = 3;
			$config['first_link'] = 'First';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			$config['next_link'] = 'Next';
			$config['prev_link'] = 'Previous';
			$config['cur_tag_open'] = "<li class='active'><a>";
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = "<li>";
			$config['num_tag_close'] = '</li>';

			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();

			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['kecamatan'] = $this->back_model->kecamatan();
			$data['title'] = "Dashborad | RTLH";
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('back/index',$data);
		}
		else{
			redirect('front/login');
		}
	}
	function desa2(){
		if($this->back_model->kelurahan($this->input->post("id_kecamatan"))->num_rows() > 1){
			echo "<option value=''>Pilih Kelurahan Disini</option>";
			foreach($this->back_model->kelurahan($this->input->post("id_kecamatan"))->result() as $row){
				echo "<option value='$row->id_desa'>$row->nm_desa</option>";
			}
		}else{
			echo 'kosong';
		}
	}
	function rw2(){
		if($this->back_model->rw($this->input->post("id_kelurahan"))->num_rows() > 0){
			echo "<option value=''>Pilih RW Disini</option>";
			foreach($this->back_model->rw($this->input->post("id_kelurahan"))->result() as $row){
				echo "<option value='$row->no_rw'>Rw No $row->no_rw - $row->nm_rw</option>";
			}
		}else{
			echo 'kosong';
		}
	}
	function rt2(){
		if($this->back_model->rt($this->input->post("no_rw"))->num_rows() > 0){
			echo "<option value=''>Pilih RT Disini</option>";
			foreach($this->back_model->rt($this->input->post("no_rw"))->result() as $row){
				echo "<option value='$row->no_rt'>Rt No $row->no_rt - $row->nm_rt</option>";
			}
		}else{
			echo 'kosong';
		}
	}
	function input_bantuan()
	{
		if ($this->session->userdata('admin_id')) {
			$data['title'] = 'Laporan | Sistem Informasi RTLH';
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['instansi'] = $this->front_model->data_tr_instansi();
			if (isset($_POST['btnSubmit'])) {
				$data_bantuan = array(
					'id-bantuan'=>'',
					'no_ktp'=>$this->input->post('no_ktp'),
					'nm_bantuan'=>$this->input->post('nm-bantuan'),
					'instansi'=>$this->input->post('instansi'),
					'jml_bantuan'=>$this->input->post('jml_bantuan'),
					'tahun'=>$this->input->post('tahun')
				);
				$this->back_model->input_bantuan($data_bantuan);
				$this->session->set_flashdata('bantuan_sukses','
					<div class="alert alert-success alert-dismissable">
					  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					  Data telah tersimpan!<br>
						<table class="table">
							<tr>
								<td>No. KTP</td>
								<td>:</td>
								<td>'.$this->input->post('no_ktp').'</td>
							</tr>
							<tr>
								<td>Nama Bantuan</td>
								<td>:</td>
								<td>'.$this->input->post('nm-bantuan').'</td>
							</tr>
							<tr>
								<td>Instansi</td>
								<td>:</td>
								<td>'.$this->input->post('instansi').'</td>
							</tr>
							<tr>
								<td>Jumlah Bantuan</td>
								<td>:</td>
								<td>'.$this->input->post('jml_bantuan').'</td>
							</tr>
							<tr>
								<td>Tahun</td>
								<td>:</td>
								<td>'.$this->input->post('tahun').'</td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td><a href='.site_url('backend/edit_input_bantuan')."/".$this->input->post("no_ktp").' class="btn btn-warning">Edit</a></td>
							</tr>
						</table>
					</div>
					');
				redirect(site_url('backend/input_bantuan'));
			} else {
				$this->load->view('back/input_bantuan',$data);
			}

		} else {
			redirect('front/login');
		}
	}

	function edit_input_bantuan($no_ktp)
	{
		if ($this->session->userdata('admin_id')) {
			$data['title'] = 'Laporan | Sistem Informasi RTLH';
			$data['show_rw'] = $this->back_model->show_rw();
			$data['kecamatan'] = $this->back_model->kecamatan();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['instansi'] = $this->front_model->data_tr_instansi();

			$data['edit_input'] = $this->back_model->getBantuanInfo($no_ktp);
			$this->load->view('back/edit_input_bantuan',$data);
		} else {
			redirect('front/login');
		}

	}

	function bantuan()
	{
		$search_data = $this->input->get('no_ktp');

		$result = $this->back_model->show_rtlh_where_no_ktp($search_data);
		if (!empty($result)) {
			foreach ($result as $row) {

				echo "<tr>";
				echo "<td>No. KTP</td>";
				echo "<td>:</td>";
				echo "<td>".$row->no_ktp."</td>";
				echo "</tr>";

				echo "<tr>";
				echo "<td>Nama Pemilik</td>";
				echo "<td>:</td>";
				echo "<td>".$row->nm_pemilik."</td>";
				echo "</tr>";

				echo "<tr>";
				echo "<td>Alamat</td>";
				echo "<td>:</td>";
				echo "<td>".$row->rtlh_almt."</td>";
				echo "</tr>";

				echo "<tr>";
				echo "<td>Prioritas</td>";
				echo "<td>:</td>";
				echo "<td>".$row->prioritas."</td>";
				echo "</tr>";
			}
			echo "<input type='hidden' name='no_ktp' value='$row->no_ktp'>";
		} else {
			echo "Nomer KTP tidak ada";
		}

	}

	function laporan(){
		if($this->session->userdata('admin_id')){
			$data['title'] = 'Laporan | Sistem Informasi RTLH';
			$data['show_rw'] = $this->back_model->show_rw();
			$data['kecamatan'] = $this->back_model->kecamatan();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('back/laporan',$data);
		}
		else{
			redirect('front/login');
		}
	}
	function list_rtlh(){
		if($this->session->userdata('admin_id')){
			$data['title'] = 'List RW | Sistem Informasi RTLH';
			$data['show_rw'] = $this->back_model->show_rw();
			$data['kecamatan'] = $this->back_model->kecamatan();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('back/list_rtlh',$data);
		}
		else{
			redirect('front/login');
		}
	}
	function list_rtlh_result(){
		if($this->session->userdata('admin_id')){
			if( ($this->input->post('kecamatan')) && ($this->input->post('kelurahan')) ){
				$kec = $this->input->post('kecamatan');
				$kel = $this->input->post('kelurahan');
				$rt = $this->input->post('rt');
				$rw = $this->input->post('rw');

				$data['kecamatan'] = $kec;
				$data['kelurahan'] = $kel;
				$data['rt'] = $rt;
				$data['rw'] = $rw;

				$data['title'] = 'List RTLH | Sistem Informasi RTLH';
				$data['show_rtlh'] = $this->back_model->show_rtlh_where_kec_kel2($kec,$kel,$rw,$rt);
				$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
				$data['instansi'] = $this->front_model->data_tr_instansi();
				$this->load->view('back/list_rtlh_result',$data);
			}else{
				redirect('back/list_rw');
			}
		}
		else{
			redirect('front/login');
		}
	}
	function rtlh_export_pdf($kec,$kel,$rw,$rt)
	{
		$this->load->library('pdfgenerator');

		$data['instansi'] = $this->front_model->data_tr_instansi();
		$data['show_rtlh'] = $this->back_model->show_rtlh_where_kec_kel2($kec,$kel,$rw,$rt);

		$html = $this->load->view('back/rtlh_export_pdf', $data, true);

		$this->pdfgenerator->generate($html,'Report');
	}
	function list_rtlh_result2(){
		if($this->session->userdata('admin_id')){
			$data['title'] = 'List RTLH | Sistem Informasi RTLH';

			if($this->input->post("kecamatan") != ""){
				$data['kecamatan'] = $this->back_model->kecamatan2($this->input->post("kecamatan"));
			}
			if($this->input->post("kelurahan") != ""){
				$data['kelurahan'] = $this->back_model->kelurahan2($this->input->post("kelurahan"));
			}
			$data['show_rtlh'] = $this->back_model->show_rtlh_where_kec_kel3();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('back/list_rtlh_result2',$data);
		}
		else{
			redirect('front/login');
		}
	}
	function list_rw(){
		if($this->session->userdata('admin_id')){
			$data['title'] = 'List RTLH | Sistem Informasi RTLH';
			$data['show_rw'] = $this->back_model->show_rw();
			$data['kecamatan'] = $this->back_model->kecamatan();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('back/list_rw',$data);
		}
		else{
			redirect('front/login');
		}
	}
	function list_rw_result(){
		if($this->session->userdata('admin_id')){
			if( ($this->input->post('kecamatan')) && ($this->input->post('kelurahan')) ){
				$kec = $this->input->post('kecamatan');
				$kel = $this->input->post('kelurahan');
				$data['title'] = 'List RW | Sistem Informasi RTLH';
				$data['show_rw'] = $this->back_model->show_rw_where_kec_kel($kec,$kel);
				$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
				$data['instansi'] = $this->front_model->data_tr_instansi();
				$this->load->view('back/list_rw_result',$data);
			}else{
				redirect('back/list_rw');
			}
		}
		else{
			redirect('front/login');
		}
	}
	function hapus_rw($id){
		if($this->session->userdata('admin_id')){
			$data['title'] = 'Hapus RW | Sistem Informasi RTLH';
			$data['data_row'] = $this->back_model->cek_id_rw_rtlh($id);
			if($data['data_row']->num_rows() > 0){
				$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
				$data['instansi'] = $this->front_model->data_tr_instansi();
				$this->load->view('back/hapus_rw',$data);
			}else{
				redirect('backend/list_rw');
			}
		}
		else{
			redirect('front/login');
		}
	}
	function hapus_rw_ac($id){
		if($this->session->userdata('admin_id')){
			$data['data_row'] = $this->back_model->cek_id_rw_rtlh($id);
			if($data['data_row']->num_rows() > 0){
				if($this->back_model->hapus_rw($id)){
					echo "1";
				}else{
					echo "0";
				}
			}else{
				redirect('backend/list_rw');
			}
		}
		else{
			redirect('front/login');
		}
	}
	function edit_rw($id_rw){
		if($this->session->userdata('admin_id')){
			$data['title'] = 'Tambah RTLH | Sistem Informasi RTLH';
			$data['kecamatan'] = $this->back_model->kecamatan();
			$data['data_row'] = $this->back_model->cek_id_rw_rtlh($id_rw);
			if($data['data_row']->num_rows() > 0){
				$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
				$data['instansi'] = $this->front_model->data_tr_instansi();
				$this->load->view('back/edit_rw',$data);
			}else{
				redirect('backend/list_rw');
			}
		}
		else{
			redirect('front/login');
		}
	}
	function edit_rw_ac(){
		if($this->session->userdata('admin_id')){
			$data['data_rw'] = $this->back_model->cek_no_rw_rtlh($this->input->post('no_rw'));
			if($data['data_rw']->num_rows() < 1){
				if($this->back_model->edit_rw()){
					echo "1";
				}else{
					echo "0";
				}
			}else{
				foreach($data['data_rw']->result() as $res){
					$norw = $res->no_rw;
				}
				if( ($norw == $this->input->post('no_rw_lama')) ){
					if($this->back_model->edit_rw()){
						echo "1";
					}else{
						echo "0";
					}
				}else{
					echo "2";
				}
			}
		}
		else{
			redirect('front/login');
		}
	}
	function tambah_rw(){
		if($this->session->userdata('admin_id')){
			$data['title'] = 'Tambah RW | Sistem Informasi RTLH';
			$data['kecamatan'] = $this->back_model->kecamatan();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('back/input_rw',$data);
		}
		else{
			redirect('front/login');
		}
	}
	function tambah_rw_ac(){
		if($this->session->userdata('admin_id')){
			$no_rw = $this->input->post('no_rw');
			$kec = $this->input->post('kecamatan');
			$kel = $this->input->post('kelurahan');
			if($this->back_model->cek_no_rw($no_rw,$kec,$kel)->num_rows() < 1){
				if($this->back_model->tambah_rw()){
					echo "1";
				}else{
					echo "0";
				}
			}else{
				echo "2";
			}
		}
		else{
			redirect('front/login');
		}
	}
	function edit_rt($id_rt){
		if($this->session->userdata('admin_id')){
			$data['title'] = 'Edit RT | Sistem Informasi RT';
			$data['kecamatan'] = $this->back_model->kecamatan();
			$data['data_row'] = $this->back_model->cek_id_rt($id_rt);
			if($data['data_row']->num_rows() > 0){
				$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
				$data['instansi'] = $this->front_model->data_tr_instansi();
				$this->load->view('back/edit_rt',$data);
			}else{
				redirect('backend/list_rw');
			}
		}
		else{
			redirect('front/login');
		}
	}
	function edit_rt_ac(){
		if($this->session->userdata('admin_id')){
			$data['data_rt'] = $this->back_model->cek_no_rt2($this->input->post('no_rt'));
			if($data['data_rt']->num_rows() < 1){
				if($this->back_model->edit_rt()){
					echo "1";
				}else{
					echo "0";
				}
			}else{
				foreach($data['data_rt']->result() as $res){
					$nort = $res->no_rt;
				}
				if( ($nort == $this->input->post('no_rt_lama')) ){
					if($this->back_model->edit_rt()){
						echo "1";
					}else{
						echo "0";
					}
				}else{
					echo "2";
				}
			}
		}
		else{
			redirect('front/login');
		}
	}
	function hapus_rt($id){
		if($this->session->userdata('admin_id')){
			$data['title'] = 'Hapus RT | Sistem Informasi RTLH';
			$data['data_row'] = $this->back_model->cek_id_rt($id);
			if($data['data_row']->num_rows() > 0){
				$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
				$data['instansi'] = $this->front_model->data_tr_instansi();
				$this->load->view('back/hapus_rt',$data);
			}else{
				redirect('backend/list_rt');
			}
		}
		else{
			redirect('front/login');
		}
	}
	function hapus_rt_ac($id){
		if($this->session->userdata('admin_id')){
			$data['data_row'] = $this->back_model->cek_id_rt($id);
			if($data['data_row']->num_rows() > 0){
				if($this->back_model->hapus_rt($id)){
					echo "1";
				}else{
					echo "0";
				}
			}else{
				redirect('backend/list_rt');
			}
		}
		else{
			redirect('front/login');
		}
	}
	function tambah_rt(){
		if($this->session->userdata('admin_id')){
			$data['title'] = 'Tambah RT | Sistem Informasi RTLH';
			$data['kecamatan'] = $this->back_model->kecamatan();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('back/input_rt',$data);
		}
		else{
			redirect('front/login');
		}
	}
	function tambah_rt_ac(){
		if($this->session->userdata('admin_id')){
			$no_rt = $this->input->post('no_rt');
			$no_rw = $this->input->post('rw');
			$kec = $this->input->post('kecamatan');
			$kel = $this->input->post('kelurahan');
			if($this->back_model->cek_no_rt($no_rt,$no_rw,$kec,$kel)->num_rows() < 1){
				if($this->back_model->tambah_rt()){
					echo "1";
				}else{
					echo "0";
				}
			}else{
				echo "2";
			}
		}
		else{
			redirect('front/login');
		}
	}
	function list_rt(){
		if($this->session->userdata('admin_id')){
			$data['title'] = 'List RW | Sistem Informasi RTLH';
			$data['show_rw'] = $this->back_model->show_rw();
			$data['kecamatan'] = $this->back_model->kecamatan();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('back/list_rt',$data);
		}
		else{
			redirect('front/login');
		}
	}
	function list_rt_result(){
		if($this->session->userdata('admin_id')){
			if( ($this->input->post('kecamatan')) && ($this->input->post('kelurahan')) ){
				$rw = $this->input->post('rw');
				$kec = $this->input->post('kecamatan');
				$kel = $this->input->post('kelurahan');
				$data['title'] = 'List RW | Sistem Informasi RTLH';
				$data['show_rt'] = $this->back_model->show_rt_where_kec_kel($kec,$kel,$rw);
				$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
				$data['instansi'] = $this->front_model->data_tr_instansi();
				$this->load->view('back/list_rt_result',$data);
			}else{
				redirect('back/list_rw');
			}
		}
		else{
			redirect('front/login');
		}
	}
	function hapus_rtlh($id_rtlh){
		if($this->session->userdata('admin_id')){
			$data['title'] = 'Hapus RTLH | Sistem Informasi RTLH';
			$data['kecamatan'] = $this->back_model->kecamatan();
			$data['data_row'] = $this->back_model->cek_id_rt_rtlh2($id_rtlh);
			if($data['data_row']->num_rows() > 0){
				$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
				$data['instansi'] = $this->front_model->data_tr_instansi();
				$this->load->view('back/hapus_rtlh',$data);
			}else{
				redirect('backend/list_rtlh');
			}
		}
		else{
			redirect('front/login');
		}
	}
	function hapus_rtlh_ac($id_rtlh){
		if($this->session->userdata('admin_id')){
			$data['title'] = 'Hapus RTLH | Sistem Informasi RTLH';
			$data['data_row'] = $this->back_model->cek_id_rt_rtlh2($id_rtlh);
			if($data['data_row']->num_rows() > 0){
				if($this->back_model->hapus_rtlh($id_rtlh)){
					echo "1";
				}else{
					echo "0";
				}
			}else{
				redirect('backend/list_rtlh');
			}
		}
		else{
			redirect('front/login');
		}
	}
	function edit_rtlh($id_rtlh){
		if($this->session->userdata('admin_id')){
			$data['title'] = 'Edit RTLH | Sistem Informasi RTLH';
			$data['kecamatan'] = $this->back_model->kecamatan();
			$data['data_row'] = $this->back_model->cek_id_rt_rtlh2($id_rtlh);
			if($data['data_row']->num_rows() > 0){
				$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
				$data['instansi'] = $this->front_model->data_tr_instansi();
				$this->load->view('back/edit_rtlh',$data);
			}else{
				redirect('backend/list_rtlh');
			}
		}
		else{
			redirect('front/login');
		}
	}
	function edit_rtlh_ac(){
		if($this->session->userdata('admin_id')){
			if($this->back_model->cek_no_ktp_rtlh($this->input->post('no_ktp'))->num_rows() < 1){
				$config['upload_path'] = './upload/foto_awal';
				$config['allowed_types'] = 'jpg|jpeg|png|gif';
				$config['max_size']	= '6012312481248';
				$config['max_width']  = '211241242024';
				$config['max_height']  = '171212412468';

				$this->load->library('upload', $config);
				$x = 0;
				$y = 0;
				for ($i = 1; $i <=6; $i++){
					/*if ( $this->upload->do_upload('Sisi'.$i)) {
						$data = array('upload_sukses' => $this->upload->data());
						$nama_file[$i] = $data['upload_sukses']['file_name'];
						$nama_file_lama[$y] = $this->input->post("Sisi".$i."_lama");
						$x++;
						$y++;
					}else{
						//$error = array('error' => $this->upload->display_errors());
						//foreach($error as $a[$i]){}
						if($this->upload->display_errors() == "<p>You did not select a file to upload.</p>"){
							$nama_file[$i] = $this->input->post("Sisi".$i."_lama");
							$x++;
						}else{
							echo "<p class='text-small'><span class='fa fa-warning'></span>Foto Sisi $i: ".$this->upload->display_errors()."</p>";
							//break;
						}
					}*/
					if (!empty($_FILES['Sisi'.$i]['name'])) {
						if ( $this->upload->do_upload('Sisi'.$i)) {
							$y++;
							$data = array('upload_sukses' => $this->upload->data());
							$nama_file[$i] = $data['upload_sukses']['file_name'];
							$nama_file_lama[$y] = $this->input->post("Sisi".$i."_lama");
							$x++;
						}else{
							$error = array('error' => $this->upload->display_errors());
							foreach($error as $a){
								echo "<p class='text-small'><span class='fa fa-warning'></span>Foto Sisi $i: $a</p>";
							}
							break;
						}
					}else{
						$nama_file[$i] = $this->input->post("Sisi".$i."_lama");
						$x++;
					}
				}

				if($x >= 6){
					if($this->back_model->edit_rtlh2($nama_file)){
						echo "1";
						//echo $nama_file[$x];
					}else{
						echo "0";
					}
				}
				while($y > 0){
					if(isset($nama_file_lama[$y])){
						unlink("upload/foto_awal/".$nama_file_lama[$y]);
						$y = $y - 1;
					}
				}
			}else{
				echo "2";
			}
		}
		else{
			redirect('front/login');
		}
	}
	function edit_rtlh_foto_awal($id_rtlh){
		if($this->session->userdata('admin_id')){
			$data['title'] = 'Edit Foto Awal Rumah RTLH | Sistem Informasi RTLH';
			$data['title2'] = 'Edit Foto Awal Rumah';
			$data['data_row'] = $this->back_model->cek_id_rt_rtlh($id_rtlh);
			if($data['data_row']->num_rows() > 0){
				$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
				$data['instansi'] = $this->front_model->data_tr_instansi();
				$this->load->view('back/edit_fotoawal_rtlh.php',$data);
			}else{
				redirect("backend/list_rtlh");
			}
		}
		else{
			redirect('front/login');
		}
	}
	function edit_rtlh_foto_awal_ac(){
		if($this->session->userdata('admin_id')){
			if($this->back_model->cek_no_ktp_rtlh($this->input->post('no_ktp'))->num_rows() < 1){
				$config['upload_path'] = './upload/foto_awal';
				$config['allowed_types'] = 'jpg|jpeg|png|gif';
				$config['max_size']	= '6012312481248';
				$config['max_width']  = '211241242024';
				$config['max_height']  = '171212412468';

				$this->load->library('upload', $config);

				if ( $this->upload->do_upload('foto_rumah')) {
					$data = array('upload_sukses' => $this->upload->data());
					$nama_file = $data['upload_sukses']['file_name'];
					if($this->back_model->edit_foto_awal_rtlh($nama_file)){
						if($this->input->post('foto_awal') != 'belum di upload'){
							$nm_foto = $this->input->post('foto_awal');
							unlink("upload/foto_awal/".$nm_foto);
						}
						echo "1";
					}else{
						echo "0";
					}

				}else{
					$error = array('error' => $this->upload->display_errors());
					foreach($error as $a){
						echo $a;
					}
				}
			}else{
				echo "2";
			}
		}
		else{
			redirect('front/login');
		}
	}
	function edit_rtlh_foto_akhir_ac(){
		if($this->session->userdata('admin_id')){
			if($this->back_model->cek_no_ktp_rtlh($this->input->post('no_ktp'))->num_rows() < 1){
				$config['upload_path'] = './upload/foto_akhir';
				$config['allowed_types'] = 'jpg|jpeg|png|gif';
				$config['max_size']	= '6012312481248';
				$config['max_width']  = '211241242024';
				$config['max_height']  = '171212412468';

				$this->load->library('upload', $config);

				if ( $this->upload->do_upload('foto_rumah')) {
					$data = array('upload_sukses' => $this->upload->data());
					$nama_file = $data['upload_sukses']['file_name'];
					if($this->back_model->edit_foto_akhir_rtlh($nama_file)){
						if($this->input->post('foto_awal') != 'belum di upload'){
							$nm_foto = $this->input->post('foto_akhir');
							unlink("upload/foto_akhir/".$nm_foto);
						}
						echo "1";
					}else{
						echo "0";
					}

				}else{
					$error = array('error' => $this->upload->display_errors());
					foreach($error as $a){
						echo $a;
					}
				}
			}else{
				echo "2";
			}
		}
		else{
			redirect('front/login');
		}
	}
	function edit_rtlh_foto_akhir($id_rtlh){
		if($this->session->userdata('admin_id')){
			$data['title'] = 'Edit Foto Akhir Rumah RTLH | Sistem Informasi RTLH';
			$data['title2'] = 'Edit Foto Akhir Rumah';
			$data['data_row'] = $this->back_model->cek_id_rt_rtlh($id_rtlh);
			if($data['data_row']->num_rows() > 0){
				$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
				$data['instansi'] = $this->front_model->data_tr_instansi();
				$this->load->view('back/edit_fotoakhir_rtlh.php',$data);
			}else{
				redirect("backend/list_rtlh");
			}
		}
		else{
			redirect('front/login');
		}
	}
	function tambah_rtlh(){
		if($this->session->userdata('admin_id')){
			$data['title'] = 'Tambah RTLH | Sistem Informasi RTLH';
			$data['kecamatan'] = $this->back_model->kecamatan();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('back/input_rtlh',$data);
		}
		else{
			redirect('front/login');
		}
	}
	function tambah_rtlh_ac(){
		if($this->session->userdata('admin_id')){
			if($this->back_model->cek_no_ktp_rtlh($this->input->post('no_ktp'))->num_rows() < 1){
				$config['upload_path'] = './upload/foto_awal';
				$config['allowed_types'] = 'jpg|jpeg|png|gif';
				$config['max_size']	= '6012312481248';
				$config['max_width']  = '211241242024';
				$config['max_height']  = '171212412468';

				$this->load->library('upload', $config);
				$x = 0;
				for ($i = 1; $i <=6; $i++){
					if ( $this->upload->do_upload('Sisi'.$i)) {
						$data = array('upload_sukses' => $this->upload->data());
						$nama_file[$i] = $data['upload_sukses']['file_name'];
						$x++;
					}else{
						$error = array('error' => $this->upload->display_errors());
						foreach($error as $a){
							echo "<p class='text-small'><span class='fa fa-warning'></span>Foto Sisi $i: $a</p>";
						}
						break;
					}
				}
				if($x >= 1){
					if($this->back_model->tambah_rtlh2($nama_file)){
						echo "1";
					}else{
						echo "0";
					}
				}else{
					while($x > 0){
						unlink("upload/foto_awal/".$nama_file[$x]);
						$x = $x-1;
					}
				}
			}else{
				echo "2";
			}
		}
		else{
			redirect('front/login');
		}
	}
	function setting_web(){
		if($this->session->userdata('admin_id')){
			$data['title'] = 'Tambah RW | Sistem Informasi RTLH';
			$data['kecamatan'] = $this->back_model->kecamatan();
			$data['data_login'] = $this->front_model->data_login($this->session->userdata('admin_id'));
			$data['instansi'] = $this->front_model->data_tr_instansi();
			$this->load->view('back/edit_web',$data);
		}
		else{
			redirect('front/login');
		}
	}
	function setting_web_ac(){
		if($this->session->userdata('admin_id')){
			$config['upload_path'] = './upload';
			$config['allowed_types'] = 'jpg|jpeg|png|gif';
			$config['max_size']	= '6012312481248';
			$config['max_width']  = '211241242024';
			$config['max_height']  = '171212412468';

			$this->load->library('upload', $config);

			if ( $this->upload->do_upload('logo')) {
				$data = array('upload_sukses' => $this->upload->data());
				$nama_file = $data['upload_sukses']['file_name'];
				if($this->back_model->update_setting_web($nama_file)){
					unlink("upload/".$this->input->post('logo_lama'));
					echo "1";
				}else{
					echo "0";
				}

			}else{
				$error = array('error' => $this->upload->display_errors());
				foreach($error as $a){}
				if($a == '<p>You did not select a file to upload.</p>'){
					$nama_file = 'unknown';
					if($this->back_model->update_setting_web($this->input->post('logo_lama'))){
						echo "1";
					}else{
						echo "0";
					}
				}else{
					echo '0';
				}
			}
		}
		else{
			redirect('front/login');
		}
	}
	function login(){
		if($this->session->userdata('admin_id')){
			redirect("front");
		}
		else{
			$user = $this->input->post('username');
			$pass = $this->input->post('password');
			if($this->back_model->data_login($user,$pass)->num_rows() > 0){
				foreach($this->back_model->data_login($user,$pass)->result() as $row){
					$id_user = $row->id_user;
				}
				$this->session->set_userdata('admin_id',$id_user);
				echo "1";
			}else{
				echo "0";
			}
		}
	}
	function logout(){
		$this->session->unset_userdata('config_search');
		$this->session->unset_userdata('admin_id');
		redirect("front");
	}
}

?>
