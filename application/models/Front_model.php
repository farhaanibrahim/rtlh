<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Front_model extends CI_Model {
	function data_tr_instansi(){
		$this->db->where('id', 1);	
		return $this->db->get('tr_instansi');	
	}
	function cari_rtlh_where_id($id){
		$this->db->where('id_rtlh',$id);
		$this->db->join('mst_kecamatan', 'mst_kecamatan.id_kecamatan = t_rtlh2.rtlh_kecamatan');
		$this->db->join('mst_desa', 'mst_desa.id_desa = t_rtlh2.rtlh_kelurahan');
		$this->db->join('t-rw', 't-rw.no_rw = t_rtlh2.rtlh_rw');
		$this->db->join('t-rt', 't-rt.no_rt = t_rtlh2.rtlh_rt');
		
		return $this->db->get('t_rtlh2');	
	}
	function cari_rtlh($config_search){
		if($config_search['kecamatan'] != ""){
			$this->db->where('rtlh_kecamatan',$config_search['kecamatan']);
		}
		if($config_search['kelurahan'] != ""){
			$this->db->where('rtlh_kelurahan',$config_search['kelurahan']);
		}
		/*if($config_search['rw'] != ""){
			$this->db->where('rtlh_rw', $config_search['rw']);	
		}
		if($config_search['rt'] != ""){
			$this->db->where('rtlh_rt', $config_search['rt']);	
		}*/
		
		return $this->db->get('t_rtlh2');	
	}
	function cari_rtlh2($limit,$config_search){
		if($config_search['kecamatan'] != ""){
			$this->db->where('rtlh_kecamatan',$config_search['kecamatan']);
		}
		if($config_search['kelurahan'] != ""){
			$this->db->where('rtlh_kelurahan',$config_search['kelurahan']);
		}
		/*if($config_search['rw'] != ""){
			$this->db->where('rtlh_rw', $config_search['rw']);	
		}
		if($config_search['rt'] != ""){
			$this->db->where('rtlh_rt', $config_search['rt']);	
		}*/
		
		return $this->db->get('t_rtlh2',3,$limit);	
	}
	function show_rtlh(){		
		return $this->db->get('t_rtlh');	
	}
	function show_rtlh2($limit){
		return $this->db->get('t_rtlh2',3,$limit);	
	}
	function data_login($id_login){
		$this->db->where('id_user', $id_login);	
		
		return $this->db->get('t_user');	
	}
}

?>