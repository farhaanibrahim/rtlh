<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Back_model extends CI_Model {
	function data_login($user,$pass){
		$this->db->where('username', $user);
		$this->db->where('password', md5($pass));

		return $this->db->get('t_user');
	}
	function kecamatan(){
		$this->db->where('kabupaten_id', 3271);
		$this->db->order_by('nm_kecamatan', 'asc');

		return $this->db->get('mst_kecamatan');
	}
	function kecamatan2($id){
		$this->db->where('id_kecamatan', $id);
		$this->db->order_by('nm_kecamatan', 'asc');

		return $this->db->get('mst_kecamatan');
	}
	function kelurahan($id){
		$this->db->where('kecamatan_id', $id);
		$this->db->order_by('nm_desa', 'asc');

		return $this->db->get('mst_desa');
	}
	function kelurahan2($id){
		$this->db->where('id_desa', $id);
		$this->db->order_by('nm_desa', 'asc');

		return $this->db->get('mst_desa');
	}
	function rw($kel){
		$this->db->where('rw_kel', $kel);
		return $this->db->get('t-rw');
	}
	function rt($rw){
		$this->db->where('rt_rw', $rw);
		return $this->db->get('t-rt');
	}
	function show_rw(){
		return $this->db->get('t-rw');
	}
	function show_rw_where_kec_kel($kec,$kel){
		$this->db->join('mst_kecamatan', 'mst_kecamatan.id_kecamatan = t-rw.rw_kec');
		$this->db->join('mst_desa', 'mst_desa.id_desa = t-rw.rw_kel');

		$this->db->where('rw_kec', $kec);
		$this->db->where('rw_kel', $kel);
		return $this->db->get('t-rw');
	}
	function cek_no_rw($no_rw,$kec,$kel){
		$this->db->where('no_rw', $no_rw);
		$this->db->where('rw_kec', $kec);
		$this->db->where('rw_kel', $kel);

		return $this->db->get('t-rw');
	}
	function cek_no_rt($no_rt,$no_rw,$kec,$kel){
		$this->db->where('no_rt', $no_rt);
		$this->db->where('rt_rw', $no_rw);
		$this->db->where('rt_kec', $kec);
		$this->db->where('rt_kel', $kel);

		return $this->db->get('t-rt');
	}
	function cek_id_rw_rtlh($id){
		$this->db->join('mst_kecamatan', 'mst_kecamatan.id_kecamatan = t-rw.rw_kec');
		$this->db->join('mst_desa', 'mst_desa.id_desa = t-rw.rw_kel');
		$this->db->where('id_rw', $id);

		return $this->db->get('t-rw');
	}
	function cek_no_rw_rtlh($id){
		$this->db->join('mst_kecamatan', 'mst_kecamatan.id_kecamatan = t-rw.rw_kec');
		$this->db->join('mst_desa', 'mst_desa.id_desa = t-rw.rw_kel');
		$this->db->where('no_rw', $id);

		return $this->db->get('t-rw');
	}
	function hapus_rw($id){
		$this->db->where('id_rw', $id);
		return $this->db->delete('t-rw');
	}
	function edit_rw(){
		$data = array(
			'no_rw'      => $this->input->post('no_rw'),
			'nm_rw'      => $this->input->post('nm_rw'),
			'almt'      => $this->input->post('almt'),
			'telp'      => $this->input->post('telp'),
			'rw_kec'      => $this->input->post('kecamatan'),
			'rw_kel'      => $this->input->post('kelurahan'),
		);
		$this->db->where('id_rw', $this->input->post('id_rw'));
		return $this->db->update('t-rw',$data);
	}
	function tambah_rw(){
		$data = array(
			'no_rw'      => $this->input->post('no_rw'),
			'nm_rw'      => $this->input->post('nm_rw'),
			'almt'      => $this->input->post('almt'),
			'telp'      => $this->input->post('telp'),
			'rw_kec'      => $this->input->post('kecamatan'),
			'rw_kel'      => $this->input->post('kelurahan'),
		);
		return $this->db->insert('t-rw',$data);
	}
	function show_rt_where_kec_kel($kec,$kel,$rw){
		$this->db->join('mst_kecamatan', 'mst_kecamatan.id_kecamatan = t-rt.rt_kec');
		$this->db->join('mst_desa', 'mst_desa.id_desa = t-rt.rt_kel');
		$this->db->join('t-rw', 't-rw.no_rw = t-rt.rt_rw');

		$this->db->where('rt_rw', $rw);
		$this->db->where('rt_kec', $kec);
		$this->db->where('rt_kel', $kel);
		return $this->db->get('t-rt');
	}
	function tambah_rt(){
		$data = array(
			'no_rt'      => $this->input->post('no_rt'),
			'nm_rt'      => $this->input->post('nm_rt'),
			'rt_almt'      => $this->input->post('almt'),
			'rt_telp'      => $this->input->post('telp'),
			'rt_kec'      => $this->input->post('kecamatan'),
			'rt_kel'      => $this->input->post('kelurahan'),
			'rt_rw'      => $this->input->post('rw'),
		);
		return $this->db->insert('t-rt',$data);
	}
	function hapus_rt($id){
		$this->db->where('id_rt', $id);
		return $this->db->delete('t-rt');
	}
	function cek_id_rt($id){
		$this->db->join('mst_kecamatan', 'mst_kecamatan.id_kecamatan = t-rt.rt_kec');
		$this->db->join('mst_desa', 'mst_desa.id_desa = t-rt.rt_kel');
		$this->db->join('t-rw', 't-rw.no_rw = t-rt.rt_rw');
		$this->db->where('id_rt', $id);

		return $this->db->get('t-rt');
	}
	function cek_no_rt2($id){
		$this->db->join('mst_kecamatan', 'mst_kecamatan.id_kecamatan = t-rt.rt_kec');
		$this->db->join('mst_desa', 'mst_desa.id_desa = t-rt.rt_kel');
		$this->db->join('t-rw', 't-rw.no_rw = t-rt.rt_rw');
		$this->db->where('no_rt', $id);

		return $this->db->get('t-rt');
	}
	function edit_rt(){
		$data = array(
			'no_rt'      => $this->input->post('no_rt'),
			'nm_rt'      => $this->input->post('nm_rt'),
			'rt_almt'      => $this->input->post('almt'),
			'rt_telp'      => $this->input->post('telp'),
			'rt_kec'      => $this->input->post('kecamatan'),
			'rt_kel'      => $this->input->post('kelurahan'),
			'rt_rw'      => $this->input->post('rw'),
		);
		$this->db->where('id_rt', $this->input->post('id_rt'));
		return $this->db->update('t-rt',$data);
	}
	function cek_no_ktp_rtlh($no_ktp){
		$this->db->where('no_ktp', $no_ktp);

		return $this->db->get('t_rtlh');
	}
	function cek_id_rt_rtlh2($id){
		$this->db->join('mst_kecamatan', 'mst_kecamatan.id_kecamatan = t_rtlh2.rtlh_kecamatan');
		$this->db->join('mst_desa', 'mst_desa.id_desa = t_rtlh2.rtlh_kelurahan');
		$this->db->join('t-rw', 't-rw.no_rw = t_rtlh2.rtlh_rw');
		$this->db->join('t-rt', 't-rt.no_rt = t_rtlh2.rtlh_rt');
		$this->db->where('id_rtlh', $id);

		return $this->db->get('t_rtlh2');
	}
	function cek_id_rt_rtlh($id){
		$this->db->join('mst_kecamatan', 'mst_kecamatan.id_kecamatan = t_rtlh.rtlh_kec');
		$this->db->join('mst_desa', 'mst_desa.id_desa = t_rtlh.rtlh_kel');
		$this->db->join('t-rw', 't-rw.no_rw = t_rtlh.rtlh_rw');
		$this->db->join('t-rt', 't-rt.no_rt = t_rtlh.rtlh_rt');
		$this->db->where('id_rtlh', $id);

		return $this->db->get('t_rtlh');
	}
	function show_rtlh_where_kec_kel2($kec,$kel,$rw,$rt){
		$this->db->join('mst_kecamatan', 'mst_kecamatan.id_kecamatan = t_rtlh2.rtlh_kecamatan');
		$this->db->join('mst_desa', 'mst_desa.id_desa = t_rtlh2.rtlh_kelurahan');
		$this->db->join('t-rw', 't-rw.no_rw = t_rtlh2.rtlh_rw');
		$this->db->join('t-rt', 't-rt.no_rt = t_rtlh2.rtlh_rt');

		$this->db->where('rtlh_kecamatan', $kec);
		$this->db->where('rtlh_kelurahan', $kel);
		$this->db->where('rtlh_rw', $rw);
		$this->db->where('rtlh_rt', $rt);

		$this->db->group_by('id_rtlh');
		return $this->db->get('t_rtlh2');
	}
	function show_rtlh_where_kec_kel3(){
		$this->db->join('mst_kecamatan', 'mst_kecamatan.id_kecamatan = t_rtlh2.rtlh_kecamatan');
		$this->db->join('mst_desa', 'mst_desa.id_desa = t_rtlh2.rtlh_kelurahan');
		$this->db->join('t-rw', 't-rw.no_rw = t_rtlh2.rtlh_rw');
		$this->db->join('t-rt', 't-rt.no_rt = t_rtlh2.rtlh_rt');

		if($this->input->post('kecamatan') != ""){
			$this->db->where('rtlh_kecamatan', $this->input->post('kecamatan'));
		}
		if($this->input->post('kelurahan') != ""){
			$this->db->where('rtlh_kelurahan', $this->input->post('kelurahan'));
		}


		return $this->db->get('t_rtlh2');
	}
	function show_rtlh_where_kec_kel($kec,$kel,$rw,$rt){
		$this->db->join('mst_kecamatan', 'mst_kecamatan.id_kecamatan = t_rtlh.rtlh_kec');
		$this->db->join('mst_desa', 'mst_desa.id_desa = t_rtlh.rtlh_kel');
		$this->db->join('t-rw', 't-rw.no_rw = t_rtlh.rtlh_rw');
		$this->db->join('t-rt', 't-rt.no_rt = t_rtlh.rtlh_rw');

		$this->db->where('rtlh_kec', $kec);
		$this->db->where('rtlh_kel', $kel);
		$this->db->where('rtlh_rw', $rw);
		$this->db->where('rtlh_rt', $rt);

		return $this->db->get('t_rtlh');
	}
	function show_rtlh_where_no_ktp($no_ktp)
	{
		$this->db->select('*');
		$this->db->like('no_ktp',$no_ktp);
		return $this->db->get('t_rtlh2')->result();
	}

	function hapus_rtlh($id_rtlh){
		$this->db->where('id_rtlh', $id_rtlh);
		return $this->db->delete('t_rtlh2');
	}
	function tambah_rtlh2($nama_file){
		$data = array(
			'rtlh_rt'      => $this->input->post('rt'),
			'rtlh_kecamatan'      => $this->input->post('kecamatan'),
			'rtlh_kelurahan'      => $this->input->post('kelurahan'),
			'rtlh_rw'      => $this->input->post('rw'),
			'no_ktp'      => $this->input->post('no_ktp'),
			'no_kk'      => $this->input->post('no_kk'),
			'nm_pemilik'      => $this->input->post('nm_pemilik'),
			'jenis_kelamin'      => $this->input->post('jenis_kelamin'),
			'status_pernikahan'      => $this->input->post('status_pernikahan'),
			'prioritas'      => $this->input->post('prioritas'),
			'latitude'      => $this->input->post('lat'),
			'longitude'      => $this->input->post('lon'),
			'keterangan'      => $this->input->post('keterangan'),
			'rtlh_almt'      => $this->input->post('almt'),
			'pekerjaan'      => $this->input->post('pekerjaan'),
			'rentang_pengahasilan_bulanan'      => $this->input->post('rentang_pengahasilan_bulanan'),
			'intensitas_penghasilan' => $this->input->post('intensitas_penghasilan'),
			'luas_bangunan' => $this->input->post('luas_bangunan'),
			'status_tanah' => $this->input->post('status_tanah'),
			'status_penguhian_bangunan' => $this->input->post('status_penguhian_bangunan'),
			'kendaraan_roda_dua' => $this->input->post('kendaraan_roda_dua'),
			'sarana_usaha' => $this->input->post('sarana_usaha'),
			'hewan_ternak' => $this->input->post('hewan_ternak'),
			'usia' => $this->input->post('usia'),
			'kategori_usia' => $this->input->post('kategori_usia'),
			'status_fisik' => $this->input->post('status_fisik'),
			'luas_tanah' => $this->input->post('luas_tanah'),
			'ruang_gerak_perjiwa' => $this->input->post('ruang_gerak_perjiwa'),
			'jumlah_penghuni' => $this->input->post('jumlah_penghuni'),
			'jumlah_dewasa' => $this->input->post('jumlah_dewasa'),
			'jumlah_anak' => $this->input->post('jumlah_anak'),
			'bantuan_yang_diterima' => $this->input->post('bantuan_yang_diterima'),
			'jumlah_balita' => $this->input->post('jumlah_balita'),
			'sisi1'      => $nama_file[1],
			'sisi2'      => $nama_file[2],
			'sisi3'      => $nama_file[3],
			'sisi4'      => $nama_file[4],
			'sisi5'      => $nama_file[5],
			'sisi6'      => $nama_file[6],
		);
		return $this->db->insert('t_rtlh2',$data);
	}
	function edit_rtlh2($nama_file){
		$data = array(
			'rtlh_rt'      => $this->input->post('rt'),
			'rtlh_kecamatan'      => $this->input->post('kecamatan'),
			'rtlh_kelurahan'      => $this->input->post('kelurahan'),
			'rtlh_rw'      => $this->input->post('rw'),
			'no_ktp'      => $this->input->post('no_ktp'),
			'no_kk'      => $this->input->post('no_kk'),
			'nm_pemilik'      => $this->input->post('nm_pemilik'),
			'jenis_kelamin'      => $this->input->post('jenis_kelamin'),
			'status_pernikahan'      => $this->input->post('status_pernikahan'),
			'prioritas'      => $this->input->post('prioritas'),
			'bantuan_yang_diterima'      => $this->input->post('bantuan_yang_diterima'),
			'latitude'      => $this->input->post('lat'),
			'longitude'      => $this->input->post('lon'),
			'keterangan'      => $this->input->post('keterangan'),
			'rtlh_almt'      => $this->input->post('almt'),
			'pekerjaan'      => $this->input->post('pekerjaan'),
			'rentang_pengahasilan_bulanan'      => $this->input->post('rentang_pengahasilan_bulanan'),
			'intensitas_penghasilan' => $this->input->post('intensitas_penghasilan'),
			'luas_bangunan' => $this->input->post('luas_bangunan'),
			'status_tanah' => $this->input->post('status_tanah'),
			'status_penguhian_bangunan' => $this->input->post('status_penguhian_bangunan'),
			'kendaraan_roda_dua' => $this->input->post('kendaraan_roda_dua'),
			'sarana_usaha' => $this->input->post('sarana_usaha'),
			'hewan_ternak' => $this->input->post('hewan_ternak'),
			'usia' => $this->input->post('usia'),
			'kategori_usia' => $this->input->post('kategori_usia'),
			'status_fisik' => $this->input->post('status_fisik'),
			'luas_tanah' => $this->input->post('luas_tanah'),
			'ruang_gerak_perjiwa' => $this->input->post('ruang_gerak_perjiwa'),
			'jumlah_penghuni' => $this->input->post('jumlah_penghuni'),
			'jumlah_dewasa' => $this->input->post('jumlah_dewasa'),
			'jumlah_anak' => $this->input->post('jumlah_anak'),
			'jumlah_balita' => $this->input->post('jumlah_balita'),
			'sisi1'      => $nama_file[1],
			'sisi2'      => $nama_file[2],
			'sisi3'      => $nama_file[3],
			'sisi4'      => $nama_file[4],
			'sisi5'      => $nama_file[5],
			'sisi6'      => $nama_file[6],
		);
		$this->db->where('id_rtlh', $this->input->post('id_rtlh'));
		return $this->db->update('t_rtlh2',$data);
	}
	function tambah_rtlh($nama_file){
		$data = array(
			'no_ktp'      => $this->input->post('no_ktp'),
			'nm_pemilik'      => $this->input->post('nm_pemilik'),
			'rtlh_kd_pos'      => $this->input->post('kode_pos'),
			'rtlh_rt'      => $this->input->post('rt'),
			'rtlh_kec'      => $this->input->post('kecamatan'),
			'rtlh_kel'      => $this->input->post('kelurahan'),
			'rtlh_rw'      => $this->input->post('rw'),
			'no_tlpHP'      => $this->input->post('no_tlpHP'),
			'jum_kk'      => $this->input->post('jum_kk'),
			'jum_individu'      => $this->input->post('jum_individu'),
			'pendapatan'      => $this->input->post('pendapatan'),
			'nama_bantuan'      => $this->input->post('nama_bantuan'),
			'status_pendataan'      => $this->input->post('status_pendataan'),
			'prioritas'      => $this->input->post('prioritas'),
			'tgl_survey'      => $this->input->post('tgl_survey'),
			'latitude'      => $this->input->post('lat'),
			'longitude'      => $this->input->post('lon'),
			'keterangan'      => $this->input->post('keterangan'),
			'rtlh_almt'      => $this->input->post('almt'),
			'sisi1'      => $nama_file[1],
			'sisi2'      => $nama_file[2],
			'sisi3'      => $nama_file[3],
			'sisi4'      => $nama_file[4],
			'sisi5'      => $nama_file[5],
			'sisi6'      => $nama_file[6],
		);
		return $this->db->insert('t_rtlh',$data);
	}
	function edit_rtlh(){
		$data = array(
			'no_ktp'      => $this->input->post('no_ktp'),
			'nm_pemilik'      => $this->input->post('nm_pemilik'),
			'rtlh_kd_pos'      => $this->input->post('kode_pos'),
			'rtlh_rt'      => $this->input->post('rt'),
			'rtlh_kec'      => $this->input->post('kecamatan'),
			'rtlh_kel'      => $this->input->post('kelurahan'),
			'rtlh_rw'      => $this->input->post('rw'),
			'no_tlpHP'      => $this->input->post('no_tlpHP'),
			'jum_kk'      => $this->input->post('jum_kk'),
			'jum_individu'      => $this->input->post('jum_individu'),
			'pendapatan'      => $this->input->post('pendapatan'),
			'nama_bantuan'      => $this->input->post('nama_bantuan'),
			'status_pendataan'      => $this->input->post('status_pendataan'),
			'prioritas'      => $this->input->post('prioritas'),
			'tgl_survey'      => $this->input->post('tgl_survey'),
			'latitude'      => $this->input->post('lat'),
			'longitude'      => $this->input->post('lon'),
			'rtlh_almt'      => $this->input->post('almt'),
			'keterangan'      => $this->input->post('keterangan')
		);
		$this->db->where('id_rtlh', $this->input->post('id_rtlh'));
		return $this->db->update('t_rtlh',$data);
	}
	function edit_foto_awal_rtlh($nama_file){
		$data = array(
			'foto_awal'      => $nama_file
		);
		$this->db->where('id_rtlh', $this->input->post('id_rtlh'));
		return $this->db->update('t_rtlh',$data);
	}
	function edit_foto_akhir_rtlh($nama_file){
		$data = array(
			'foto_akhir'      => $nama_file
		);
		$this->db->where('id_rtlh', $this->input->post('id_rtlh'));
		return $this->db->update('t_rtlh',$data);
	}
	function update_setting_web($nama_file){
		$data = array(
			'nama'      => $this->input->post('nama'),
			'alamat'      => $this->input->post('alamat'),
			'nip_kepsek'      => $this->input->post('nip_kepsek'),
			'kepsek'      => $this->input->post('kepsek'),
			'no_telp'      => $this->input->post('no_telp'),
			'email'      => $this->input->post('email'),
			'tentang_singkat'      => $this->input->post('tentang_singkat'),
			'logo'      => $nama_file,
		);
		$this->db->where('id', 1);
		return $this->db->update('tr_instansi',$data);
	}

	function points()
	{
		return $this->db->get('t_rtlh2');
	}

	function mark_first_priority()
	{
		$where = array('prioritas'=>1);
		return $this->db->get_where('t_rtlh2',$where);
	}
	function mark_second_priority()
	{
		$where = array('prioritas'=>2);
		return $this->db->get_where('t_rtlh2',$where);
	}
	function mark_third_priority()
	{
		$where = array('prioritas'=>3);
		return $this->db->get_where('t_rtlh2',$where);
	}

	function input_bantuan($data)
	{
		$this->db->insert('t_bantuan',$data);
	}

	function edit_input_bantuan($data)
	{
		$this->db->update('t_bantuan',$data);
	}

	function getBantuanInfo($no_ktp)
	{
		$this->db->where('no_ktp',$no_ktp);
		return $this->db->get('t_bantuan');
	}

	public function get_jml_p1($kecamatan)
	{
		$where = array(
			'rtlh_kecamatan'=>$kecamatan,
			'prioritas'=>'1'
		);
		$this->db->where($where);
		return $this->db->get('t_rtlh2');
	}
	public function get_jml_p2($kecamatan)
	{
		$where = array(
			'rtlh_kecamatan'=>$kecamatan,
			'prioritas'=>'2'
		);
		$this->db->where($where);
		return $this->db->get('t_rtlh2');
	}
	public function get_jml_p3($kecamatan)
	{
		$where = array(
			'rtlh_kecamatan'=>$kecamatan,
			'prioritas'=>'3'
		);
		$this->db->where($where);
		return $this->db->get('t_rtlh2');
	}
}

?>
